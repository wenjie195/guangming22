<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminEditMainVideo.php" />
<meta property="og:title" content="Main Video Details | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Main Video Details  | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<link rel="canonical" href="https://gmvec.com/adminEditMainVideo.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Teaser Video</h2>
        <div class="clear"></div>

        <form method="POST" action="utilities/editLiveFunction.php">

            <?php
            if(isset($_POST['data_id']))
            {
                $conn = connDB();
                $liveDetails = getLiveShare($conn,"WHERE id = ? ", array("id") ,array($_POST['data_id']),"i");
            ?>
                <div class="dual-input">
                    <p class="input-top-text">Title</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $liveDetails[0]->getTitle();?>" name="update_title" id="update_title" required>       
                </div>
                
                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Host</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $liveDetails[0]->getHost();?>" name="update_host" id="update_host" required>       
                </div>

                <div class="dual-input">
                    <p class="input-top-text">Platform</p>
                    <!-- <input class="input-name clean input-textarea admin-input" type="text" value="<?php //echo $liveDetails[0]->getPlatform();?>" name="update_platform" id="update_platform" required>        -->
                    <select class="aidex-input clean" type="text" name="update_platform" id="update_platform" required>
                        <option value="">Please Select A Platform</option>
                        <?php
                        if($liveDetails[0]->getPlatform() == '')
                        {
                        ?>
                            <option selected>Please Select a Platform</option>
                            <?php
                            for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                            {
                            ?>
                                <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                    <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                </option>
                            <?php
                            }
                        }
                        else
                        {
                            for ($cnt=0; $cnt <count($platformDetails) ; $cnt++){
                                if ($liveDetails[0]->getPlatform() == $platformDetails[$cnt]->getPlatformType())
                                {
                                ?>
                                    <option selected value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                        <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                        <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select> 
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Link</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $liveDetails[0]->getLink();?>" name="update_link" id="update_link" required>       
                </div>

                <input type="hidden" value="<?php echo $liveDetails[0]->getId();?>" name="live_id" id="live_id" required> 

            <?php
            }
            ?>
    	
            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

	</div>


<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>