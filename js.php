<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>

<div class="footer-div width100 overflow text-center">
	<p class="footer-p">© <?php echo $time ;?>  Guang Ming Daily, All Rights Reserved.</p>	
</div>



<!-- Login Modal -->
<div id="login-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-login">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 gold-text login-h1">Login</h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <form action="utilities/loginFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text">Username</p>
                <input class="aidex-input clean" type="text" placeholder="Username" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <input class="aidex-input clean" type="password" placeholder="Password" id="password" name="password" required>
            </div>   

            <button class="clean-button clean login-btn pink-button" name="login">Login</button>
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Login Modal -->
<div id="userlogin-modal" class="modal-css">
	<div class="modal-content-css login-modal-content userlogin-modal-content">
        <span class="close-css close-userlogin">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 gold-text login-h1">Login</h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <form action="utilities/userLoginFunction.php" method="POST">
            <div class="login-input-div first-login-div">
                <p class="input-top-text">Username</p>
                <input class="aidex-input clean" type="text" placeholder="Username" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input"  type="password" placeholder="Password" id="password_user" name="password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionD()" alt="View Password" title="View Password">
                </div>  

            </div>   

            <button class="clean-button clean login-btn pink-button" name="login">Login</button>
            <div class="clear"></div>

            <div class="width100 overflow text-center">     
                <p class="open-userregister red-link register-a" >Register</p>
            </div>			 
			 
			 
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- Register User Modal -->
<div id="userregister-modal" class="modal-css">
	<div class="modal-content-css login-modal-content userlogin-modal-content userregister-modal-content">
        <span class="close-css close-userregister">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 gold-text login-h1">Register</h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <form action="utilities/registrationFunction.php" method="POST">
            <div class="login-input-div first-login-div">
                <p class="input-top-text">Username</p>
                <input class="aidex-input clean" type="text"placeholder="Username" id="register_username" name="register_username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Phone Number</p>
                <input class="aidex-input clean" type="text" placeholder="Phone Number" id="register_phone" name="register_phone" required>
            </div>  			 
            <div class="login-input-div">
                <p class="input-top-text">Email</p>
                <input class="aidex-input clean" type="email" placeholder="Email" id="register_email" name="register_email" required>
            </div> 			 
			 
            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input"  type="password" placeholder="Password" id="register_password" name="register_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>  
            </div>   
            <div class="login-input-div">
                <p class="input-top-text">Retype Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input" type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
                </div>  
            </div> 
            <button class="clean-button clean login-btn pink-button" name="submit">Sign Up</button>
            <div class="clear"></div>

            <div class="width100 overflow text-center">     
                <p class="open-userlogin red-link register-a" >Login</p>
            </div>			 
			 
			 
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- HomeVideo Modal -->
<div id="homevideo-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-homevideo">&times;</span>
        <div class="clear"></div>
       <h1 class="title-h1 gold-text login-h1">Home Main Video</h1>	   
       <div class="big-white-div">
           <img src="img/home-main.png" class="width100">              
       </div>
        
                
	</div>

</div>
<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
<script>

var loginmodal = document.getElementById("login-modal");
var userloginmodal = document.getElementById("userlogin-modal");
var userregistermodal = document.getElementById("userregister-modal");
var homevideomodal = document.getElementById("homevideo-modal");
var mahsingmodal = document.getElementById("mahsing-modal");
	
var openlogin = document.getElementsByClassName("open-login")[0];
var openuserlogin = document.getElementsByClassName("open-userlogin")[0];
var openuserlogin1 = document.getElementsByClassName("open-userlogin")[1];
var openuserregister = document.getElementsByClassName("open-userregister")[0];
var openhomevideo = document.getElementsByClassName("open-homevideo")[0];



var closelogin = document.getElementsByClassName("close-login")[0];
var closeuserlogin = document.getElementsByClassName("close-userlogin")[0];
var closeuserregister = document.getElementsByClassName("close-userregister")[0];
var closehomevideo = document.getElementsByClassName("close-homevideo")[0];
var closemahsing = document.getElementsByClassName("close-mahsing")[0];
var closemahsing1 = document.getElementsByClassName("close-mahsing")[1];

if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openuserlogin){
openuserlogin.onclick = function() {
  userloginmodal.style.display = "block";
}
}
if(openuserlogin1){
openuserlogin1.onclick = function() {
  userloginmodal.style.display = "block";
  userregistermodal.style.display = "none";
}
}	
if(openuserregister){
openuserregister.onclick = function() {
  userregistermodal.style.display = "block";
  userloginmodal.style.display = "none";
}
}
if(closelogin){
  closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closeuserlogin){
  closeuserlogin.onclick = function() {
  userloginmodal.style.display = "none";
}
}
if(closeuserregister){
  closeuserregister.onclick = function() {
  userregistermodal.style.display = "none";
}
}
if(closemahsing){
  closemahsing.onclick = function() {
  mahsingmodal.style.display = "none";
}
}
if(closemahsing1){
  closemahsing1.onclick = function() {
  mahsingmodal.style.display = "none";
}
}
	
	
window.onclick = function(event) {

  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }  
  if (event.target == userloginmodal) {
    userloginmodal.style.display = "none";
  }  
   if (event.target == userregistermodal) {
    userregistermodal.style.display = "none";
  }     
   if (event.target == mahsingmodal) {
    mahsingmodal.style.display = "none";
  }   
}
</script>
<script src="js/headroom.js"></script>
<script>
(function() {
    var header = new Headroom(document.querySelector("#header"), {
        tolerance: 5,
        offset : 205,
        classes: {
          initial: "animated",
          pinned: "slideDown",
          unpinned: "slideUp"
        }
    });
    header.init();

    var bttHeadroom = new Headroom(document.getElementById("btt"), {
        tolerance : 0,
        offset : 500,
        classes : {
            initial : "slide",
            pinned : "slide--reset",
            unpinned : "slide--down"
        }
    });
    bttHeadroom.init();
}());
</script>
<script src="js/jquery.colorbox-min.js"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements

				$(".ajax").colorbox();
				$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				$(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
				$(".iframe").colorbox({iframe:true, width:"90%", height:"90%"});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});

				$('.non-retina').colorbox({rel:'group5', transition:'none'})
				$('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>

<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionD()
{
    var x = document.getElementById("password_user");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}	
</script>