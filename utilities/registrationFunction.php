<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Registration.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$username,$phoneNo,$email,$userType,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"registration",array("uid","username","phone_no","email","user_type","password","salt"),
          array($uid,$username,$phoneNo,$email,$userType,$finalPassword,$salt),"ssssiss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     $phoneNo = rewrite($_POST['register_phone']);
     $email = rewrite($_POST['register_email']);
     $userType = "1";

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $phoneNo ."<br>";

     // $usernameRows = getRegistration($conn," WHERE username = ? ",array("username"),array($username),"s");
     // $usernameDetails = $usernameRows[0];

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {

               $usernameRows = getRegistration($conn," WHERE username = ? ",array("username"),array($username),"s");
               $usernameDetails = $usernameRows[0];

               if (!$usernameDetails)
               {
                    if(registerNewUser($conn,$uid,$username,$phoneNo,$email,$userType,$finalPassword,$salt))
                    {
                         echo "<script>alert('Register Success !');window.location='../liveChat.php'</script>";   
                    }
                    else
                    {
                         echo "<script>alert('fail to register register !');window.location='../liveChat.php'</script>";   
                    } 
               }
               else
               {
                    echo "<script>alert('registration data already taken by others !');window.location='../liveChat.php'</script>";   
               } 
          }
          else 
          {
               echo "<script>alert('password must be more than 6');window.location='../liveChat.php'</script>";
          }
     }
     else 
     {
          echo "<script>alert('password and retype password not the same');window.location='../liveChat.php'</script>";
     }  

     // if (!$usernameDetails)
     // {
     //      if(registerNewUser($conn,$uid,$username,$phoneNo,$email,$userType))
     //      {
     //           echo "<script>alert('Register Success !');window.location='../index.php'</script>";   
     //      }
     //      else
     //      {
     //           echo "<script>alert('fail to register register !');window.location='../registration.php'</script>";   
     //      } 
     // }
     // else
     // {
     //      echo "<script>alert('registration data already taken by others !');window.location='../registration.php'</script>";   
     // }     
}
else 
{
     header('Location: ../livechat.php');
}
?>