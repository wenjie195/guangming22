<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Liveshare.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];
$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $updatePlatform = rewrite($_POST["update_platform"]);
    $updateLink = rewrite($_POST["update_link"]);

    $userUid = rewrite($_POST["user_uid"]);

    $imageOne = $timestamp.$_FILES['image_one']['name'];

    // stop cos no use zoom
    // $target_dir = "../userProfilePic/";

    // Temporarily Image Link
    $target_dir = "../uploads/";

    $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
    // Select file type
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Valid file extensions
    $extensions_arr = array("jpg","jpeg","png","gif");
    if( in_array($imageFileType,$extensions_arr) )
    {
        move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
    }

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $userDetails = getUser($conn," uid = ?   ",array("uid"),array($userUid),"s");   
    
    if(!$liveIdDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($updatePlatform)
        {
            array_push($tableName,"platform");
            array_push($tableValue,$updatePlatform);
            $stringType .=  "s";
        }
        if($updateLink)
        {
            array_push($tableName,"link");
            array_push($tableValue,$updateLink);
            $stringType .=  "s";
        }

        // if($imageOne)
        // {
        //     array_push($tableName,"broadcast_share");
        //     array_push($tableValue,$imageOne);
        //     $stringType .=  "s";
        // }

        if($imageOne)
        {
            array_push($tableName,"title");
            array_push($tableValue,$imageOne);
            $stringType .=  "s";
        }

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "UPDATED !!";
            header('Location: ../adminDashboard.php');
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
