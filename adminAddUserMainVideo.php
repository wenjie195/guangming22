<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allUser = getUser($conn," WHERE user_type = '1' ");
$platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminAddUserMainVideo.php" />
<meta property="og:title" content="Add New Video in Home | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Add New Video in Home | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<link rel="canonical" href="https://gmvec.com/adminAddUserMainVideo.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <!-- <h2 class="h1-title">Add New Video (Unique Index)</h2>  -->
    <h2 class="h1-title">Add Teaser Video</h2> 
    
    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

        <!-- <form action="utilities/addNewUserMainVideoFunction.php" method="POST">  -->
        <form method="POST" action="utilities/addNewUserMainVideoFunction.php" enctype="multipart/form-data">
            <div class="dual-input">
                <p class="input-top-text">User</p>  
                <input class="aidex-input clean" type="text" value="<?php echo $userDetails[0]->getUsername();?>" name="register_user" id="register_user" readonly> 
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Title</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="Title" name="register_title" id="register_title" required>        -->
                <input class="aidex-input clean" type="text" placeholder="Title" name="register_title" id="register_title">     
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text">Host</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="Host Name" name="register_host" id="register_host" required>        -->
                <input class="aidex-input clean" type="text" placeholder="Host Name" name="register_host" id="register_host">     
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Platform</p>
                <select class="aidex-input clean" type="text" name="register_platform" id="register_platform" required>
                    <option value="">Please Select A Platform</option>
                    <?php
                    for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                    {
                    ?>
                        <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                            <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                        </option>
                    <?php
                    }
                    ?>
                </select> 
            </div>

            <div class="clear"></div>
            
            <div class="width100">
                <p class="input-top-text">Link</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="Link" name="register_link" id="register_link" required>        -->
                <input class="aidex-input clean" type="text" placeholder="Link" name="register_link" id="register_link">       
            </div>

            <div class="clear"></div> 

            <div class="width100">
                <p class="input-top-text">Temporarily Image</p>
                <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer" /></p>
            </div>

            <div class="clear"></div> 

            <input class="aidex-input clean" type="hidden" value="<?php echo $_POST['user_uid'];?>" name="user_uid" id="user_uid" readonly> 
        
            <div class="clear"></div> 

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>
    
<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>