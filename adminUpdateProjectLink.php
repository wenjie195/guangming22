<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminUpdateProjectLink.php" />
<meta property="og:title" content="Project Logo and Link | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Project Logo and Link | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminUpdateProjectLink.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Edit Project Logo Link</h2> 
    
    <div class="clear"></div>

        <?php
        if(isset($_POST['pic_link_uid']))
        {
        $conn = connDB();
        $projectDetails = getImage($conn,"WHERE uid = ? ", array("uid") ,array($_POST['pic_link_uid']),"s");
        ?>

            <form method="POST" action="utilities/editImageLinkFunction.php">

                <div class="width100 overflow margin-top30">
                    <p class="input-top-text">Link 1</p>
                    <input class="aidex-input clean" type="text" placeholder="Link 1" value="<?php echo $projectDetails[0]->getLinkOne();?>" id="update_link_one" name="update_link_one" required>        
                </div> 

                <div class="clear"></div>

                <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['pic_link_uid'];?>" id="pic_link_uid" name="pic_link_uid" readonly>  

                <div class="clear"></div>

                <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>

            </form>

        <?php
        }
        ?>

    <div class="clear"></div>
       
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>