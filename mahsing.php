

<!doctype html>
<html>

<head>


<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/mahsing.php" />
<link rel="canonical" href="https://gmvec.com/mahsing.php" />
<meta property="og:title" content="Mah Sing Group 馬星集團 | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Mah Sing Group 馬星集團 | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="keywords" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">



<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>

<div class="width100 same-padding overflow gold-bg min-height">
	<div class="width100 overflow text-center">
		<a href="https://www.mahsing.com.my/" target="_blank"><img src="img/mahsing.png" class="mah-size opacity-hover text-center" alt="Mah Sing Group 馬星集團" title="Mah Sing Group 馬星集團"></a>
	</div>
    <div class="width100 overflow margin-top30 first-div-margin">    
        
        <div class="width100 top-video-div overflow">


                            
                                <iframe class="youtube-top-iframe ow-top-iframe" src="https://www.youtube.com/embed/3lXggG62M1I?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                       

			<!--
            <div class="right-project-div">

                    
                        <a href="" target="_blank"><img src="uploads/" class="project-logo opacity-hover"></a>
            
                        
                                <a href="" target="_blank"><img src="uploads/" class="project-logo opacity-hover "></a>
                          
				</div>-->
        </div>
    </div>

    <div class="clear"></div>
    
    <div class="width100 overflow margin-top30">
    	

                        <div class="two-div  overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/EkMVnIr7HUA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                           
                                    <p class="gold-text four-div-p text-center"><a href="https://ferringhiresidence2.com/360.php" class="blue-to-orange" target="_blank">360°</a></b></p>
                              
                        </div>
                         <div class="two-div second-two-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/Hu4gvtoi-eg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                           
                                    <p class="gold-text four-div-p text-center"><a href="https://ferringhiresidence2.com/360.php" class="blue-to-orange" target="_blank">360°</a></b></p>
                              
                        </div>                  

                    
                   


</div>
</div>

<?php include 'js.php'; ?>

</body>
</html>