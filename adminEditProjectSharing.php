<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Sharing.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminEditProjectSharing.php" />
<meta property="og:title" content="Edit Sharing | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Edit Sharing  | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminEditProjectSharing.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Edit Booth/Zoom</h2>

    <div class="clear"></div>

    <?php
    if(isset($_POST['sharing_uid']))
    {
        $conn = connDB();
        $subDetails = getSharing($conn,"WHERE uid = ? ", array("uid") ,array($_POST['sharing_uid']),"s");
    ?>

        <form action="utilities/editSharingFunction.php" method="POST" enctype="multipart/form-data"> 
                            
            <div class="dual-input">
                <p class="input-top-text">Booth/Department Title</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getTitle();?>" name="update_title_one" id="update_title_one">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Staff Image(354px (w) X 216px (h)) : <a href="userProfilePic/<?php echo $subDetails[0]->getFile();?>" class="blue-to-orange" target="_blank"><?php echo $subDetails[0]->getFile();?></a></p>
                <p><input id="file-upload" type="file" name="file_one" id="file_one" class="margin-bottom10 pointer" /></p>
                <input class="aidex-input clean" type="hidden" value="<?php echo $subDetails[0]->getFile();?>" name="ori_file_one" id="ori_file_one">       
            </div>

            <div class="dual-input">
                <p class="input-top-text">Host</p>
                <!-- <input class="aidex-input clean" type="text" value="<?php //echo $subDetails[0]->getRemark();?>" name="update_host" id="update_host">        -->
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getRemark();?>" name="update_remark" id="update_remark">     
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Zoom Link</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getLink();?>" name="update_link" id="update_link">       
            </div>
<!--
            <div class="dual-input">
                <p class="input-top-text">Remark</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getRemark();?>" name="update_remark" id="update_remark">       
            </div>
-->
            <div class="clear"></div>  
            
            <input type="hidden" value="<?php echo $subDetails[0]->getUid();?>" name="sub_uid" id="sub_uid" readonly> 

            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>