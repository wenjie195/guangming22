-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 22, 2020 at 12:38 PM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gmveccom_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `uid`, `title`, `content`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '71dbfdf1997603d9610a3f9f90c45a1b', NULL, 'Announcement: We will have a live talk in 31st August 2020. Stay Tuned!', 'Available', 2, '2020-07-21 03:46:13', '2020-09-07 03:27:58'),
(2, '1362413c4842c918c87c09917a7a6227', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 1', 'Available', 1, '2020-07-21 03:46:39', '2020-07-21 03:46:39'),
(3, '5d4ad3b1b412f1aa01f73a5d4fb57c6f', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 2\r\n', 'Available', 1, '2020-07-21 03:46:46', '2020-07-21 03:46:46'),
(4, '6eb492ce0f4fe93827ee5d9c7ae882b6', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 3', 'Available', 1, '2020-07-21 03:46:51', '2020-07-21 03:46:51'),
(5, 'd14db52f8c57b997cef7c032bfdd323a', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 4\r\n', 'Available', 1, '2020-07-21 03:53:07', '2020-07-21 03:53:07'),
(6, '0cc59292c2308b7d2d41ef9cdcfe0d2e', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 5', 'Available', 1, '2020-07-21 03:53:14', '2020-07-21 03:53:14'),
(7, '6d30e124799c7d79293f91f5a60e393d', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 6', 'Available', 1, '2020-07-21 03:53:20', '2020-07-21 03:53:20'),
(8, '016d2bf60f79ce76cffa816301bc1ca3', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 7\r\n', 'Available', 1, '2020-07-21 03:53:34', '2020-07-21 03:53:34'),
(9, '422644553d984015b2ebd8d1f828e4ab', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 8', 'Available', 1, '2020-07-21 03:53:40', '2020-07-21 03:53:40');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text,
  `seo_title` text,
  `article_link` text,
  `keyword_one` text,
  `keyword_two` text,
  `title_cover` text,
  `paragraph_one` text,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text,
  `img_one_source` text,
  `img_two_source` text,
  `img_three_source` text,
  `img_four_source` text,
  `img_five_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `link_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `link_two` varchar(255) DEFAULT NULL,
  `type` int(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `uid`, `user_uid`, `image_one`, `link_one`, `image_two`, `link_two`, `type`, `date_created`, `date_updated`) VALUES
(1, '9a2257c9de18c44cabd3bb8a9ed46516', '923a1263cf3818339855fcf8f37cbef2', '1595304402.png', 'https://www.google.com/', NULL, NULL, 1, '2020-07-21 04:06:42', '2020-07-21 04:06:57'),
(2, '33db1d4c1546e4131d54e06a308bfb3f', '923a1263cf3818339855fcf8f37cbef2', '1595304434.png', 'http://www.ixftv.com/', NULL, NULL, 1, '2020-07-21 04:07:14', '2020-07-21 04:07:16'),
(3, '5ce0fe981215364c1fdc2bc5c33aff7a', '923a1263cf3818339855fcf8f37cbef2', '1595304446.png', 'https://www.amway.my/', NULL, NULL, 1, '2020-07-21 04:07:26', '2020-07-21 04:07:36'),
(4, 'b1730bcc9c1e0b9b8105099d06b9a0c6', '923a1263cf3818339855fcf8f37cbef2', '1595304474.png', 'https://guangming.com.my/', NULL, NULL, 1, '2020-07-21 04:07:54', '2020-07-21 04:07:56'),
(5, 'ca478b2068ada5c5b6a89d1016319bcd', 'e4bbb370d994a76f41c90a0b0d749ad8', '1599808355.png', 'https://www.zeon.com.my/projects/info?id=proje6fffdde-dde6-48c3-8c85-b1e99b8cee50', NULL, NULL, 1, '2020-09-11 07:12:35', '2020-09-11 07:12:52'),
(6, 'f5062934c85d967b52e729cfe44741ea', '632372877da43ab6f7d8af2a73f2e0b3', '1600081052.png', 'https://instagram.com/mahsinggroup?igshid=wx5b1q41gl9o', NULL, NULL, 1, '2020-09-14 10:57:32', '2020-09-21 06:42:12'),
(7, '80ef99d5e5e3f9dd6e96d73f8a887c46', '632372877da43ab6f7d8af2a73f2e0b3', '1600081109.png', 'https://instagram.com/mahsinggroup?igshid=wx5b1q41gl9o', NULL, NULL, 1, '2020-09-14 10:58:29', '2020-09-21 06:42:45'),
(8, '1f93284da254c33906c1006cbe0ed421', 'e4bbb370d994a76f41c90a0b0d749ad8', '1600081595.png', 'https://www.facebook.com/CityofDreamsPG/', NULL, NULL, 1, '2020-09-14 11:06:35', '2020-09-14 11:08:32'),
(9, '50f20149e1c8876ecb293ca80c175985', 'e4bbb370d994a76f41c90a0b0d749ad8', '1600081726.png', 'https://www.eweinberhad.com/', NULL, NULL, 1, '2020-09-14 11:08:46', '2020-09-14 11:08:53'),
(10, '6b8951aa79f82189b06e490c4cff3659', 'b3691c35cea1dfed6274227f67744e80', '1600167631.png', 'http://www.hunzagroup.com/', NULL, NULL, 1, '2020-09-15 11:00:31', '2020-09-15 11:02:41'),
(11, '29ecb44718448c86c95df317384f4f4d', 'b3691c35cea1dfed6274227f67744e80', '1600167827.png', 'http://www.picc-penang.com/index.php', NULL, NULL, 1, '2020-09-15 11:03:47', '2020-09-15 11:03:53'),
(12, 'b91d08210dd2dc3572e9ea68197f0908', 'b3691c35cea1dfed6274227f67744e80', '1600167864.png', 'https://www.treeo-hunza.com/', NULL, NULL, 1, '2020-09-15 11:04:24', '2020-09-15 11:04:27'),
(13, 'd15e210a2c480bf75e1af0cd63cee38a', 'b3691c35cea1dfed6274227f67744e80', '1600167911.png', 'http://www.mekarsari.com.my/', NULL, NULL, 1, '2020-09-15 11:05:11', '2020-09-15 11:05:15'),
(14, '7b6b0d81e48346e63e21b1f0e8f66934', 'b3691c35cea1dfed6274227f67744e80', '1600167962.png', 'https://www.alila.com.my/', NULL, NULL, 1, '2020-09-15 11:06:02', '2020-09-15 11:06:05'),
(15, '3a4346f20fa5f93ed1d94df7fd77b9e4', '3942b0430fbe8374db84b3aa7f1d9831', '1600168544.png', 'https://www.berjaya.com/berjaya-land/', NULL, NULL, 1, '2020-09-15 11:15:44', '2020-09-15 11:16:04'),
(16, 'acdbd6120b26c969c901f9d3e7a259ba', '3942b0430fbe8374db84b3aa7f1d9831', '1600169030.png', 'https://www.jadigroup.com/', NULL, NULL, 3, '2020-09-15 11:23:50', '2020-09-15 11:26:52'),
(17, '36f597d8b791f672afca431642dff375', '9619819c11170ebf2f4785ca71e9d4bf', '1600169237.png', 'https://www.jadigroup.com/', NULL, NULL, 1, '2020-09-15 11:27:17', '2020-09-15 11:27:19'),
(18, '82f66b8ac479bbcf828ce3eacf56f679', '9619819c11170ebf2f4785ca71e9d4bf', '1600169274.png', 'https://www.tambunroyalecity.com/royale-infinity/', NULL, NULL, 1, '2020-09-15 11:27:54', '2020-09-15 11:27:57'),
(19, '98cdc1427c01fb5430346978af0e9ab9', '9619819c11170ebf2f4785ca71e9d4bf', '1600169290.png', 'https://www.tambunroyalecity.com/', NULL, NULL, 1, '2020-09-15 11:28:10', '2020-09-15 11:32:38'),
(20, '25bc6ea4a204f3b500becc75309577be', '9619819c11170ebf2f4785ca71e9d4bf', '1600169361.png', 'https://www.tambunroyalecity.com/royale-heights/', NULL, NULL, 1, '2020-09-15 11:29:21', '2020-09-15 11:32:15'),
(21, '4b80c018ffbd9db04031b5a71b830672', '9619819c11170ebf2f4785ca71e9d4bf', '1600169491.png', 'https://www.jadigroup.com/portfolio-item/bm-highland/', NULL, NULL, 1, '2020-09-15 11:31:31', '2020-09-15 11:31:44'),
(22, 'd9a3680075f5e154bbe71a077a5c75bc', 'e4f4785e35b9623455e78794eac15511', '1600169906.png', 'http://www.tahwah.com/TWProjectD.asp?id=25', NULL, NULL, 1, '2020-09-15 11:38:26', '2020-09-15 11:39:22'),
(23, 'ef6ee09fb3aabfd8580525705e700433', 'e4f4785e35b9623455e78794eac15511', '1600170061.png', 'http://www.tahwah.com/TWProjectD.asp?id=18', NULL, NULL, 1, '2020-09-15 11:41:01', '2020-09-15 11:41:26'),
(24, '028ad2a36d20ad52eabf6f39055d30b3', 'e4f4785e35b9623455e78794eac15511', '1600744386.png', 'https://www.facebook.com/pages/category/Real-Estate-Developer/Orange-Residence-2438039779574418/', NULL, NULL, 1, '2020-09-15 11:45:21', '2020-09-22 03:13:06'),
(25, '18720759739d64cac739a784644dc4fe', 'e4f4785e35b9623455e78794eac15511', '1600170415.png', 'http://www.tahwah.com/', NULL, NULL, 1, '2020-09-15 11:46:55', '2020-09-15 11:47:06'),
(26, '6344fe3f4f99f78b75aab60ce13e3a43', 'e4f4785e35b9623455e78794eac15511', '1600341519.png', 'http://www.tahwah.com/TWProjectD.asp?id=20', NULL, NULL, 1, '2020-09-17 11:18:39', '2020-09-17 11:18:59'),
(27, 'c0ecbf2fc41458893eaea136f435f699', 'c22aa05c66ccf407b9172c26c9907358', '1600673035.png', 'https://aspen.com.my/', NULL, NULL, 1, '2020-09-17 11:21:34', '2020-09-21 07:23:55'),
(28, '749b630b62cebc2bd38d73cd1a051f42', 'c22aa05c66ccf407b9172c26c9907358', '1600672703.png', 'https://vivo-avc.com/', NULL, NULL, 1, '2020-09-21 06:44:38', '2020-09-21 07:18:23'),
(29, '9e36034332d9ccc8f0c6a1d57b451eb4', 'c22aa05c66ccf407b9172c26c9907358', '1600670803.png', 'https://vogue-lifestyle-residence.com/', NULL, NULL, 3, '2020-09-21 06:46:43', '2020-09-21 07:17:24'),
(30, '5da439a617eb1394028dbcc3130f15e3', 'c22aa05c66ccf407b9172c26c9907358', '1600671153.png', 'https://viluxe.com.my/', NULL, NULL, 3, '2020-09-21 06:52:33', '2020-09-21 07:17:09'),
(31, 'cd5dc0d0650b35ef8a071d58219deae6', 'c22aa05c66ccf407b9172c26c9907358', '1600672727.png', 'http://verturesort.com/', NULL, NULL, 1, '2020-09-21 06:53:46', '2020-09-21 07:18:47'),
(32, 'c854fe34e8371ff230df056a34d08b61', 'c22aa05c66ccf407b9172c26c9907358', '1600671637.png', 'https://tri-pinnacle.com/', NULL, NULL, 3, '2020-09-21 07:00:37', '2020-09-21 07:17:11'),
(33, 'cbe17e18a51246f28c87da460651f9e2', 'c22aa05c66ccf407b9172c26c9907358', '1600671707.png', 'http://hhpark.com.my/', NULL, NULL, 3, '2020-09-21 07:01:47', '2020-09-21 07:17:12'),
(34, '904af3bd09af29904cc0482b79f28ae3', 'c22aa05c66ccf407b9172c26c9907358', '1600672754.png', 'https://www.beacon-suites.com/', NULL, NULL, 1, '2020-09-21 07:19:14', '2020-09-21 07:19:36');

-- --------------------------------------------------------

--
-- Table structure for table `live_share`
--

CREATE TABLE `live_share` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `live_share`
--

INSERT INTO `live_share` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '4d1bbbed461a9ec43995d606504a52eb', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'City of Dream Penang', 'Ewein Properties', 'Youtube', 'LFzduCN2QWk', 'Available', 1, '2020-09-11 07:11:48', '2020-09-11 07:11:48'),
(2, '1faa1f4f2ef38a04072647e32b0d3910', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Ferringhi Residence 2 Show Gallery Tour', 'Mah Sing Group', 'Youtube', '3lXggG62M1I', 'Available', 1, '2020-09-14 09:39:33', '2020-09-14 09:39:33'),
(3, '8a7400bf2233688947afffb31f81b85c', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Hunza', 'Hunza', 'Youtube', 'FLSRWnJVnJ8', 'Available', 1, '2020-09-18 10:22:43', '2020-09-18 10:22:43'),
(4, '30ff0d14bfef69e18a95536dd311075b', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', '', '', 'Temporarily', '', 'Delete', 3, '2020-09-21 03:46:16', '2020-09-21 03:46:16'),
(5, 'cecb3265fecfe28a5c895cf9bce45778', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Royal Height', '', 'Temporarily', '', 'Delete', 3, '2020-09-21 03:53:43', '2020-09-21 03:53:43'),
(6, 'c9d098df060138f3780a986ac532cf2e', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', '', '', 'Temporarily', 'Club house-day-e2.jpg', 'Delete', 3, '2020-09-21 04:59:10', '2020-09-21 04:59:10'),
(7, '2ef2eb03c070cae43dc15da15f9b9a02', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen Img 1', 'Aspen Group', 'Temporarily', 'Royale Heights - DSTH OPTION 1.jpg', 'Delete', 3, '2020-09-21 05:01:53', '2020-09-21 05:01:53'),
(8, '91df29724cebd017839d8e4164357891', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', '', '', 'Temporarily', 'rsz_vivo_webimage-1.jpg', 'Delete', 3, '2020-09-21 05:19:19', '2020-09-21 05:19:19'),
(10, 'b2251b0c621e18cbd4b5bbe041cc358f', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Tmn Jadi  G1', 'Tmn Jadi  G1', 'Temporarily', 'Leon Lee.jpeg', 'Delete', 3, '2020-09-21 05:35:25', '2020-09-21 05:35:25'),
(11, 'cd5355a83db717762d4884a2e74c3848', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', '', '', 'Temporarily', '2str ter-e12.jpg', 'Delete', 3, '2020-09-21 06:12:47', '2020-09-21 06:12:47'),
(12, '6ddc50d01f61aa55f2648ddb50d12fc0', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', '2str-ter-e12.jpg', 'Available', 1, '2020-09-21 06:15:03', '2020-09-21 06:15:03'),
(13, '2efcea7d7b44a63dc1aed1774a4f95f3', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen', 'Aspen', 'Temporarily', 'rsz_vivo_webimage-1.jpg', 'Delete', 3, '2020-09-21 06:20:26', '2020-09-21 06:20:26'),
(14, 'b2ba4f34d18117bff89ea8ea3831d4d8', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'slider-1.jpg', 'Delete', 3, '2020-09-21 06:21:45', '2020-09-21 06:21:45'),
(15, '29e5e534f0c11997fbea5c2e91b374d6', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'Artboard 2-100.jpg', 'Delete', 3, '2020-09-21 06:34:10', '2020-09-21 06:34:10'),
(16, '19b4006ee7f81afc42dc0f1cdf34f0d7', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'Artboard-2-100s.jpg', 'Delete', 3, '2020-09-21 06:36:34', '2020-09-21 06:36:34'),
(17, '26d018171c5225edb7114698136742ea', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'Artboard-2-100s2.jpg', 'Delete', 3, '2020-09-21 06:38:44', '2020-09-21 06:38:44'),
(18, 'f3b42a8beeb8ccccc61242a4435dc61d', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen', 'Aspen', 'Temporarily', 'rsz_vivo_webimage-1.jpg', 'Delete', 3, '2020-09-21 07:38:04', '2020-09-21 07:38:04'),
(19, '3ac79e9b72e697993146d473ac2a57ed', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen', 'Aspen', 'Temporarily', 'rsz_vivo_webimage-1a.jpg', 'Delete', 3, '2020-09-21 07:39:23', '2020-09-21 07:39:23'),
(20, '1728369f96d6fabe7b76368462242b67', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Temporarily', 'Royale-Infinity22.jpg', 'Delete', 3, '2020-09-21 07:56:35', '2020-09-21 07:56:35'),
(21, '1dfff7e7cceedd6372fba22f7b919aa0', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Temporarily', 'Royale-Infinity22a.jpg', 'Available', 1, '2020-09-21 07:58:04', '2020-09-21 07:58:04'),
(22, 'b8cdbd533146f3c617ef251c03a35d49', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'Artboard-2-100s2aa.jpg', 'Available', 1, '2020-09-21 08:04:48', '2020-09-21 08:04:48'),
(23, 'e2029197d6afdfec1370d45d5c641d94', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen', 'Aspen', 'Youtube', '_Jv5-43YWI0', 'Available', 1, '2020-09-21 09:42:56', '2020-09-21 09:42:56');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `message_uid` varchar(255) DEFAULT NULL,
  `receive_message` varchar(255) DEFAULT NULL,
  `reply_message` varchar(255) DEFAULT NULL,
  `reply_one` varchar(255) DEFAULT NULL,
  `reply_two` varchar(255) DEFAULT NULL,
  `reply_three` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, READ = user read sms',
  `admin_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, REPLY = admin reply sms',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

CREATE TABLE `platform` (
  `id` bigint(20) NOT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` (`id`, `platform`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Youtube', 'Available', 1, '2020-04-21 08:00:50', '2020-04-21 08:00:50'),
(2, 'Facebook', 'Available', 1, '2020-04-21 08:01:55', '2020-04-21 08:01:55'),
(3, 'Zoom', 'Stop', 2, '2020-04-21 08:05:41', '2020-04-21 08:05:41'),
(4, 'UStream', 'Stop', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51'),
(5, 'Temporarily', 'Available', 1, '2020-09-21 03:16:03', '2020-09-21 03:16:03');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sharing`
--

CREATE TABLE `sharing` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` text,
  `remark` text,
  `file` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sharing`
--

INSERT INTO `sharing` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `remark`, `file`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '0568ec8f367619c2b88de85bddbbefbb', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Mah Sing Booth 1', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Staff 1', '1600078274staff1.jpg', 'Delete', 3, '2020-09-14 10:11:14', '2020-09-14 10:11:14'),
(2, '3f2cbf179332fa20966a0566aea76c34', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Mah Sing Booth 2', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Staff 2', '1600078300staff2.jpg', 'Delete', 3, '2020-09-14 10:11:40', '2020-09-14 10:11:40'),
(3, '0c53d3ea681f4ac8201f835ed1e3a1af', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Master of Project', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Vincent Koay 郭洺神', '1600080566vincent.jpg', 'Delete', 3, '2020-09-14 10:49:26', '2020-09-14 10:49:26'),
(4, '0b175cc1fd8a2f644ac2306b728f9d45', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Business Development', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Carol Cheah 谢晓双', '1600080699carol.jpg', 'Delete', 3, '2020-09-14 10:51:39', '2020-09-14 10:51:39'),
(5, '419b6ef81d8f400e6b170765e47c0c20', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Master of Project', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Vincent Koay 郭洺神', '1600081216vincent.jpg', 'Delete', 3, '2020-09-14 11:00:16', '2020-09-14 11:00:16'),
(6, '3ef00204e5e1cf470ecba8e2d73146d4', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Business Development', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Carol Cheah 谢晓双', '1600081249carol.jpg', 'Delete', 3, '2020-09-14 11:00:49', '2020-09-14 11:00:49'),
(7, '2f562529389d7bfd0ecf2a7b0e435c55', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Assistant Manager Sales and Marketing', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Allycia Ng', '1600748185Allycia Ng - Assistant Manager Sales and Marketing.jpg', 'Available', 1, '2020-09-14 11:05:28', '2020-09-14 11:05:28'),
(8, '126b5854cc6d607ed0b07cde27798e7e', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售助理经理  Assistant Manager Sales and Marketing', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '吕汶霓  Cheryl Loo', '1600748225Cheryl Loo - Assistant Manager Sales and Marketing.jpg', 'Available', 1, '2020-09-14 11:12:35', '2020-09-14 11:12:35'),
(9, '7a9a62150b7693e75c454ea6c4342509', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售高級经理  Senior Manager Sales and Marketing', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '周芷芸  Chew Zhi Ying', '1600748246Chew Zhi Ying - Senior Manager Sales and Marketing.jpg', 'Available', 1, '2020-09-14 11:15:18', '2020-09-14 11:15:18'),
(10, 'a3161c89ff2a07393e7e6a4bf4642079', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Assistant Manager Sales and Marketing', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Jesphin Goh', '1600082265jesphin.jpg', 'Delete', 3, '2020-09-14 11:17:45', '2020-09-14 11:17:45'),
(12, '53834cd15e9906705104b57ff38ae86f', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售高級经理  Senior Manager Sales and Marketing', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '张嘉薏  Joey Teoh Kar Yee', '1600082459joeyteoh.jpg', 'Available', 1, '2020-09-14 11:20:59', '2020-09-14 11:20:59'),
(14, '7e61c053fa255c62b01880a076d687f7', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Assistant Manager Sales and Marketing', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Jesphin Goh', '1600082827jesphin.jpg', 'Available', 1, '2020-09-14 11:27:07', '2020-09-14 11:27:07'),
(15, '3c5ab31fffa64c587717a27c25569ad2', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售助理经理  Assistant Manager Sales and Marketing', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '林晓琪  Lim Shiau Qi', '1600748293Lim Shiau Qi - Assistant Manager Sales and Marketing.jpg', 'Available', 1, '2020-09-14 11:27:32', '2020-09-14 11:27:32'),
(16, 'deb4f58ee57742ee3b7327bfab93f3d6', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Mah Sing Property Advisor 产业销售顾问', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Shawn 方志松', '1600425805Shawn-Hong,-Mah-Sing-Property-Advisor-.jpg', 'Available', 1, '2020-09-18 10:43:25', '2020-09-18 10:43:25'),
(17, 'b52e2f809ab3c7f1eeb5f7ffa3784fac', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Mah Sing Property Advisor 产业销售顾问', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Alan 谢其伦', '1600425844Alan-Cheah,-Mah-Sing-Property-Advisor-.jpg', 'Available', 1, '2020-09-18 10:44:04', '2020-09-18 10:44:04'),
(18, '1574ba13ae6343776d47a04db70a23c6', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Senior Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Katherine Cheang', '1600483641Katherine-Cheang3.jpg', 'Available', 1, '2020-09-19 02:47:22', '2020-09-19 02:47:22'),
(19, 'ea27c1d94b1f82faa0c6d620ee8963ad', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Senior Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Ang Khwang Harn', '1600483680Ang-Khwang-Harn2.jpg', 'Available', 1, '2020-09-19 02:48:00', '2020-09-19 02:48:00'),
(20, '013f71749e7f935467b3cfa28afec2dd', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Ng Wei Ping', '1600483706Ng-Wei-Ping2.jpg', 'Available', 1, '2020-09-19 02:48:26', '2020-09-19 02:48:26'),
(21, '8de7fbbf2c73a380bb474358a3d01e4e', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Kellie Leen', '1600483734Kellie-Leen2.jpg', 'Available', 1, '2020-09-19 02:48:54', '2020-09-19 02:48:54'),
(22, '5a8ae874afa312426dec10ddfd1bd6a2', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Dickson Lau', '1600483760Dickson-Lau2.jpg', 'Available', 1, '2020-09-19 02:49:20', '2020-09-19 02:49:20'),
(23, 'a4c6caa0cbbd8864650e0ba1d3a07a16', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Director Business Development 业务发展总监', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Vincent Koay 郭洺神', '16006518471600080566vincent.jpg', 'Available', 1, '2020-09-21 01:30:47', '2020-09-21 01:30:47'),
(24, '40eb35da467e4591978943b31dea33da', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Director Business Development 业务发展总监', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Carol Cheah 谢晓双', '16006518901600080699carol.jpg', 'Available', 1, '2020-09-21 01:31:30', '2020-09-21 01:31:30'),
(25, '08362a4311821d22c140fe193937f312', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', '-', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'PICC', '1600657846picc-tower.jpg', 'Delete', 3, '2020-09-21 03:10:46', '2020-09-21 03:10:46'),
(26, '37ec9c1bf6d4ccd5c08e6e7c0bf1a607', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', '-', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Muze Grand Entrance', '160065795001 Muze Grand Entrance.jpg', 'Delete', 3, '2020-09-21 03:12:30', '2020-09-21 03:12:30'),
(28, '5ab04340ff78478ed9d0942db7d0e9f6', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', '-', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'PICC Medical Center', '160066262403 PICC Medical Center.jpg', 'Delete', 3, '2020-09-21 04:30:24', '2020-09-21 04:30:24'),
(29, 'ac3d664ed9a0d2630afa427048d7f5c8', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', '-', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'BBQ Area', '1600664850staff1.jpg', 'Delete', 3, '2020-09-21 05:07:30', '2020-09-21 05:07:30'),
(30, 'de33d8ab95734c6203469d1f9f5712d2', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', '-', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Swimming Pool', '1600664881staff2.jpg', 'Delete', 3, '2020-09-21 05:08:01', '2020-09-21 05:08:01'),
(31, 'd79478855bb7be8a10cfd277f8105b44', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Sales Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665000booth.png', 'Stop', 2, '2020-09-21 05:10:00', '2020-09-21 05:10:00'),
(32, '8119abd7b8d1d94e1ee82b5456c12d5b', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Marketing Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665052booth.png', 'Stop', 2, '2020-09-21 05:10:52', '2020-09-21 05:10:52'),
(33, 'cdee25f87b3d0fb16d1f31f059488836', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Business Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665083booth.png', 'Stop', 2, '2020-09-21 05:11:23', '2020-09-21 05:11:23'),
(34, 'b1a25f825b1d9caee42a245d37db20e9', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665161booth.png', 'Stop', 2, '2020-09-21 05:12:41', '2020-09-21 05:12:41'),
(35, '0efad2c2b73bb7c86a13ee135c3c8010', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Marketing Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665182booth.png', 'Stop', 2, '2020-09-21 05:13:02', '2020-09-21 05:13:02'),
(36, 'e171bda4bdced4a12598171d7fc2f200', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Business Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665200booth.png', 'Stop', 2, '2020-09-21 05:13:20', '2020-09-21 05:13:20'),
(37, '09e931e3e441ec2779aa050495309581', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665261booth.png', 'Stop', 2, '2020-09-21 05:14:21', '2020-09-21 05:14:21'),
(38, '251d0bf65d674447bc1e4ffa626758cc', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Marketing Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665285booth.png', 'Stop', 2, '2020-09-21 05:14:45', '2020-09-21 05:14:45'),
(39, '5963b4bb12c09dd7e464e3fadcac32c1', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Business Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665305booth.png', 'Stop', 2, '2020-09-21 05:15:05', '2020-09-21 05:15:05'),
(40, 'fc5d990345525a7796bf4ed675a9d385', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Sales Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665393booth.png', 'Stop', 2, '2020-09-21 05:16:33', '2020-09-21 05:16:33'),
(41, '2b959fa14904991abcf892adf5e315d3', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Marketing Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665415booth.png', 'Stop', 2, '2020-09-21 05:16:55', '2020-09-21 05:16:55'),
(42, '4d90c5e18e0c9c07c06a5770a531c8a4', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Business Dept', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665436booth.png', 'Stop', 2, '2020-09-21 05:17:16', '2020-09-21 05:17:16'),
(43, '334d8b6bf58c76bd4d81b8d5b36f812a', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Senior Sales Manager', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Daniel Ng 黄添俊', '1600673642daniel.jpg', 'Available', 1, '2020-09-21 07:34:02', '2020-09-21 07:34:02'),
(44, '13c420d16694fba842f3f5274c6b3a2c', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Sales Consultant', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Chuah Thean Cheat 蔡展捷', '1600673742chua.jpg', 'Available', 1, '2020-09-21 07:35:42', '2020-09-21 07:35:42'),
(45, '64cac3ea89f95afd67115c9661379bfc', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Area Sales Manager', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Doris Ericia 吴佳怡', '1600673775doris.jpg', 'Available', 1, '2020-09-21 07:36:15', '2020-09-21 07:36:15'),
(46, '7a07cbe87b248b797ba274c8fa5d3ad8', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Sales Consultant', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Ashley Wee 黄靖琇', '1600673798ashley.jpg', 'Available', 1, '2020-09-21 07:36:38', '2020-09-21 07:36:38'),
(47, 'ab056a73ef919ab2dac670e6bce9e202', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Senior Sales Consultant', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Enson Lim 林成发', '1600673826enson.jpg', 'Delete', 3, '2020-09-21 07:37:06', '2020-09-21 07:37:06'),
(48, 'b2f2c9c0ba703f1590033d6416a73b66', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Specialist 销售专员', '', '', 'https://api.whatsapp.com/send?phone=60182333927', 'JEN', '1600745267Jen.jpg', 'Available', 1, '2020-09-22 03:27:47', '2020-09-22 03:27:47'),
(49, '23035d68eb654cd2a097f1bb9b65aff6', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Specialist 销售专员', '', '', 'https://api.whatsapp.com/send?phone=60183668223', 'JO ANN', '1600745300Jo Ann.jpg', 'Available', 1, '2020-09-22 03:28:20', '2020-09-22 03:28:20'),
(50, '236d09da68bdb85600036e93407dd81d', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Specialist 销售专员', '', '', 'https://api.whatsapp.com/send?phone=60183701991', 'EUNICE', '1600745320Eunice.jpg', 'Available', 1, '2020-09-22 03:28:40', '2020-09-22 03:28:40'),
(51, '915b4ea0f019fd1bc9e61fe9b06aff34', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Senior Sales &amp; Marketing Executive', '', '', 'https://api.whatsapp.com/send?phone=60194184663', 'Ooi Lai Teik', '1600746373Ooi Lai Teik.jpg', 'Available', 1, '2020-09-22 03:46:14', '2020-09-22 03:46:14'),
(52, '7710a4bf513db62755ccefd650b3437e', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Senior Sales &amp; Marketing Executive', '', '', 'https://api.whatsapp.com/send?phone=60134284663', 'Tan Kher Suan', '1600746420Tan Kher Suan.jpg', 'Available', 1, '2020-09-22 03:47:00', '2020-09-22 03:47:00'),
(53, '88511df01e2258a383a490cba32e7595', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Sales &amp; Marketing Executive', '', '', 'https://api.whatsapp.com/send?phone=60134874663', 'Vicky Tan', '1600746453Vicky Tan.jpg', 'Available', 1, '2020-09-22 03:47:33', '2020-09-22 03:47:33'),
(54, 'f032ea38f89ace571b89de3c7e409a50', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '林瑞端 Joselyn Lim', '1600746749___ (Joselyn Lim).jpg', 'Available', 1, '2020-09-22 03:52:29', '2020-09-22 03:52:29'),
(55, 'd9ef4e7022a2cf19cfc8ca2ad273b6e9', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '曾垂聪 Allan Chan', '1600746802___ (Allan Chan).jpg', 'Available', 1, '2020-09-22 03:53:22', '2020-09-22 03:53:22'),
(56, 'b8a321d971d28856f4d466288788c44c', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '沈彩颜 Vivian Sim', '1600746837___ (Vivian Sim).jpg', 'Available', 1, '2020-09-22 03:53:57', '2020-09-22 03:53:57'),
(57, 'fd6f6095a3291ad1e5141958233d3117', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '谢瑞媚  Swee Mi', '1600746867___  (Swee Mi).jpg', 'Available', 1, '2020-09-22 03:54:27', '2020-09-22 03:54:27'),
(58, '056904639b71d5cd98f5156077c92435', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '吴志勤 Sunny Goh', '1600746907___  ( Sunny Goh ).jpg', 'Available', 1, '2020-09-22 03:55:07', '2020-09-22 03:55:07'),
(59, '313e7a9bb2b6190874de0ef7076a1eee', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '林炜阳 Lim Wei Yang', '1600746939___ ( Lim Wei Yang ).jpg', 'Available', 1, '2020-09-22 03:55:39', '2020-09-22 03:55:39'),
(60, 'c70a6798cdcecb42e82fbfbc7c6712bf', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '林荣祖 Matthew Lim', '1600746971___ (Matthew Lim).jpg', 'Available', 1, '2020-09-22 03:56:11', '2020-09-22 03:56:11'),
(61, '7bb2a0664ada86e3c4b74db4b8bed306', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '周小彦 Vivian Chow', '1600747077___  (Vivian Chow).jpg', 'Available', 1, '2020-09-22 03:57:57', '2020-09-22 03:57:57'),
(62, '8c541d7aaf88b5baa220a7dead148844', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '王征熙 Ivan Ong', '1600747101___ (Ivan Ong).jpg', 'Available', 1, '2020-09-22 03:58:21', '2020-09-22 03:58:21');

-- --------------------------------------------------------

--
-- Table structure for table `sub_share`
--

CREATE TABLE `sub_share` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` text,
  `remark` text,
  `file` varchar(255) DEFAULT NULL,
  `title_two` varchar(255) DEFAULT NULL,
  `host_two` varchar(255) DEFAULT NULL,
  `platform_two` varchar(255) DEFAULT NULL,
  `link_two` text,
  `remark_two` text,
  `file_two` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_share`
--

INSERT INTO `sub_share` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `remark`, `file`, `title_two`, `host_two`, `platform_two`, `link_two`, `remark_two`, `file_two`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '9b79e78e32e0214e860cdb89b5293e36', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'City of Dream Penang', 'Ewein Properties', 'Youtube', 'LFzduCN2QWk', 'https://my.matterport.com/show/?m=WZfy6SNApdB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Stop', 2, '2020-09-11 07:15:14', '2020-09-11 07:15:14'),
(2, '0da47871acb2521b35016a8472ab963d', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Ferringhi Residence 2 with VO HD 1080p', 'Mah Sing Group', 'Youtube', 'EkMVnIr7HUA', 'https://ferringhiresidence2.com/360.php', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-09-14 09:41:23', '2020-09-14 09:41:23'),
(3, '11a07fb014a095b60663ebd555d6345c', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Ferringhi Residence 2 with facilities', 'Mah Sing Group', 'Youtube', 'Hu4gvtoi-eg', 'https://ferringhiresidence2.com/360.php', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-09-14 09:42:05', '2020-09-14 09:42:05'),
(4, '55b0555488c08356ab77b6683f045207', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'PICC', 'Hunza', 'Youtube', 'FLSRWnJVnJ8', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Stop', 2, '2020-09-18 10:23:19', '2020-09-18 10:23:19'),
(5, 'a298cae922e091b303349faaeb7d1226', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'PICC', 'Hunza', 'Youtube', 'GjypSzfkE3c', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-09-18 10:24:05', '2020-09-18 10:24:05'),
(6, 'c175acbb4f8d05020807032d48bfe578', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Orange BM Night', '', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Delete', 3, '2020-09-21 04:02:38', '2020-09-21 04:02:38'),
(7, '57881803a3e044ba3f9d26926f8e040f', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Royale Heights', '', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Delete', 3, '2020-09-21 04:03:18', '2020-09-21 04:03:18'),
(8, '7a802af889b8ac9fd97456854af3991d', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', '1', 'Tah Wah', 'Temporarily', '', '', '16006617532str-bung-CD1 small.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Delete', 3, '2020-09-21 04:15:53', '2020-09-21 04:15:53'),
(13, '216dac7771cf383e2900afc916627b33', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah Test 1', 'Tah Wah Test 1', 'Temporarily', 'Tah Wah Test Link 1', '360', '', NULL, NULL, NULL, NULL, NULL, '1600671661Stephen Kam.JPG', 'Delete', 3, '2020-09-21 07:01:01', '2020-09-21 07:01:01'),
(14, 'e16766a6eb6143d98db2319496441c97', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'HH Park Residence', 'Aspen Group', 'Youtube', 's9rKZB3Vj_g', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Stop', 2, '2020-09-21 07:04:09', '2020-09-21 07:04:09'),
(15, 'd4cd5fd8442e5fa574bf6473b6db029d', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Tri Pinnacle', 'Aspen Group', 'Temporarily', '', 'https://my.matterport.com/show/?m=PXbwhSJzzq5', '', NULL, NULL, NULL, NULL, NULL, '1600671968tri.jpg', 'Stop', 2, '2020-09-21 07:06:08', '2020-09-21 07:06:08'),
(16, '406947a43f074a30d4a581dca6090645', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD1', 'Ewein Properties1', 'Temporarily', '', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160067218214007_COD_Lobby V5_20190822.jpg', 'Available', 1, '2020-09-21 07:09:42', '2020-09-21 07:09:42'),
(17, '23124ddcbe6b9625108912f15cdc6e3e', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD', 'Ewein Properties', 'Temporarily', '', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160067223214007_COD_Lobby V1A_20191218.jpg', 'Available', 1, '2020-09-21 07:10:32', '2020-09-21 07:10:32'),
(18, 'c455a0d549f35a5e283f486ee7e688ec', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD', 'Ewein Properties', 'Temporarily', '', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160067225414007_COD_Lobby V4a_20190822.jpg', 'Available', 1, '2020-09-21 07:10:54', '2020-09-21 07:10:54'),
(19, '2ca32bf90b2ff7ebc5f6fd3eb7f5bf41', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Vertu Resort', 'Aspen Group', 'Temporarily', '_Jv5-43YWI0', '', '', NULL, NULL, NULL, NULL, NULL, '1600681413vertu.jpg', 'Available', 1, '2020-09-21 07:15:20', '2020-09-21 07:15:20'),
(20, '7bbea90c024d1f15083b2b385026952c', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD', 'Ewein Properties', 'Temporarily', '', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160067417914007_COD Ballroom V1_20200219.jpg', 'Available', 1, '2020-09-21 07:42:59', '2020-09-21 07:42:59'),
(21, 'a31a908cc0b9224c8226a4ef9370cadf', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Delete', 3, '2020-09-21 07:48:40', '2020-09-21 07:48:40'),
(22, '7b017c119e97082cb69fc52b3181bc67', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '1600674554Club house-day-e2.jpg', 'Available', 1, '2020-09-21 07:49:14', '2020-09-21 07:49:14'),
(23, 'e3334992cf706a4a5dc2bd05596cfb3f', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '16006747582str-bung-CD1 small.jpg', 'Available', 1, '2020-09-21 07:52:38', '2020-09-21 07:52:38'),
(24, '02d5bbe905b50ba227d2eb08c36380fa', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '1600674843Pool-morning-e13.jpg', 'Available', 1, '2020-09-21 07:54:03', '2020-09-21 07:54:03'),
(25, '55751cd87a7c79ebb7c8587fa11d6b14', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '16006748733str Semi-D.jpg', 'Available', 1, '2020-09-21 07:54:33', '2020-09-21 07:54:33'),
(26, '5046eecc1b6551cca4c6c1675be43a41', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '1600675123staff1.jpg', 'Available', 1, '2020-09-21 07:58:43', '2020-09-21 07:58:43'),
(27, 'a0cc29ebd124e62e7a6cbf6ebbaa9886', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '1600675137staff2.jpg', 'Available', 1, '2020-09-21 07:58:57', '2020-09-21 07:58:57'),
(28, '82612d0025b4e04edad9c425114dc769', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '1600675152staff3.jpg', 'Stop', 2, '2020-09-21 07:59:12', '2020-09-21 07:59:12'),
(29, '8d3a07069b1293fdb484fe14898f4b6d', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'PICC', 'Hunza', 'Facebook', '923178258168840', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-22 04:27:12', '2020-09-22 04:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `title`
--

CREATE TABLE `title` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `title`
--

INSERT INTO `title` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'WELCOME TO LIVE-STREAMING PLATFORM', 'Available', 1, '2020-07-08 01:18:45', '2020-07-08 01:18:45');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `broadcast_live` varchar(255) DEFAULT NULL COMMENT 'Yes = On Live, No = Off Live',
  `title` varchar(255) DEFAULT NULL,
  `broadcast_share` varchar(255) DEFAULT NULL COMMENT 'Yes = On Live, No = Off Live',
  `platform` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `autoplay` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT '1' COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `nationality`, `broadcast_live`, `title`, `broadcast_share`, `platform`, `link`, `autoplay`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9a04ea5286dc214d621622e14a858dc4', 'admin', 'admin@gmail.com', '2bd3fc76b60d220e67a40adb4c4a9be1d2cdde2afb4a406b6ff1c2daad40ea59', 'a221197fcfb97258210864e1175f32f221cdc16f', '6012-3456789', NULL, NULL, NULL, NULL, NULL, 'Youtube', 'H63Oq8YYlgM', NULL, 1, 0, '2020-03-18 06:28:23', '2020-09-21 02:29:53'),
(2, 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'info@zeon.com.my', 'badf5f9aa63f176b7bbb791435a48a7d97b65eb3fd0c2877e425eb2968728878', '1143f7c0663b667b433fe968c5eb4375ceda5bfb', '042910036', NULL, NULL, 'Available', NULL, '1600424768', 'Youtube', 'LFzduCN2QWk', NULL, 1, 1, '2020-08-12 01:29:22', '2020-09-21 06:35:50'),
(3, 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'enquiry@aspengroup.com', '5b5ba99347814a60a60e7087187e9b3fd1fde9bda8018e713fabd7795bf19f5c', '55a89e50829562b991d1e2a2a95549fc5716250e', '042275000', NULL, NULL, 'Available', '1600681344', NULL, 'Youtube', '_Jv5-43YWI0', NULL, 1, 1, '2020-09-14 07:22:52', '2020-09-21 09:42:24'),
(4, '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'crm@mahsing.com.my', 'e261844ed0a4556c3c69fca0881a7b2850ca9c5a9c04335dd731419f33b0d0e5', 'b3578f4f0a135e95aa9bf41b7af638414812e099', '046599989', NULL, NULL, 'Available', NULL, '1600076339', 'Youtube', '3lXggG62M1I', NULL, 1, 1, '2020-09-14 07:08:08', '2020-09-21 06:36:02'),
(5, 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'hunza@hunzagroup.com', 'c3efd0f9a47ab616c56438531ae2cbf7ad43c0dd186b7e302cb63ed43792262d', '8d8abbf246e1e084b64854f4d98d5961f8236ca3', '042290888', NULL, NULL, 'Available', '1600748902', '1600656689', 'Youtube', 'FLSRWnJVnJ8', NULL, 1, 1, '2020-09-14 07:15:54', '2020-09-22 04:28:22'),
(6, 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'enquiry@tahwah.com', '476bcf8f55b54467702d0415295e89c0dddfec0d57fc6cadb078b17f654e2b10', 'cd2addc406af5e903b1fda16d3961e5fd3a4581d', '043140638', NULL, NULL, 'Available', '16006687262str ter-e12.jpg', NULL, 'Temporarily', 'asde', NULL, 1, 1, '2020-09-14 07:10:11', '2020-09-21 06:36:45'),
(7, '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'marketing@jadigroup.com', '721e6b5c5db8793137ac0ec8d306db2e3287a1789fd9ebdb35d307a5472e1065', 'b3021f4096bf3aea8534cbe4302d93aa9a20af99', '045937168', NULL, NULL, 'Available', '1600664196Royale-Infinity22.jpg', NULL, 'Temporarily', 'aaa1', NULL, 1, 1, '2020-09-14 07:14:45', '2020-09-21 06:36:49'),
(8, '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'enquiry@berjayaland.com', '96105833b51eb1a7f4c7d9c62bce4369670ed06feb4c876e0f39e51e237171a8', '9205f7c15f48e57e27696cbd149cae7fdf219399', '0321491999', NULL, NULL, 'Available', '1600670301Artboard-2-100s2.jpg', NULL, 'Temporarily', NULL, NULL, 1, 1, '2020-09-14 07:13:38', '2020-09-21 06:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `userdata`
--

CREATE TABLE `userdata` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userdata`
--

INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(1, 'fdc700332b8cdf88e179d60fe34d9ba8', 'Lim Theam Huat', 'limtheamhuat2012@gmail.com', 'f059d809ce946ece99b1525157ffe4d28e2643146bb1733d772ada4dd42ca921', '3a99e14af2b1112c3c9c3d27edc9fe65eab9df17', '0162319397', NULL, 1, '2020-09-07 02:41:18', '2020-09-07 02:41:18'),
(2, 'aa990e0054a9091ba74b2bc00b547c7d', 'Simon Lim', 'limlikean@gmail.com', 'f01bc863c2f05b851cd34e006779f4d513e78500bd5d51f8fa0e99567bddb9ff', 'c68fb9d59e7beee87e12966ea88959e39234fca7', '0194459405', NULL, 1, '2020-09-07 02:41:47', '2020-09-07 02:41:47'),
(3, '343412e6a1d5570d6f49d8c64f247a79', 'David', 'davidyewab@gmail.com', 'bf050a0ac92e494da2f2d4ebba7f1aa191f15b9a9c59536cf3c6b1a38e2b9697', '155c36a59422be9ee6c08f909f1cc2599b02ad4b', '0164232203', NULL, 1, '2020-09-07 02:42:15', '2020-09-07 02:42:15'),
(4, '69229a4ddbad02a7cc3ccbd0e092129d', 'Chuzen Lee', 'leechuzen@yahoo.com', 'b856b153a7f0132aa1b1ce513ff733ad0f8b35f7362c02792c412b45e1cfba85', '2992d25615c7170e914b84c7b34e32df158d9cae', '0164744193', NULL, 1, '2020-09-07 02:42:30', '2020-09-07 02:42:30'),
(5, 'eb7fc103524b6801c3116bfd845e2887', 'Cheah Yi Wei', 'cheahywei@hotmail.com', '7819b95ac947036e92be5243c739d011cea11793d61d95e993477b5441939cd4', '6fe8a3c7d441c2aa075d9d6eb60d3ccaa791d213', '0125280869', NULL, 1, '2020-09-07 02:42:44', '2020-09-07 02:42:44'),
(6, 'ab13945019ad09c9fca6ab5adc488fbc', 'Cheah Chin Wah', 'majorcheah@gmail.com', 'da1b5486b3960e2a465519ee449c479a41a66bbbd8c21c49d0a58d648499b4dc', 'b2663843dda8d63d760490c107287aef14ce7542', '0124280869', NULL, 1, '2020-09-07 02:42:58', '2020-09-07 02:42:58'),
(7, 'cf0a947e487d19ffb5b45b1d4c57215c', 'Jimmy Ng Leong heng', 'Jimnglh8@gmail.com', '8212ef6a99bb55a6c1d9254280b6ba15c1900f7ea94a6851b0e23aeae5101d48', '7745648360f4f78df8d5a708241b942efed5aad2', '0125590999', NULL, 1, '2020-09-07 02:43:12', '2020-09-07 02:43:12'),
(8, 'bacdaa77dbf496cb1498491192035f12', 'TIU KOK KEI', 'tiukk@yahoo.com', '9007342cca89ca75171e30f0f5eba723a326c793a1e9077adba5f05157626fe9', '50a879acfb93ed1512a7c7acc6bdf3acd668bb4f', '0174167942', NULL, 1, '2020-09-07 02:43:32', '2020-09-07 02:43:32'),
(9, '0bf8570a61a9efaee2853a800f00d092', 'Leena Lee', 'lee@wellpoint.com.my', '9938433fddab3472e79519a4e95941ee069acfc095d538d097048512366ea73c', 'eaf0a8f6cf9c4ca91e096f79dae789ccb6b67a3f', '0124775428', NULL, 1, '2020-09-07 02:43:45', '2020-09-07 02:43:45'),
(10, '0ec87fc594ee329470996e6471ed2ce8', 'Kevin Yam', 'kevinyam168@gmail.com', 'adf5467921c9e3992bb06d0ea0ccad5f611867ddc551eaf9abc1bf858800a548', '166786196d475fe310115d849fbede3b70551146', '0165324691', NULL, 1, '2020-09-07 05:01:57', '2020-09-07 05:01:57'),
(11, '4353c6dbe204808ce26bb1887e9fe654', 'Jimmy nlh', 'jimnglh7@gmail.com', '05a24c005c4b72502e1aaa05ae38f04f70c36d829e10491fe085f01018632990', 'e7a78080aeceec9a242e3a86a6e8313129da44a3', '0125590999', NULL, 1, '2020-09-07 05:08:23', '2020-09-07 05:08:23'),
(12, 'd5e1373f3d295cb0868fadbb16cb90a2', 'Shirly', 'shirlykhoo95@gmail.com', '0fcf27fd8874394e24316f6a2fa4f8571623e4a504aed9b29718075a6c4f2d03', '522d4c683a5cfe936a63f27393465848c8095ae4', '0164745367', NULL, 1, '2020-09-07 22:52:38', '2020-09-07 22:52:38'),
(13, '65f85be79c2b393c28c495cd34fc9611', 'Sherry Tan', 'sherry.tanhl@gmail.com', '2855b15969f3fef85a72921e885316ac65942601cd2cb495876da29f8a71e791', 'fd7d2233810689456c11c9a1f666ab24f64f3a92', '01234567877', NULL, 1, '2020-09-08 00:53:16', '2020-09-08 00:53:16'),
(14, '043d93b17950177af59a7fc860c16b30', 'Carolyn Leong', 'Carolynleongmc@gmail.com', 'fea818fe46b6b181ad93ea07003bc58cbe7fe2550bebbb66216ddc2701303ecf', '68fd1f18587a362582ba403bd2370382c7d8b70a', '0124882226', NULL, 1, '2020-09-08 01:04:22', '2020-09-08 01:04:22'),
(15, 'e5be253ff00101b7bd90b5365eeb1a47', 'Sherry Tan', 'sherry.tanhl9@gmail.com', '6e80440f67ab916e03337a0b67151cf84e7012fdd418776ae9229a6d67f403a5', '01d5432662f5fcdeef7a93c212d095642b62604d', '01234567855', NULL, 1, '2020-09-08 01:10:47', '2020-09-08 01:10:47'),
(16, '6d533cb6349942cc72242e1ee971dfbf', 'Jul', 'Julia.ngcl@outlook.com', '298122a682bb782c64ef1fc3425c39216fdc2b3177d2e77bdf080f240421f28c', 'ca3c7dca6478ac76620d611c78c9335f76eb9df0', '+61432605890', NULL, 1, '2020-09-08 02:11:20', '2020-09-08 02:11:20'),
(17, '9dda4793c0040d93561187bf1bfb229e', 'GOH', 'ikgohspt@yahoo.com', '3f9e57ee2497a85cf8323d2a3a2c1621e225403d5a4278aecd528285c05ac994', '37f7292651dd4dffd60b435a4bf0ae93462e5a92', '0194817631', NULL, 1, '2020-09-09 01:15:24', '2020-09-09 01:15:24'),
(18, 'f019e76a7730c59508d65828a3a91977', 'Saw Cheng Siew', 'mitscs61@yahoo.com', 'adfb79a1ef78b027c65732cd146ca428236429c7843520d0c7ccbdae2c959c73', 'c6b8ed90577992c24a63739f586ae3d2255bdf16', '0164105808', NULL, 1, '2020-09-09 08:31:40', '2020-09-09 08:31:40'),
(19, '218d9e412818e03306144e7203ffd16a', 'Sherry Tan', 'sherry.tanhl12@gmail.com', 'b9607d30a4da994a716fc6c6fbff89047e5d5a00a14e2a0ef6fa9f204840619b', 'c492f156f9bbc1b35d4418f63a3c46ab64c14bbd', '0123456781111', NULL, 1, '2020-09-10 04:49:55', '2020-09-10 04:49:55'),
(20, 'fbea12995fa14326bf4ab4d0b2a85152', 'Cheryl Loo', 'cherylloo@zeon.com.my', '3a5fd8d6027ef3109b69a6267b63a7476d1418afd601d6bc87084ac9f9fc16da', 'd361ff72626eb1255444065c7802fb4706fda719', '01112165658', NULL, 1, '2020-09-10 07:08:49', '2020-09-10 07:08:49'),
(21, '94ea604545f008f01bf264b589cc570f', 'Melvin Tan Jie You', 'tanmelvin666@gmail.com', 'acfc47232f5f3196c407accbae1d8f1d4e08b143a196f5190f5ddb367da637ae', 'bc38fd0db9ace4a6415e0b32eac2f762faf3afd8', '0143020994', NULL, 1, '2020-09-10 07:10:45', '2020-09-10 07:10:45'),
(22, '55851f974839891ae21501115c4dc644', 'Goh Jia Pei', 'jesphingoh@zeon.com.my', 'f05829c8ea7f019acb804b6723b7c8df3d9af5e958b1e270bda6dbb08b82f639', '61ea52a4c7441e318ea310b6b23b2641f7f66890', '0103974255', NULL, 1, '2020-09-10 07:11:35', '2020-09-10 07:11:35'),
(23, '2b26eb08be534bbcc4230a56fd5b181a', 'Yap Wen Ming', 'yapwenming@zeon.com.my', '0475f6ff4d4131a5deb8fea78ff0903ed4e9156d76691f467d7a2ed38680c831', 'c3633f7d3f537b38c038f550f224319c2a1de9a8', '0164263797', NULL, 1, '2020-09-10 07:12:47', '2020-09-10 07:12:47'),
(24, '215065b5e54739da4ac5dd975366bf4c', 'Alicia Ng', 'q-alicia-q@outlook.com', '0686a37df841026321f8aea7d97b7afcff5b1d82b8b155e8247aafb5b04906fb', '1a0777367876aef7f3cf28fcdbadf1bf287f64fe', '0164440261', NULL, 1, '2020-09-10 07:13:03', '2020-09-10 07:13:03'),
(25, '33d652729cc58518bfd2739915841237', 'Shiau Qi', 'shiauqi_97@hotmail.com', 'a617c96f377df6130e5dc54fe226589e4f5fe41339cf8b66936eb4f9aee59e74', '47396260f53129dc3a2a1da1f77415180816d533', '0175451837', NULL, 1, '2020-09-10 07:13:03', '2020-09-10 07:13:03'),
(26, '43e8f32ea0ceb9993ca995ef70a32d6e', 'Joey teoh', 'joeyteoh@zeon.com.my', 'f0399587977f7bda6742e42e61d14d334925c4ef4ef21140b82ee823982d902e', 'd2bd92aed2a944a787bdde66ef922f10507a1b91', '0189477725', NULL, 1, '2020-09-10 07:14:26', '2020-09-10 07:14:26'),
(27, '9fdc625fef459a7a4a9257c005e10de6', 'SingNing Kee', 'singningk@gmail.com', '972823d5e9e33e78dcd1081c93fca9d95d0d6cb937802c624c54e8bc7445f8f2', 'f16f93455311474c197b3bd8a3dc0ed8da6edf59', '0126544227', NULL, 1, '2020-09-10 07:14:51', '2020-09-10 07:14:51'),
(28, '5a1b8eade36b97e2f4faf3c6b45b609a', 'Lim Jia Cong', 'jclim92@gmail.com', '27eafa65f0d2205024427ac231bdca3e2691a87df00c1b5af2c8a5ede561e713', 'f124a81e36e5bf0f86fd289b19bb9c2df5e6b69a', '0164824044', NULL, 1, '2020-09-10 07:22:54', '2020-09-10 07:22:54'),
(29, 'ec0dc9bca3ca4531159d735bb9f547ab', 'Koay Boon Chang', 'boonchang1017@gmail.com', 'a9a5ae34ac31804125aefad44e6647f254b323d8ac048357ef3fffde7b8a6c8c', '0ce92bab1a7bd4e02c104286516547d068e0b924', '0192673299', NULL, 1, '2020-09-10 07:26:44', '2020-09-10 07:26:44'),
(30, 'de9a54f41029ee114b1eb35da0852b70', 'Michael Fong', 'michaelfg81@gmail.com', 'c4fb33b732ed32b11bf112015466044e93391d2c2197c0f196cbf7b7297a862b', 'cf55e080a39377908bad5cfdbfd8063d8b9fa23b', '0177797511', NULL, 1, '2020-09-10 07:30:17', '2020-09-10 07:30:17'),
(31, '4ae517573b120dbf884221efdd1997bc', 'Bent Lee', 'bentlee3288@gmail.com', 'f1159818848452fbdc4604392def9c9995a79892bbab2c512e9d7c99a87d7250', '73ee719b65e832d31dcbdf9e6ab274fbdf695b86', '0169046071', NULL, 1, '2020-09-10 07:33:53', '2020-09-10 07:33:53'),
(32, '38c7f9093ba584b62d91bbb3fe6b04fc', 'Goh Hau Che', 'gohhc99@gmail.com', '72283920b0af71efd78c8c35f289daf552d797491ce258acaaf5ddd3129880af', '58d816a3669f050f94595559a8cf22103a474323', '0194643985', NULL, 1, '2020-09-10 07:43:22', '2020-09-10 07:43:22'),
(33, '2e05a73a06f41898b77ef8264bf71de3', 'Khor Tien Wei', 'fiolikhor1231@gmail.com', '13ff5dd25f7847ee698d8e47927c3a705ecd190aa7ef213fb7b11c2861813291', '61e01f370afa54ef1377898ca59e5371a57b295e', '01139287504', NULL, 1, '2020-09-10 07:45:51', '2020-09-10 07:45:51'),
(34, '351dd62b0bd4b9cb7a8151e12ee0c046', 'TEOH BOON LIANG', 'blteoh9338@gmail.com', 'f131b655da670d8388e0022cca30cf50667df6a156bd4c50ee720f88d27f3372', '6e6a859c7915ff7510077ac03aa60396f50ad1d3', '0164346628', NULL, 1, '2020-09-10 09:35:50', '2020-09-10 09:35:50'),
(35, 'd4a6ab37682033416615412813606a6e', 'ML WONG', 'mlwong57@gmail.com', '2103cd65e8b16c7a101523e5d26638fa2909bb0d23ae35f5c3c1f56d422e00cb', '26e0056958dc11323b2799acbd6a833a2eecc636', '0124100199', NULL, 1, '2020-09-10 10:57:55', '2020-09-10 10:57:55'),
(36, 'd0133900805bd38f3150865168683b78', 'Max Ding', 'Maxding@zeon.com.my', 'a48e80541d57eed4a36394f17f50a5de98148fa673cab8eb7219667d8b2ad6ad', 'fdb72d80fb9ee84772e854309267265772bf737a', '0129505443', NULL, 1, '2020-09-10 10:59:51', '2020-09-10 10:59:51'),
(37, '0a6a2a97d882c54c2b74ba0c3a3b1494', 'Jimmy Ng', 'jimnglh9@gmail.com', '1b17293b8a5a64b44490e0f55fb2d6db519da11d33005a1b25cfcc51733ade46', 'c0b4cb3866c3ec204b3b4140ac0712a1a83bd4d5', '0164186688', NULL, 1, '2020-09-10 11:06:20', '2020-09-10 11:06:20'),
(38, 'c3f95d2bb7fc3ebf578799656357f461', 'Jimmy Ng lh', '', '1d0359a8983bb0ffa359c9d1a09fdbacd2f024d01205cda0daf893526dcacbbf', '7925e5abe805ca451c55e19f21dfbfc77f808432', '0164156688', NULL, 1, '2020-09-10 11:06:42', '2020-09-10 11:06:42'),
(39, 'd0cc540aa863c96aab36ece5cfaca33c', 'Wendy Teoh', 'ccteoh@guangming.com.my', '85050d4e74def200e837a54b0eb3248e5c58502ef0bf5ca40005bf8bc6c8b2aa', '1990bb4499b3a5ac9a53e97293f743b4685cca28', '0164198226', NULL, 1, '2020-09-10 11:10:20', '2020-09-10 11:10:20'),
(40, 'b0c5f403fa167cfc848f300a13687d5b', 'yeow yeang hong', 'stevenyeow2@gmail.com', 'a07da796df15ed94ec64979f92d945e01a344dd70515d5748a96fa9a36903ab5', 'a0338a2e827169a7206c9ddebd3675eaa1bd6133', '0183218288', NULL, 1, '2020-09-10 11:20:20', '2020-09-10 11:20:20'),
(41, '049c3d3a4870e5f492746e5f06212690', 'Ang Hock Tat', 'Hocktat@gmail.com', '22f66e972373bd74b79eae78713b19fd07ceeeeabffaf259dec197faff14983e', '6c6f962f5a6ca40bdc8ef52e5b0f3221754a77a8', '0124776441', NULL, 1, '2020-09-10 11:28:28', '2020-09-10 11:28:28'),
(42, '7b1e755f6d024c912aa7ed62f02a2fa2', 'Tan', 'tanailiassoc@gmail.com', '03c2f8e6e83754d33ffc7c5d17a48f8608d2ba9abb37970c67f7b42fee65b3c5', '53bd9e5d7538890bb3ef081f82c81cca93b5a35c', '0176629608', NULL, 1, '2020-09-10 11:32:55', '2020-09-10 11:32:55'),
(43, 'e564a0683831d63eea99f3205e279871', 'Ngo chee yi', 'Yicompg@gmail.com', '8b98decbc6372e42bba0cf1c218eaee4972fc395e558fa47253a0e111fa2bf70', 'c7ac6590fa07d6d1693462066d335bfa84fc0e8a', '0174782238', NULL, 1, '2020-09-10 11:36:57', '2020-09-10 11:36:57'),
(44, '54e3caa91dfc51582f8876b7f940a259', 'Yen Chan', 'somuiyen@hotmail.com', 'a94b7558f1f75f2721b797eab178d0a5641efeecc57744d806ca93a5147a87d5', '8f553258d2f39eb8049cca58fab7f9f282a6b757', '0125812005', NULL, 1, '2020-09-10 11:52:14', '2020-09-10 11:52:14'),
(45, '7716b19c341af7a300ff390862897904', 'Lim Seng Choon', 'schoonlim@hotmail.com', 'e69e2cb57a0dbd40810b5ae9208ca2e65b722a8c7ca6050accb812d1004d22a3', '8385e794fbaf1dc5bba2629084e8249bf6c2beda', '0195771840', NULL, 1, '2020-09-10 11:55:39', '2020-09-10 11:55:39'),
(46, '4e445d8acf7576e143433bd9d4ffb645', 'TAN CHUN WEI', 'jordantan83@gmail.com', '2d5cf65f9ee99255940ae5bbe185432219445b13e9d2e69950dff17b268aa9f4', '6eb29293584ecd3fd8aaf4ae31690a27566a1816', '0124108289', NULL, 1, '2020-09-10 12:04:16', '2020-09-10 12:04:16'),
(47, 'e344da056fb189a5ce7b23fd556606ba', 'PHUAH Eng Hai', 'ehphuah9876@gmail.com', '96e01ed9296c44ebb46b3792809e3d6b0027ea520d0c31dcd322086fc56232e5', '74d9ed567706b0aa7aeee1ca574262501663346f', '0164198130', NULL, 1, '2020-09-10 12:10:55', '2020-09-10 12:10:55'),
(48, '8f069d87b8c08ca79f2b58870f20c6d6', 'Yap Pow Ping', 'yappowping@yahoo.com', '88859a4891ec04b852f4ce7b5a7fae43b1fb6a3aa396e74b48bfedd969a082c5', '8a9cb92f50b36f918ff1a2457292e5ed438a2d41', '0194417972', NULL, 1, '2020-09-10 12:15:13', '2020-09-10 12:15:13'),
(49, '6bb8d54d3b394b2cafb249e4c878fc50', 'TAN SEONG PEOW', 'tanjack126@gmail.com', '1f469491ea4340284ee1cc4f2b68409e4bf6c3663bf29f394c962c96a1acb671', '1d706a1b9bdc401e54254af75b3672a2a07c0076', '0125771314', NULL, 1, '2020-09-10 12:16:26', '2020-09-10 12:16:26'),
(50, '984b2dee96952407688896e3d37188aa', 'goh xin min', 'icequeen5454@gmail.com', 'e5bf67d8f4ab01e25d0a84f844974faf3ab985645702324bceef2291a3601e4c', '7218254a1321690df9b4ce3b86436632a0bbe4cc', '0164717314', NULL, 1, '2020-09-10 12:19:58', '2020-09-10 12:19:58'),
(51, 'b3c6e70ccdf8d462bf2889d2b166068f', 'Elaine Ooi', 'bayelaine@hotmail.com', 'ce4d293206f95f01c371c23fd7573bce64596c1c59db0505ac3755261647d3b6', '882205259429becc2d79bd8a5360d32d5aa2a40a', '60179210696', NULL, 1, '2020-09-10 12:21:06', '2020-09-10 12:21:06'),
(52, 'ffbeabee7eeff7c5475ece1a636be30c', 'chongcheeooi', 'cochong@guangming.com.my', 'c77b0316e6af926c77427aada1fb735825d9de05d7a90a3c9037261a03a50007', 'eb6308d04e0b933c4d3877b3b6f18de8453737ce', '0164196637', NULL, 1, '2020-09-10 12:28:10', '2020-09-10 12:28:10'),
(53, '068b9f3b5d0a027feab8c195d81bee9b', 'CHIN WAY LI', 'chinwayli70@gmail.com', '0d7e2711e7c11fbd95f0daf54d2c5044caebc4fadcb7874de6c0dc32cc7ce5fd', '7edf103a95fa91b18c66fba726ce6f1e4a402dc0', '0164198140', NULL, 1, '2020-09-10 12:31:54', '2020-09-10 12:31:54'),
(54, '5417a0c511d0aa56f6eaaa6bcd5d2da9', 'Tay', 'chtay731@gmail.com', '4097097f551197a411c610e21f4ab880d43379d17142c3226d595b38aca758db', '2db31120451918e8840e9bc9081284183ba5e20d', '0174235939', NULL, 1, '2020-09-10 12:40:41', '2020-09-10 12:40:41'),
(55, '3a59bf439eecb122ab03371dd0261e0c', 'Lim Mei Fern', 'mflim2000@yahoo.com', '66fb7f6615b2b3750bbc7749312db7df0579c13f8aa87e24d9af50a249128eb4', '008cb8f5290f38a1a663244d7245a7bebc1bdc59', '0124666524', NULL, 1, '2020-09-10 12:45:04', '2020-09-10 12:45:04'),
(56, '957b38302653526b429a7e24a5fc70d8', 'Lim Bong Ha', 'bonghalim@gmail.com', '51e1f2ee6df7a6ede2105eb3ed64831c27376b40b98e24e96a84ed842072df04', '3d57793e5d2cb0ae3ce16df1945952cfcbb7912c', '0175444018', NULL, 1, '2020-09-10 12:45:43', '2020-09-10 12:45:43'),
(57, 'd161694e2d8234ba39a7c1898900b619', 'John Lim', 'johnlimty@gmail.com', 'd6ac3262d5b437feadd6a18ed5e6de0be6bf5b261cc3d9edf3ac661fe06240cc', '3dfdba86c4ca6aec730955bb1c8566f2e19cf9bd', '0124288416', NULL, 1, '2020-09-10 12:58:23', '2020-09-10 12:58:23'),
(58, '57d66e45dc2260ed35877133d6677d2f', 'Yuen Wen Jieh', 'wjyuen@gmail.com', '9f466fa20c4838f8faaa65c11cb11c400300341669952d196a02efcc6c27b894', '9cde92b547b1f9d793b69f76be122f18f5d395c0', '0164141919', NULL, 1, '2020-09-10 13:03:23', '2020-09-10 13:03:23'),
(59, '4c590f913eb95dcaa3227f6d79163c13', 'AIMAX LEE', 'aimaxlee88@gmail.com', '2c0c70f3b7209f27a7689c4e0a4eb8f4b04a0c8eeec8421e865299c07eb405fb', 'e7c8d91cf7cb15c0ff6751719983f89f76f19cd0', '0175996877', NULL, 1, '2020-09-10 13:30:12', '2020-09-10 13:30:12'),
(60, '94abd1c39c21cc2804df92cd304667bb', 'Ms Goh', 'karengpshin@gmail.com', '5da5fce4f2ba0b5b79ec19ad7b13a7a6af8a3ff317a5e346565e5a2bac2bc078', '326dad55d87a9cbd35287217f5c40f7d5a8b306a', '0125276892', NULL, 1, '2020-09-10 13:37:47', '2020-09-10 13:37:47'),
(61, '63bbf8e030dc91921558730ba870241a', 'Sharon Teoh', 'sharonleang@yahoo.com.my', '765071d802c1b36c737dcaa2bba1827dcbe420c59b249efbf78f95ca0e73c731', '3c176b546e2e638f6f4bfaf4be1262789c2923ce', '0174731903', NULL, 1, '2020-09-10 15:15:27', '2020-09-10 15:15:27'),
(62, 'ff3efeafc97f7f62615c09965c4db1ea', 'CHOO YAW GAN', 'summergan8@gmail.com', '212598e51e8d0c7e0f92a06c1b9b9b96bd284c28b299cebf54b8287d30ac1a4c', 'bc2026d4eeab105d1f20984111f8b744fe9928ba', '0189540822', NULL, 1, '2020-09-10 16:19:16', '2020-09-10 16:19:16'),
(63, '950f56cc08294d49c8723008b3988fc5', 'Khoo', 'sinar5231@gmail.com.my', 'a4935c1f0405985fbd4826c411b7485ffbcd2a4d750aa19ddd4733546131eeea', '1910089dbacfacd3eb71747ec32fc6b505a96440', '0124535427', NULL, 1, '2020-09-10 16:35:10', '2020-09-10 16:35:10'),
(64, '506a7e40c1dc8c5a3b5274affb6b312e', 'Lim Soon Seng', 'jasenlim@affinex.com.my', '6fc6ded2efec691e54357b7b9442ea52cbb861aca642a608ae48d19a6d12e224', '2aff87c392f6372963365bfb38d0508b350903a5', '0124278858', NULL, 1, '2020-09-10 17:18:38', '2020-09-10 17:18:38'),
(65, '4a59d8579f5352c32e56190d765b1fae', 'Iris Tan', 'iristjw93@gmail.com', 'd0f9b538c7cbb00ec7f681b49eed44a8df71144afdfad3b2dd17852c87c53e4f', 'b69b66594ae47dca696847e13fbde8d7769f7bc3', '0143093427', NULL, 1, '2020-09-11 01:58:47', '2020-09-11 01:58:47'),
(66, '5475a0391d26adc407569df964c4a093', 'Shereen Kung', 'shereenkung@zeon.com.my', '62544ee93b6d9365335a1bbe46911002ae8c03f7aedfcc7c4e3fff1f05e8fb4c', 'e26c42c3b001315b1a3a9eee0ec08956caaee60f', '0164634065', NULL, 1, '2020-09-11 01:59:33', '2020-09-11 01:59:33'),
(67, '79d5d4f7ef7a1714dd3efc80241a7b90', 'Lau Chun Keat', 'twc1008@hotmail.com', '31b4d69fccc6bb4fcf0b2d326b2b13f71dbe2faa520f83952daa24f57797ea5c', '07c72a4dbe8185a8bfa938b0ed4c005263fd2664', '0164642079', NULL, 1, '2020-09-11 03:08:00', '2020-09-11 03:08:00'),
(68, 'e7e1a3805db1da508f16d8db1f9c9ebf', 'Ng Yeong Mun', 'ngyeongmun@gmail.com', 'b6b4207dd9f98b3c09a791191e83ff98bfddc756b743457d5eea7b6a5e41a044', '1f205a41db3e3476ae549763e698c5ac688e383c', '0126422877', NULL, 1, '2020-09-11 03:37:48', '2020-09-11 03:37:48'),
(69, '069a0f9d5d5ad58f6f7a21a6ea901eee', 'Loo', 'felicia.lyc@gmail.com', 'a7b4d679c51df522d9cf615e0bcbcf8ebc06d8731aae62657cadcb3dc0d0d5e3', '9c8ca63f78f89949069640ab5c00a2d256de3324', '0124819109', NULL, 1, '2020-09-11 07:00:59', '2020-09-11 07:00:59'),
(70, 'ee5a0e459c3a6b4f0d546c0950f03da3', 'lim lay chin', 'jimnglh3@gmail.com', 'bc7f2cb4043b1985cc93dc19cf3a7d8dfe0be01392e6dfce5092fe23b20fc2de', '073124c6bde20d4dcae131199d806ba19ffa8a78', '0174880809', NULL, 1, '2020-09-11 07:53:18', '2020-09-11 07:53:18'),
(71, '5d81494a4ca44801142d8eb120bf5c8e', 'Tan Kang Wei', 'Kangheyhey@hotmail.com', 'd172f013b24bd3d920e23b2b9df90219d28270331648b278c7e4bfd30fd57ab8', 'b228b6a1ef2ce4144a29622c37bd19f34fcba295', '60124129370', NULL, 1, '2020-09-11 08:04:45', '2020-09-11 08:04:45'),
(72, '88baa48ed89f714ab84b49cbfd7eeeda', 'Tan Kang Wei', 'edutech0830@gmail.com', '42ee198a6157f2e797b88fca5eb9837206235c42264eef47b58bf6ca67050ccd', 'e888f89054d3ed2da461e4b2744e11526cd45382', '01111279370', NULL, 1, '2020-09-11 08:05:12', '2020-09-11 08:05:12'),
(73, '9bfb85145012d8eb919e5457eb550b35', 'Kenny Chia', 'kennyjuti@gmail.com', '22607f3cbd71673466e3a05e085b1b0804a6cbeaf4c22ba7a5ee5bfe21faf0ca', '965feecfb8066afd69408ed1440eb1b9dc5a4204', '60192260801', NULL, 1, '2020-09-11 08:37:37', '2020-09-11 08:37:37'),
(74, 'd6874123f3cd64af542a13b55a80439d', '戴志豪', 'Hauwen2000@gmail.com', '0a994afbabba34e5160e045fce38aa3a7dd83c59e0c9b822c34ddd058ccd5958', '0eaf404fab4390c2753ba238ebc40345d31da9ea', '0124274562', NULL, 1, '2020-09-11 08:53:36', '2020-09-11 08:53:36'),
(75, '62c72466ff31bf42f5413e340e74aabc', 'Wang Hang Min', 'whmjohor@yahoo.com', '0481fe9b9469ba4ed16ef5d4e75257e862616a0a9f6c75ed464eaebeefe7d618', '5527ff88cd03c343d1af459b52910debfcec1b66', '0129003353', NULL, 1, '2020-09-11 09:57:42', '2020-09-11 09:57:42'),
(76, '4750bb7e6a75b4f1057d877bb5a999d3', 'Chuah zhi hao', 'Topbrandgary@gmail.com', '69500a2827e4f79e6f188fae476ae78d92cfa353efc072f6a1cfc8abd57951c9', '482e482b64dd39fb79eac7c6fe857d5ef62b8b5c', '01139548361', NULL, 1, '2020-09-11 10:16:33', '2020-09-11 10:16:33'),
(77, '6dc5aac0d010177b8d8c10aebe2fb377', 'Tan', 'pohteik@hotmail.com', '805baea1efe191e3bd1e27a0f723b03832d4109a80c94c9576cbbd203d21f690', 'f25a18ae668a5ac5762db42fcf3c2a8459828bab', '0176661662', NULL, 1, '2020-09-11 10:19:20', '2020-09-11 10:19:20'),
(78, '3eafb453509ffd5c6ebe896a88d9466e', 'EK', 'ekng2000@gmail.com', '6eb6afc8feaf7dc4deb80f193fe758f026cfa5f80f40a0bdea93982fd8719c50', 'c59c91b3c69616ba0f0d130a21ba85943a575bd6', '0164991470', NULL, 1, '2020-09-11 10:50:41', '2020-09-11 10:50:41'),
(79, '634ab5943ef0e4cc46d9600765958806', 'Edwin Ong', 'edwinoce@gmail.com', 'e41ab678db9d7916abb00c8bceb21001de98bd7f5ec69a588ce8f5d4f095fc1b', '2ce6e1ee8c680930fd5a3c4e1a6659d67f75056d', '0124233368', NULL, 1, '2020-09-11 10:57:26', '2020-09-11 10:57:26'),
(80, '8e04ee825f1d08f325fe9be3a9feb80e', 'Tan Teik Chuan', 'spe_tan@yahoo.com', '7e82f8e7e0946164c2efaed9236bba15266d614aca3c8fbdbe3a107d0bc1d7c6', '18d79c7658b3a3faa5a5e1a76386df0af311ab1e', '0125556666', NULL, 1, '2020-09-11 12:43:07', '2020-09-11 12:43:07'),
(81, '8e3c2137b19abcf93d6966571ea204dc', 'Royden', 'Royden931@gmail.com', '7f8a0354c18fba6e0cda67edd38a4d7bb431e58d0e2322da3fbdce6f1688a87c', 'ab54a8f851df2b13e7323d483fc4eaf17286fe6d', '0124258616', NULL, 1, '2020-09-11 12:45:56', '2020-09-11 12:45:56'),
(82, 'd295709cd45f76c993bd790aa1535a6a', 'Song', 'sharonsong2@gmail.com', '87eb4036a9e3cc269c69e949a6095145e63f3d2e7653e5f383c7afdf7d5dfbb5', 'e2da83bbcdc4173a7e36f6fb6e2dbabe7a4c907d', '0124312738', NULL, 1, '2020-09-11 13:18:06', '2020-09-11 13:18:06'),
(83, 'dff327e890c34d01d81501c724bd9f69', 'BB TAN', 'bbtan.yomax@gmail.com', 'f3cc88052e4e50823621aafbe98f7979918a6308a2cb7103b91cd8a860c65f31', '63a10c9e4153115c827671c075fb3db5ffea7328', '0124711517', NULL, 1, '2020-09-11 13:52:27', '2020-09-11 13:52:27'),
(84, 'b340638b3b281004102c3e5af217921d', 'Wong Wai Sing', 'Jasonwg1818@gmail.com', 'fbed0c531c04924f913f6f46d418515e38614fb828677854c84c63589ddb2325', 'f4c4ac65ece65e0d830d398e1ae6adc0fd8684db', '0169765692', NULL, 1, '2020-09-11 14:09:32', '2020-09-11 14:09:32'),
(85, 'cf6d037e1467edb42bc22f764256a54b', 'Ho', 'hbc1977@yahoo.com', 'a8cf484d72fe4481306b4a31c24a2884d226a2e8d50d7c7fb0773722fffd1aba', 'e6021c2e134d95120af25c034de1da0d9c3ac8af', '0183729072', NULL, 1, '2020-09-12 01:17:46', '2020-09-12 01:17:46'),
(86, 'a73951b3d3139bcb3feaecb28a28287b', 'Harris Loo', 'harris@acewideasia.com', '328b271ed87ee844958a5c4723c87f9f657a4d08545451771b842a29fd3304af', '5fcfb433b01c44881d9546e1b35254dc71204672', '0124756220', NULL, 1, '2020-09-12 03:33:26', '2020-09-12 03:33:26'),
(87, 'dc96efef451ed8335c2a59611bb366b1', 'Shaun', 'jagamjnb@gmail.com', 'c0be2db38ddff4a88f42a490966ee92f715bbc9a869721126b108fb363e7e1c0', '11cafc77dc4c3f7b94449e1372b1fe7032f7f8ad', '0195779523', NULL, 1, '2020-09-12 09:26:05', '2020-09-12 09:26:05'),
(88, 'e828b117f5844ae3519910749c8e352b', 'JC Loo', 'jcloo526@gmail.com', '45ba348f023a2b1f7cefc773f194bced218f6fdd92fd9b56a55956c1bd2d4ffb', '6d74359cad17025ca857c56bb644d6eb6fae1982', '0174061366', NULL, 1, '2020-09-12 09:27:58', '2020-09-12 09:27:58'),
(89, '869670806104798d93e9f89456ecf487', 'Ooi Eng Seng', 'oes64@hotmail.com', 'bf6921380c64d148c9afb08588de371d9ef5a5727674e10a916bcaad25ee36fd', '1d56dfd0803a7a387bc9123e31abe19bd6677320', '0164719783', NULL, 1, '2020-09-12 11:12:32', '2020-09-12 11:12:32'),
(90, '405947fa47eba82fb91893c0aa1ed7bf', 'Darren Choy', 'darrenckchoy@yahoo.com', '58e3bc91745515357d94b55dd66cfe2ed4464bde4e96ba5c16a95aef3045ca76', '8ca7e4faab6a774463e1c55fda21533360ca42ff', '0124771515', NULL, 1, '2020-09-12 13:10:34', '2020-09-12 13:10:34'),
(91, 'fb49df0616fd67bccd0905c87878c220', 'Chris Quah', 'chrisquah@hotmail.com', '5fc7442135c4b13814db07aee9d0e045debd4682bbc588b146d5d69a83ea418d', 'e6a0cd0ac78d9930f758af824c0121d830b0f4d0', '0164196609', NULL, 1, '2020-09-12 13:11:40', '2020-09-12 13:11:40'),
(92, '15a735d53eee2bd9cb40c35aaa57e8fc', 'SF Thum', 'sfthum@gmail.com', 'cd1f9aa841d181c16693180f2ecf6d37b3c242d05e3d8624fa1a72799489a0e5', 'efba02c4012405edc59e1107905142104da14098', '0164764959', NULL, 1, '2020-09-12 13:19:01', '2020-09-12 13:19:01'),
(93, 'af9dc2857b603a919374bc88815f08b1', 'Lacey teh', 'Lacey.teh@gmail.com', '6a53b2f195c0fbff2efabbc2741c93c6de67a51b07bb107c158726e11ee223ad', '62cef8281c8679d5614a88d32a599ed6ca62b4d4', '60124721246', NULL, 1, '2020-09-12 13:42:41', '2020-09-12 13:42:41'),
(94, 'c65a73c7ea8eb242ffd79f7d16805922', 'Nelson Teoh', 'nelsonteoh@gmail.com', '7b66c350ec05cdac04e3f7957d9ae0fc62dee2f2c5d2ec4dc970e2d5f9788ea7', '7c126e6d96c3065bf3fbbbdb2fb7d999a8a8ddf2', '0164220422', NULL, 1, '2020-09-12 14:28:30', '2020-09-12 14:28:30'),
(95, '0b8804c1ba5da3b32d6a95c477e00f0c', 'TG Ch’ng', 'taiyochng@gmail.com', '0e5f07b71a95c282cbc765494ac05db4acc734e574742f3c4f2e56f8aba2908f', 'b73118169d08da049517650594410ba129b97800', '0124876777', NULL, 1, '2020-09-12 15:26:12', '2020-09-12 15:26:12'),
(96, '4252d1270250f1d29079be05fa50ff65', 'Tee Hong Ping', 'HPTee@hlbb.hongleong.com.my', 'bd1cc5483356337af93ebac16eef42748723e187b44cd496089d759b5173e3fe', '409102ef1107f51d3ded917b94ed2ad9e896d588', '0164527545', NULL, 1, '2020-09-12 16:11:20', '2020-09-12 16:11:20'),
(97, 'a84f7f148180abfddbf079d983fad3c4', 'Chong Chee Kheong', 'sjckc2001@yahoo.com', '089660375062b704744e69b87ee10078e02079b6f87fde222f53d5a0e14abbcf', '4c6f0f51a3d4bc03121a4d7db46980521dec7eab', '0124938146', NULL, 1, '2020-09-12 23:03:32', '2020-09-12 23:03:32'),
(98, '4fc0c1df34ada4ceadbafde4ab7a2104', 'Tan Kim Leng', 'kltan208@gmail.com', '561e914649f9a8588a77acac4f46b4cca90624b25d6d15d971435d39a399c143', '15664a76a651d269f38e1c28ce19c98b36511da5', '0124237193', NULL, 1, '2020-09-12 23:24:07', '2020-09-12 23:24:07'),
(99, '9699a40a6a6a25aca17ebb8214aa640b', 'TH Teoh', 'Tteoh8 @gmail.com', 'cae9b5d936d2e8d806ba0fd691f678810bac2323e3c34f857166ccdca41d0218', '624c3068719ed5a950d2de90ff5d8befad9f9601', '0124183866', NULL, 1, '2020-09-12 23:30:07', '2020-09-12 23:30:07'),
(100, '5e682ce0987dbbebe97c4ac7a3f53da9', 'Low Seu Beng', '1234andykok@gmail.com', 'd65eebe74fc2d98bf34f70c43c5cf62afe5e0d37118e9fcdac4b47fe267c20bb', 'ea4249e1c1e08d046030d1d047b54401fba62beb', '0124105680', NULL, 1, '2020-09-13 01:49:04', '2020-09-13 01:49:04'),
(101, '1eb882d7f8ce0040a17d893565ab4d44', 'TANI', 'kyokogtn@yahoo.com', 'f75e2e8155a9f80bde125940b52a4b7c61776051eb1b227e6d2778bcf5fbf27c', '4e5614f744ea2f859df831cbeff5eb747e676548', '0194421119', NULL, 1, '2020-09-13 03:07:23', '2020-09-13 03:07:23'),
(102, '5407484987edf3c7662bba0a6eda4c7e', 'Pk Hoh', 'Pkhoh2013@gmail.com', '0b8fd4ef10a25a95d054a1a229b213521b1a1693ee1c6a100db01b9af19279fb', '0a6e41ec3a93036bbf5a7ce19058d3f1908dbe61', '0163460030', NULL, 1, '2020-09-13 03:27:37', '2020-09-13 03:27:37'),
(103, '580e29f410afee8fa518485676aac601', 'Sim kooi lean', 'hedenasim@yahoo.com.hk', '137c976579362b5800d162e17a229f5c1f258b043b218dd4982befdf4e698182', 'fdd7343effcac945b788bdbfef5c2527a9607f71', '0194120604', NULL, 1, '2020-09-13 04:05:43', '2020-09-13 04:05:43'),
(104, '4b0f93aa45c9dd52052649d1c045b0a8', 'LIM LEE SA', 'adeline7669@gmail.com', '07ce2bd051f104d6a2dd0bc80e944efc07233f75a72a6e11a35313dffa44a2ae', '442698859ac9aad578334498aa80e2a31d2aa23d', '01112442832', NULL, 1, '2020-09-13 04:50:08', '2020-09-13 04:50:08'),
(105, '51b8dbeb96aa559839fb0eedc1516159', 'Fong KF', 'Fkf98@yahoo.com', 'ce1e29fede986b1bfc110808e327d288881b61a04b3401bb1e143f00b292fd09', '9f9b0e47d85932005aa30bee17f09175ba30fd2a', '0124097330', NULL, 1, '2020-09-13 05:06:48', '2020-09-13 05:06:48'),
(106, 'fbf916b674a6e9058806a2fbfde0d99f', 'TAN SENG SOON', 'tsengsoon@gmail.com', '667c4ca287e2d749ad118386a10a89909eef67f967769d8b1421e1332c3be1ad', '36f7e22a88c576474aff1bb880c4cc055ff929df', '0134895426', NULL, 1, '2020-09-13 06:15:06', '2020-09-13 06:15:06'),
(107, 'bd178837b7e8158873a133e0129c4047', 'Soo Kee Leng', 'alecsoo@gmail.com', 'ff48bdcfe05b7bfb2a94e8d4747a1d92f898c564823d943e8af9f2f692b54ca5', 'd6cffd4fd2b8210caf1d3788a74b0f4041ad3421', '0124783282', NULL, 1, '2020-09-13 06:41:41', '2020-09-13 06:41:41'),
(108, 'bfd2a7cb8e325468827efa97c1bacc1d', 'Jether Koh', 'jether.koh@gmail.com', '54007eb6c9be2d27d8d592e205d9510d11010ca917df3491d07952a53e1a663a', '817b868917524cf835f01c82c5f051d7653c1eea', '0125616272', NULL, 1, '2020-09-13 08:32:45', '2020-09-13 08:32:45'),
(109, '448789e7550d8648458a23902e97d2d9', 'ang siew siew', 'ssangren20@gmail.com', '2d36a81ed6e02bde13877cca3842682f204cd9fda2bafdce113713d39995913e', '272b9f0419b7f00f534a58589e214f9fcbdf71c0', '0174182192', NULL, 1, '2020-09-13 09:06:36', '2020-09-13 09:06:36'),
(110, '591727a2607671c0a727d98d24dfe472', 'Chris', 'Chris_lgk@hotmail.com', '748a50ff2a58304591d75ae2ebc632316232edfd09a5955aff820678927d176d', '159815073b53dfe89cbfdb0b2dd45ab830cce958', '0124183873', NULL, 1, '2020-09-13 09:38:06', '2020-09-13 09:38:06'),
(111, 'dfc0090cb992c8aed2e3b5468d77f6ab', 'LK Tan', 'whitedovetlk@yahoo.com', 'ac0758880409e6537487527a42919b5bd1f344ac97b487b243b4646960b37409', '64e1983c931488ce31ff563fc8c74436d21e63f3', '0164964476', NULL, 1, '2020-09-13 10:10:31', '2020-09-13 10:10:31'),
(112, 'da74fd0ecdaed32f10ba0375d832a7bb', 'James Tan', 'jamestth@hotmail.com', 'e743b1074a4002daeaf0a28de39f243e37cb0f9e9c6e6ce9cc7043edc32105ce', '1f229d141404faf721c1b24930f2105ddc8f0d35', '0196621673', NULL, 1, '2020-09-13 10:14:45', '2020-09-13 10:14:45'),
(113, 'e2dd50d8826fd7feae5fee269d5be63b', 'Chai charng yuan', 'Mynonis@hotmail.com', 'd55b5280f20b4b185df31fabb28442935dff3bc3023589a791343462d83207d7', '29e14bcb20ef1a6a3817a67511c88866977e75a5', '0123731913', NULL, 1, '2020-09-13 10:18:00', '2020-09-13 10:18:00'),
(114, 'e27ff5768b46f8b3f87844cac75721bb', 'Recca Loh', 'wewiclown@hotmail.com', 'f6345277ead9fdc3d69781f069daba94bb7345107030aa15a68c5227c6fec9f4', '389dc2d3daac699fea9511de669c2f87e91d2fd1', '0125321045', NULL, 1, '2020-09-13 10:20:21', '2020-09-13 10:20:21'),
(115, '86e71b4eae202ee7e535de1de74fda9b', 'Ooi Yinn Pin', 'yinnpin@gmail.com', '3716236dab616b11bdd522bc35f94cad285c96e352e00b8ab4e12116a0fb60f2', 'bab692e2c7589485e7ec30413342cc5e2f40ac58', '0124711408', NULL, 1, '2020-09-13 10:21:21', '2020-09-13 10:21:21'),
(116, '5e84734abddf7f0bbabf640f452f693c', 'Ng Irene', 'ngirene123@gmail.com', '256766d4f4db782c6ce3232cbee4c7a08e0fad9b662c7d86fee86c7d93223683', 'a8d58908cfa443cfb0ee71ed5bbbf44ca91b640c', '0124679887', NULL, 1, '2020-09-13 11:27:19', '2020-09-13 11:27:19'),
(117, '56525a15f03810b79fabd05113b4ab05', 'Ng Chyun Wei', 'Jeffz_93@hotmail.com', '4be3e6b8dc771d8aa151b6667eb4a8bb8a64975fa18caede4d95ae90a6e06b4d', '7c10d41bf769baca7a02528f02173156937e25f4', '0174271899', NULL, 1, '2020-09-13 11:29:21', '2020-09-13 11:29:21'),
(118, '0e5e6bafbda3d0e7eeb6d2be0ba9613c', 'Tan Tong Yang', 'tongyangtan99@gmail.com', '1157d215f04f8139a4fbf1504d397b3918579e4fe0a4e06614f576d5d81eed3b', '9f9f57b3ed4aa4ae49726bcd86e3358152a943bf', '0126689123', NULL, 1, '2020-09-13 11:47:06', '2020-09-13 11:47:06'),
(119, 'f068f563f94bf9fcb932d22340a99b5f', 'Chong Khai Jong', 'khaijong@gmail.com', '17ace881a36f1e2777ace1e7df672e73d6b28e94edf5794db577f1baf2064e42', '6bb1c22e739f043fdc366eddcc7cbddb675f61c1', '0127588542', NULL, 1, '2020-09-13 11:48:58', '2020-09-13 11:48:58'),
(120, '04a830eebfbd6a895c5c6efa10e7200c', 'ONG SENG LYE', 'darren_osl@yahoo.com', '5acd4bb5cfe04a2fccad4561d9e1dd821a3cb6a64e0658d6189e696d4d529c6f', '0d45c555c6ea776f84d4a2872efe215801edfa30', '0125487669', NULL, 1, '2020-09-13 14:17:52', '2020-09-13 14:17:52'),
(121, '948c4a1a542491f1d5459b8680d4237e', 'Heng', 'fotomate2929@gmail.com', '5c19beccf3934697b2d067e04fde0f06a753ce70b24937e66fe6636bfb77498a', '54be45131d8f11b4c0608fbb783ad015968ac05b', '0125586333', NULL, 1, '2020-09-13 14:31:46', '2020-09-13 14:31:46'),
(122, 'cb224c2a10efe0b8d9f9a7547f6f5cde', 'Siew Yong', 'ooisiewyong@gmail.com', '157b89a222148f64296d642307f08865974d2a9115a3e34f96327b14bcb3988c', '7c019636e829cab32ccf729c5cadd26bd2f7c89d', '0164179393', NULL, 1, '2020-09-13 14:54:50', '2020-09-13 14:54:50'),
(123, 'a099fe6667295774ac6655bff7f31dde', 'CHONG WOON FAH', 'wfchong829@gmail.com', '67d316147d6c2ec247d6707728e8d7e1f7a20ae1103826b6c864fdb288c658b7', '7590231e7006ec1721ad1cf81d87cf513a9a871f', '0174889188', NULL, 1, '2020-09-13 15:09:52', '2020-09-13 15:09:52'),
(124, '56022a709750ac5f5667a8f886eeaed1', 'Lee Beng Tiek', 'tieklee0910@gmail.com', 'da148c24ec5521dc17d1defefe18b4c247b98c828c589e6924f138935d7e137e', '19ef81ea9d0f1dab2ce6845fa4cdbf9a0e2e3ceb', '0164261122', NULL, 1, '2020-09-14 00:56:01', '2020-09-14 00:56:01'),
(125, '6da7bba935249906800cde6cce3e68cf', 'Dato Lee', 'Teong-li.lee@amphenol-TCS.com', 'ebff394fd3ef6acd51f550f72bf9c68d090113bf9e137e1a455f25fa16c2a508', 'da71765868698397677ecdd24432e114924536b6', '0124778181', NULL, 1, '2020-09-14 02:19:06', '2020-09-14 02:19:06'),
(126, '4ca19e41765da6b49dbcd66ea11c1ee6', 'Koay Kah Guan', 'Koaykg@gmail.com', '50d2241c9c6dde691da1a74598a59954d76cf94a037c575193d045b4c316d143', 'e542526ccfd25febec41c6b249536db1c6adb5a2', '0124881047', NULL, 1, '2020-09-14 02:35:55', '2020-09-14 02:35:55'),
(127, '2fdd37305fb232562e6649f05de4beb3', 'Jayden Cheah', 'kmcheah94@hotmail.com', '1c11f8fcd7942346b7b22a2f5a92854d60fa8b41e0efa4ec9d78fbcb44cacd52', '5b3c2d4e1f58e69aa7a37810dab355ea632424f3', '0123440869', NULL, 1, '2020-09-14 02:40:17', '2020-09-14 02:40:17'),
(128, '46e09dd19c03ae3c58f2495c9ce956dd', 'Chong Hai Li', 'haili-94@live.com', 'ffe6689fd7ac151dbe15caf0de114332af5e560545dcffd78f4a423f2add8b48', '8b9e6fedb2440e406b41f595ad23d14bd31d51d2', '0164782886', NULL, 1, '2020-09-14 02:54:07', '2020-09-14 02:54:07'),
(129, '42236e7e6293aa52879084588e7a72d6', 'Simon Loo', 'Simon.loo@hotmail.com', 'efdab576a88c3cb045378f436e3621e9ff49c4a6069be710032cd2d1bf2ccc0f', '8c004c1f0a5bda53ab75d23a7c01dab8bc5fc114', '0124438632', NULL, 1, '2020-09-14 04:46:12', '2020-09-14 04:46:12'),
(130, '66783d356a8accb34d58df30c9e47291', 'ANG AH EAN', 'angahean@gmail.com', '0d08e97c2a8c6490d202031e6e44ca896ac24077becf9a5317386133c630980d', '1441e0ccd7ef05845fe422385bea257ab91dc2e1', '0124815510', NULL, 1, '2020-09-14 06:04:31', '2020-09-14 06:04:31'),
(131, '1a73b804f8fe09858320faac343e4759', 'Ng Kok Hwa', 'ng_kkhwa@yahoo.com', 'd8adeb84770aa1bcc21807e72da13f14b0026b3205f115ff51a3edced0ca8ebf', 'f52dca37aa3b947b440e4c4889e1cdee6c38faba', '0124438268', NULL, 1, '2020-09-14 11:56:58', '2020-09-14 11:56:58'),
(132, '6eb7e36edce7327b5c6d94a12d785ac5', 'Ong  Hock Leong', 'Eddie7700@hotmail.com', '02c317f53ea947551f4046d61a655304ff2cc0955c45dff227d1adb991aa302d', '4e595a45536f695e8c6ddab3a1424c4e93945346', '0124780881', NULL, 1, '2020-09-14 14:06:10', '2020-09-14 14:06:10'),
(133, '0ac03abf9306842a01ded8da0a202b12', 'Penny Ng', 'pence_ng1201@hotmail.com', 'b8f36a2ac63e433c8e5cc0ee19889abccc8a92a36f5305b6082187718fac529d', 'e3345f4b469a704606cf613cfa763b9ec26ef75a', '0124022066', NULL, 1, '2020-09-14 14:07:06', '2020-09-14 14:07:06'),
(134, '1f69e957c55ed5516f5fad17eb7b227e', 'Khoo yuet boon', 'Kayabun80@hotmail.com', '59dc5569dd573a8444fe8e1503cd615226d427caec7c6eb1244143627692d106', 'a8ccef4b08139f167380ddb5ed5868c6423cf358', '0194666613', NULL, 1, '2020-09-14 14:15:08', '2020-09-14 14:15:08'),
(135, 'ae1dc61c75446f7b1773099e883b4dce', 'Chuah chin hin', 'Chinhin @hotmail.co.uk', '5230e6cc2b2580adac1934abea41d88c56f315a8c3b87668aa57e085ccbc376b', '64d004e4683b8baf10ec8881f51243a3c2f0fd05', '0194472928', NULL, 1, '2020-09-14 14:16:17', '2020-09-14 14:16:17'),
(136, '61420a7e72690c5c5093931c56f3cb8e', 'Lee Wei loong', 'Jakson_8888@hotmail.com', 'c7a79876a659405289a3279de8c57e16ad195bf064ebb3662e495ca2eba1ba1b', '433c427b092a51077c1865a5505e5115396ca9ca', '0164801419', NULL, 1, '2020-09-14 14:59:12', '2020-09-14 14:59:12'),
(137, '46fbd83faa829a2bc3beaa616660fdce', 'Rayson', 'rayson_cts@yahoo.com', '7cfd6f95f6d8c3193f6cd34c87af1f5efb0daef2332e910c56aca9689be9c768', '40022320e8366aa317bc1d345684822a7e2674b5', '0225673166', NULL, 1, '2020-09-14 15:01:34', '2020-09-14 15:01:34'),
(138, 'bd9e680d4b65756a5c1cf1b9b1aa30b7', 'Philip Yew', 'Philipyew@hotmail.com', '35553da0e63efb3df9ea4c6e7b95c051b6f5e91dfd34cd8fdf5c830aa11f8605', '197a46c32322a034460dcd9b1c50cbc4300f551b', '0134776868', NULL, 1, '2020-09-14 15:28:56', '2020-09-14 15:28:56'),
(139, 'd884ff38fa9de3313d1fde720c79c2a9', 'Choong Yen Kong', 'cyk7354@gmail.com', '14b5636519aa2bc4835506689d6500ab8eebf33211a8a7545d45a7bf207114b1', '95503ab1804eae967ec16d2f5cbffc6814adad1e', '0175616988', NULL, 1, '2020-09-14 16:35:03', '2020-09-14 16:35:03'),
(140, 'e24f908df94c54fc319b4a679ba2ad7a', 'Ricky Lew', 'ricky_lew@yahoo.com', 'e6554f74228dab77edaaf289bea3225dfe924712dd7f736cef06b294143886be', '28d075d8c3e2699726a37562b5588802fe349911', '0165265823', NULL, 1, '2020-09-15 00:30:28', '2020-09-15 00:30:28'),
(141, '6be1ff5179d2e70348f5e379694b28a9', 'lim boon teik', 'jlim362@gmail.com', '17b8c98183f2dab6360ada3fcbec9c78e7e9df8a161ab8321f34b95525304560', '7e976bc2401835687e64d4f2fbb1f771a35576bf', '0164199932', NULL, 1, '2020-09-15 00:48:59', '2020-09-15 00:48:59'),
(142, '35ae1a3c3e057af5ca587d3390512259', 'Ng', 'Jimnglh.lhng@gmail.com', '6a34fd695785bc9d96bbe0988d29ff0a265678392da416793f851910f25bc93a', '7007acde3adf3023a886cd9256441e10b67213fe', '0108908909', NULL, 1, '2020-09-15 00:50:49', '2020-09-15 00:50:49'),
(143, '33cedf59e066c0dd304d6fab2da6e519', 'Koay Hian Beng', 'koayhianbeng@gmail.com', '88f3f8317cfc7d704246344751482daed3886143a58b2ff86731d743037f4b20', '7e83a1480c50f285f4bc4c52bcf204d28943b902', '0124666903', NULL, 1, '2020-09-15 03:36:30', '2020-09-15 03:36:30'),
(144, '9bae92ffa6fb2f2148aca170e0c08b13', 'Lee Eng Leong', 'klaoven@yahoo.com', '9f8f81093feb91fca297852775dfaf4955820be410dd394a443e9951dbe4fc7f', '22b9b1df434c592db8c3f0ecb087c833072fcb8d', '0194883576', NULL, 1, '2020-09-15 04:26:30', '2020-09-15 04:26:30'),
(145, '31a1e18bc08a87a9b329b5fc144f01d6', 'Ang Beng seang', 'Bengseang@gmail.com', '2aa46480d0e8d554124a8f67b9df52ea9dddaffdec36e69b0f8da7a4d0ad7b19', '73573b0faf5b63370b2c055a642fb00a2bd93653', '0164711778', NULL, 1, '2020-09-15 04:42:53', '2020-09-15 04:42:53'),
(146, '992e954e01eda485a76da453815b1d82', 'Tan Chee Keong', 'cheekeong_my@yahoo.com', '01507ad4e38a8b7a1689279642e4b0b5005750d47cf29badc3fcda768ee10213', '898c7eaede1bc2b8b021d37c1bd0587b8bde2a7c', '0164755376', NULL, 1, '2020-09-15 05:05:40', '2020-09-15 05:05:40'),
(147, '99bd540c7ee295d5b9981fb01dae1779', 'Tan Poh Hing', 'cystamp@gmail.com', '6edc64549ab0f091886dbdebe7c4f38f7d8258dd355598337a8c65c5e35ab8ac', '666b43776019524f30995851840a6f1a23aa7944', '0164034162', NULL, 1, '2020-09-15 07:32:34', '2020-09-15 07:32:34'),
(148, '385390f6c21e51b1fd8678255124eed6', 'Thum Lik Tatt', 'sonygrey97@gmail.com', 'a020b052cb7e923970c6a9be7aa9995eaa014c5b1e12e2f3cf0e851796f929aa', '036f549d599e6570a18424de24a47fb87fda4857', '01116561098', NULL, 1, '2020-09-15 08:48:05', '2020-09-15 08:48:05'),
(149, 'ff029ce240638019eaf8d9c400088ef4', 'Tan', 'lilianloke947@gmail.com', '5d7ea1c371a23485910da0b628d9b95cefadc454a2f673b1b407b03f239b75bc', '9589dbaa1bee7bd3680e372bc3034d8d21ad9bae', '0146035875', NULL, 1, '2020-09-15 12:30:42', '2020-09-15 12:30:42'),
(150, 'b203c097e594caca628ec2c9c9e54f9b', 'Ong', 'pohaik@hotmail.com', 'bf6b3927380715b2b03f14302d7e10ec1a052cd5eed693d9a787ae238ec681fa', '8e047f8cd9f5e025e7723f3e8aaf65381ec5901f', '0164228771', NULL, 1, '2020-09-15 13:12:51', '2020-09-15 13:12:51'),
(151, 'a77b8e6a0a3677a2f3e7e50b8265630d', '', '', 'be396252ac7afde7fef8d8e0488f325171807120c9a3b81c5e53e7adcdd8449a', '6fd9bd6ff6312aef174844b221cb9123ff270a18', '', NULL, 1, '2020-09-15 13:41:33', '2020-09-15 13:41:33'),
(152, 'c709791f5cf836aca4fdef9bd4dd8c88', 'WONG KHAI KHAY', 'Kkwong3049@yahoo.com', '7ba0cddcca14b2902a9340d7db4e0de6f6290fb3fb690d056053ad4bb5f66dfd', '6eb33cb2ce6cac4d5cdcbf1614879f3f592bd240', '0164192038', NULL, 1, '2020-09-15 14:04:37', '2020-09-15 14:04:37'),
(153, '52bdc993ac2c1f882a8ab47fcd974345', 'Loai Peng Hoong', 'loaiph64@gmail.com', 'd40f357918472d492380e8b518d7e5b7ab1f60ad7529fe83f4cf394ef2acaca6', '6595f048079e062f7cdaa55e5e107b8276d528ed', '0175015417', NULL, 1, '2020-09-15 14:45:48', '2020-09-15 14:45:48'),
(154, '979d72d01f99e9f05e4bbf74a2fde382', 'Sally Goh', '5sallygoh@gmail.com', '482d626c0356446e65175a8b3b9b8adeee44ba7d198abfe9b62d68f5480cfb36', 'bf98417eb2a0c5ac321fb46751780e950eabda33', '0194705799', NULL, 1, '2020-09-15 20:19:11', '2020-09-15 20:19:11'),
(155, '620a8ca0ef4254c3a708ebd6f62ba0b2', 'SAW KIAN WEI', '', '7d17b456ed51a2b4ef6024afa0952022c7747cf0bc41d8afeca53b9fc59a6ba4', '1484eeab4488f6c5bd98b2a19de3b62070f255d7', '0125678699', NULL, 1, '2020-09-15 23:46:46', '2020-09-15 23:46:46'),
(156, 'c8a92f8e05d657a6da59327362cb8cf5', 'SAW KIAN WEI', 'davidsaw83@gmail.com', '981ea01ce78ce8b856637169eb6dd5efa2bdf8c4fd3732d8ee670767f5a9f73f', 'f9a38c4a00ee4ce11d4b996ba9e504f62e9c40ed', '0186884211', NULL, 1, '2020-09-15 23:47:59', '2020-09-15 23:47:59'),
(157, '9590d174903c957273d7eea0f8a00038', 'LoonHooi', 'loonhooi@gmail.com', '7a173b12aacf47e192c4481cd7741d866f362d2c6da3f0e28985dd4bac1e39af', '7103e73c5d35211b76a7b00c3d00f811b64a0a7c', '0164752782', NULL, 1, '2020-09-16 01:14:57', '2020-09-16 01:14:57'),
(158, '46044af321f357e44cd8995c048b1cd6', 'Cp', 'Cplee.personal@gmail.com', '02c249c697b80fee4d35c798f9d1e6b5ee33c17e410e137a5d1abda332512f70', '1cedf51fd3335a971d28885035800854dcdfb454', '0125788668', NULL, 1, '2020-09-16 02:48:28', '2020-09-16 02:48:28'),
(159, '58afed1a30af37c467f203fd6ffd2c55', 'Pek Bee Hong', 'pekbh88@yahoo.com', '94edce8d33ec7f6a12ec63ef1c04dd1fc3198b2c368ac66102f9d41b620d4587', 'e1c081b7af0175b611a1ff9b291c0098eb9213ea', '0124618355', NULL, 1, '2020-09-16 04:13:08', '2020-09-16 04:13:08'),
(160, '793ad196c1954aa06c6a1667a54547fb', 'CHNG', 'clchng@guangming.com.my', 'c120ee7fe7b9985672f20092d6bff5e66ee4723857a375c74c7e05d1df4d1852', '78187dfcc09bebb526d28c13d92b5ccf087fa0b9', '0129628975', NULL, 1, '2020-09-16 05:43:05', '2020-09-16 05:43:05'),
(161, 'cd77264c9384dba0da771584ef1b58f1', 'CHEW CHONG KHENG', 'chwwllm@yahoo.com', '7fa492f0a78aa0ae37250aaaab21395cf9dc5975107c0e92b20e7b5b8802936a', '25767205308ed8e7e293c7a6cf5699f6baa606e9', '0124287268', NULL, 1, '2020-09-16 07:06:11', '2020-09-16 07:06:11'),
(162, '3f1d2bd06c596e2fd77892a618f0b557', 'Joshua Ong', 'ong.joshua@outlook.com', 'bd1698ce15593b3a2c93f05ed1ffec35e936fd3ad4f270fd0f903eb622319647', 'ded79f621597abffa383dbd78c537cc1376a36a0', '0124543014', NULL, 1, '2020-09-16 08:18:38', '2020-09-16 08:18:38'),
(163, 'f11a00ea202387c7638fd0c004e966ca', 'cm chong', 'benchalon@gmail.com', 'abac17e89dbd30d266ca5d1c3e37912245cfb547996afe5daac14ecee35b2842', '870355a66953b96a4710296c9421bd7d69fe1bad', '0149019960', NULL, 1, '2020-09-16 08:44:10', '2020-09-16 08:44:10'),
(164, 'c0156963ed5e8b53d18dad7c4472bcbf', 'CH&#039;NG', 'sweeean@yahoo.com', '9094afbccc015e73e1a2c9e2db8510deddb31c607e0fa94913106ac7d1fcb153', 'dd6abafec02735a9509163142a434dca87c284bb', '0124668722', NULL, 1, '2020-09-16 09:05:18', '2020-09-16 09:05:18'),
(165, '6f0f2ebd5a93d944e146f6551b2134d6', 'FCWONG', 'fcwongpg@gmail.com', '02e963dbca89de724ed4bc5595015a03f8d333fed4c4fa1042097deec3052a15', '3fe795643b79d4966d6459288b0362464a7e41e5', '0124545315', NULL, 1, '2020-09-16 11:45:20', '2020-09-16 11:45:20'),
(166, '30e7d148c2edbae44ed21bf12fb2d353', 'Alan lim', '', 'f3548ff26f6138a467081ac11211ad6f19c754f1ef3bf26dfe7bc623dd61d056', '8764e405200f0e0c63f170b6c52fa250139fa3da', '0194475585', NULL, 1, '2020-09-16 13:00:06', '2020-09-16 13:00:06'),
(167, 'c67e7e96016e96ab993e1922fbf79da0', 'Khor Wei chen', 'weichenkhor@gmail.com', '83fcc39b04a28833fbd03a670288cd47d9d88ae6f309e0c0b75469e7ba123858', '806b0ba1d46939b04cd8fe61bb150032b0453840', '0195772125', NULL, 1, '2020-09-16 13:01:43', '2020-09-16 13:01:43'),
(168, 'a2124344a89aaffab8f5b609b7731a9b', 'UNG GIM LING', 'jinh_ung@yahoo.com', 'df8537fd5fd6e0bb3a8d693d55ad4427c764487637f161409427a080e531d1f6', '7eaa240ba50fb1ec27aa4371173bb1b332077245', '0164223530', NULL, 1, '2020-09-16 15:55:53', '2020-09-16 15:55:53'),
(169, '60e1ae36a62375e1275825d3ef06d4d8', 'CHEW KIA LIANG', 'klchew6@gmail.com', '72dec013c62345dc0f33b201695a6e15cb3e33a261822e7f8f13c46ca991797d', '484c698475ca60e24fb751a2883561b328cdbdd1', '0165534126', NULL, 1, '2020-09-16 17:05:38', '2020-09-16 17:05:38'),
(170, 'fa73dadb2b9d96c990f1d32f7fa90f59', 'Eunice', 'hlingchng@gmail.com', '6dd84f7ebdb7664b0f20b4350b8b7777abf62a9368e2ba40ce63212851bf9152', '78a84f018cb113155cdd71e8354662759240d7c6', '0127172808', NULL, 1, '2020-09-16 17:06:40', '2020-09-16 17:06:40'),
(171, '737e62aefe5f17913b5518f3cfb7fb4f', 'Jeffry Liew', '', '2436e57d84086281957cefa3a4b7738887e93a20cac27c81e2687c59891dc418', 'fc33b6fba9618cc9c665588bd48c156032a60229', '0124516367', NULL, 1, '2020-09-16 21:07:59', '2020-09-16 21:07:59'),
(172, '25c6f38042c090873ce6b2bb3fd7f971', 'ooi mooi yong', 'maxteh@yahoo.com', '36333f2d09fdcbbc1bfd72298045155953179d158c4a90c6eb458f0dac33c298', '998968d9bb6d7c3a9dd5367d4f39a20ef9a67c26', '0102221043', NULL, 1, '2020-09-16 21:12:35', '2020-09-16 21:12:35'),
(173, '930166f2abbcfb3b4e874025ec7bd9e0', 'Choy Chun Keet', '', 'c1b5a217f670c6e9342a8904af596322bedb7c3f7c3a8ef054eb21e135b94b23', '9518c0bc7a118dfc382597bd199f648a2e2ed34c', '0125535126', NULL, 1, '2020-09-16 21:13:29', '2020-09-16 21:13:29'),
(174, 'f4debd8073ca758cdb290f96b8e40a13', 'Lim Jiew Chiew', 'jclim55@gmail.com', 'f5225eb14c05fb4e79e73a613dc9065848749b9db2c5ab9e2160e6781012bf85', 'b2f565f14cbc23276ab1cbe3a6a5665a9fdf033b', '0164206189', NULL, 1, '2020-09-16 21:18:46', '2020-09-16 21:18:46'),
(175, 'd78e2adaa3fcb1b4661a8e5612fa867c', 'Joe', 'obze91@gmail.com', '9ee3835eca09c198d51b42ab92cca894ca982e90c91dc4fc52cfd7efe7a78ce6', 'd5b9a0ab737f65704916d99b8a434cfbb73d4e23', '0199390215', NULL, 1, '2020-09-16 22:46:45', '2020-09-16 22:46:45'),
(176, '9a3c8f4bae3528718d09473325fd9033', 'Vincent Low', 'luweilong23@gmail.com', '5a4d6fdd0c57eef2efd20031bddee174307873e61a3c421a4bf025dc6f790485', '72d08d04edcd8ab4864c01d6b522853fa5415758', '0124200988', NULL, 1, '2020-09-16 23:24:31', '2020-09-16 23:24:31'),
(177, 'b1b5fc14584ffc3ddf05470492b51975', 'OOI POH SENG', '1986ooipohseng@gmail. Com', 'a5b4dbc170e1295160bf8e86f98a79d32c3f94a44921f78cfd1002e68b2daa67', '291fce42a382b985a4f796610ff8c8afea884fac', '0184604879', NULL, 1, '2020-09-16 23:31:07', '2020-09-16 23:31:07'),
(178, 'b0358a467baf176cf15d105f57f64d5d', 'Lai cheah swee', 'laicheahswee@yahoo.com', '3a3d9d063f1730c34c3d1e4424a26778a47d4913cb96cde893e207e8d508ea0c', '83715625d3fca9c3571d422d6a02a71d40a623c9', '0194000261', NULL, 1, '2020-09-17 00:36:57', '2020-09-17 00:36:57'),
(179, '4947d4a8601c4511097c849c18c97b58', 'Sim Khoon Hooi', 'simkh2@gmail.com', '392095394e4b2364472431e6cad4974d4f4d4d4314c1cb05bd8f462e355a4927', '4c84362de9f2ad66a75b455f839d347a7f9812ad', '0124618024', NULL, 1, '2020-09-17 00:50:50', '2020-09-17 00:50:50'),
(180, 'bdd3d775398a587a86fbc6e6cffbecc3', 'Lim Sheau Rou', 'sheaurou@gmail.com', '0617515605f051c8be213b891dfef7c32ef94b22b889a42e5b71474be2266452', '2f9f8a120b85e4a7f4c22e0acc5aad1df607fdfd', '0124583172', NULL, 1, '2020-09-17 01:10:59', '2020-09-17 01:10:59'),
(181, '061b71e540b9991c5c2a295d47985ff0', 'EL', 'ewelee1@gmail.com', '48db07eefe24509cb11cf29f3c77586d36672063996d241bf028cd71f4a4e549', '8eee839cfd7a878c1802002b418b5780858e3e19', '0165559210', NULL, 1, '2020-09-17 01:19:28', '2020-09-17 01:19:28'),
(182, 'a50f16e7a8deda11b1c159d08df77b63', 'NGAN THIEN HOE', 'nganth66@gmail.com', '711b3d7f2b728fb27935aa0bd1709a0c13c3013dd229135071944e606294dcdf', '0a67c5b9bbe2681754f93749734c796cbe459c88', '0194789998', NULL, 1, '2020-09-17 01:36:04', '2020-09-17 01:36:04'),
(183, '19be2f2d12b2c2ea3661f1a5bd203a02', 'Seah WC', 'srah_wc@yahoo.com', 'f90016a830908e88d9125572afc876560a52eb149b1e4d5596c2bb50577a3dff', 'ae576cafcf067b6fc0231bf6bf99ca1537a857f1', '0124302107', NULL, 1, '2020-09-17 01:36:43', '2020-09-17 01:36:43'),
(184, '2b2e8ed9fd40201450e87e309a1d0ef3', 'PW', 'pohweng@gmail.com', '4f45729da097b29fa9df3821552c25319754e89e3494959627e67fe15d300390', '816064acdc21f5b478ee0e3e875cb2984e48fc2b', '0125253585', NULL, 1, '2020-09-17 01:53:07', '2020-09-17 01:53:07'),
(185, '165a3f4c83c8b063b659977f88c8e3cf', '李慧卿', 'qinglee1122@gmail.com', 'f0605e0a90826132762c21c5794726763294fcda452ae766baeb8c68eeb3ace8', 'b16308902e44724d13bdc4f640ebf6f9869594f7', '0126597897', NULL, 1, '2020-09-17 02:55:33', '2020-09-17 02:55:33'),
(186, 'ba951dd5c02fab60743a1d2fc2d2b4b4', 'CHEAH YEE MEI', 'yeemei.cheah@wellpoint.com.my', '27ef976ddd5fdd28833fbcf4a721ce60bedccd1209294986610a44f94d38c934', 'ae71b169337fb13444574494644f3793301993d1', '0174158003', NULL, 1, '2020-09-17 03:57:19', '2020-09-17 03:57:19'),
(187, 'fa34f67eab5c69d48d2e14932a89fd59', 'JANICE', '', '15a122cd2f0d14f42c9e82d736b9944517584a4e01105ee54676a87f44e8f74b', '3b5634ade790693482f0f2569e15b01ee232806f', '0122820802', NULL, 1, '2020-09-17 03:58:15', '2020-09-17 03:58:15'),
(188, 'e6bb93bad284be754d3958788b15181d', 'TAN WEI LYNN', 'weilynn.tan@wellpoint.com.my', '8eb43f155a168acef51ac6eeb749987760a235d7e69f53ebb1adac561fdd5b44', '7f68659bced58d75785abad902ff02617e410013', '0125252839', NULL, 1, '2020-09-17 04:00:20', '2020-09-17 04:00:20'),
(189, '8b378ab673c11932b3974aece3083188', 'WAN LILI', 'lili.wan@wellpoint.com.my', 'd46a99150c33ba8611ae1e579fd2ef0d060c57e6d5f8a9cdf7cbeefb21ee045e', 'ef3a9046251d5e566ff9b5f57d377f7987f210f2', '0124651668', NULL, 1, '2020-09-17 04:01:01', '2020-09-17 04:01:01'),
(190, '06a638ccffd848c557d34eac596c80d7', 'CHEW KE XING', 'kexing.chew@wellpoint.com.my', '05731a942a36918b0287d3eb39184f41a10e90160c43040ac5f909a09a471361', 'dd33d7ad2211d92a7ce5b958d0fbe0ac9ae99b0b', '0184086998', NULL, 1, '2020-09-17 04:01:58', '2020-09-17 04:01:58'),
(191, '5b59f2d781d417cb38fe088479061792', 'NG MEI FONG', 'meifong.ng@wellpoint.com.my', '52684faef56e79ce4eea03b953d5ac148f8d49596ae75f2c9eeb446b2dc5e924', '8fbec80d8988e92b8e44c58c869f8c6c376070eb', '0189870511', NULL, 1, '2020-09-17 04:03:06', '2020-09-17 04:03:06'),
(192, 'd091d7b79f2917b7e9b4f121d5143514', 'SONG BOON HOU', 'boonhou.song@wellpoint.com.my', '8c3408e17bdc757236272651db6f41266b5fecb4758a707cf7d91709d38cf8e2', 'ee14490f4be68adc809445e8066a49b1e5514384', '0174303850', NULL, 1, '2020-09-17 04:03:44', '2020-09-17 04:03:44');
INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(193, '1470b3aa26bc3ee42ac84b2f37683078', 'WEE ANCO', 'anco.wee@wellpoint.com.my', 'ce8e8e07c445fdae7c331d4917493ab8e30ab09597c6512a34587373ca959fea', 'c5d13a809ac9f1c5278869b8834086be453f1e36', '0177681809', NULL, 1, '2020-09-17 04:05:04', '2020-09-17 04:05:04'),
(194, '3d4ca709e6ccb224b14696649379e2d7', 'KHING XI NIAN', 'xinian.khing@wellpoint.com.my', 'c7ee45ba882fb60f7f3440cf6ce3f2820deba368ddfa3c306543742d707d97f5', '66abb2c2d2139b461ca4e6ba6d315e5c4bd3923b', '0174371794', NULL, 1, '2020-09-17 04:05:39', '2020-09-17 04:05:39'),
(195, '24105ece3a41c148caa3e2866230fe40', 'TEOH KHANG WEI', 'khangwei.teoh@wellpoint.com.my', 'f6c6ff49e97efafadb3724ea662972838ec5767b7870af3d3026415fe9a7dc86', 'c0411affe5135384d91db09364c26bcd61b94d19', '0184663010', NULL, 1, '2020-09-17 04:06:20', '2020-09-17 04:06:20'),
(196, '562fcda27a34e53b853b5115d9e031c4', 'EOH YEE CHING', 'yeeching.eoh@wellpoint.com.', '8792558811aaf9632ddbec70563680a4bccfa27560cbf234ac2a25f2b27bae2a', '246fc1b655bf87323cff9b83548103a7ceeb95f8', '0164246364', NULL, 1, '2020-09-17 04:07:02', '2020-09-17 04:07:02'),
(197, '377b3157dd683e610ecab346f15bbd5a', 'LIM HAN NEE', 'hannee.lim@wellpoint.com.', 'd3e2d457ac571cd4c9d73e1a5dbbac1cacb07765351b25584ebf5fe2a7be27ae', 'a6b3652c9732517f3af6a299872fb1acffe17ae9', '0194216318', NULL, 1, '2020-09-17 04:07:46', '2020-09-17 04:07:46'),
(198, '70f0f90ec09cb1fb84fc9e013a7ea217', 'LEE CHIA XIN', 'chiaxin@wellpoint.com.my', 'fb982e6a773a3a7494919f92bc5e084deb357f9619fd07d324a14bda92d5a3a7', '5e0ffec4fcf2ec9dd757ef23613f64518d85985b', '0135347820', NULL, 1, '2020-09-17 04:08:25', '2020-09-17 04:08:25'),
(199, 'be8b40c82127c3244783ae29974bba43', 'Vince', 'Vskl1811@yahoo.com', '809fe3b05b2ec478d0466e2594c5d713d58af6343d4861b7172f0557e6e4330f', 'fdb4d04de9f5d67d86621fa35c21ff4fcd350415', '0126448868', NULL, 1, '2020-09-17 04:17:20', '2020-09-17 04:17:20'),
(200, 'ed9136911dc7a28df6d0a35ca6b26349', 'Edmund Chew', 'chewannboon@gmail.com', 'ecd2d5286935c10b5e868346e176ea04909338c06ce092267b8459d0c8e9e0cc', '8e5b8976641e33be9a8bf8bbac57a3ced854b953', '0165661707', NULL, 1, '2020-09-17 04:50:09', '2020-09-17 04:50:09'),
(201, '0e7977c17fd8acfb977ae56bdc7c5b71', 'CHEE TOO TUCK YUAN', 'alex.cheetoo@wellpoint.com.m', '124a87e036aa0078ae513533b8898fcba863bdc65e3d743159a68f783e802a2e', '53ea33dbc94765a6768acb2b7a41991843545ae1', '0143418790', NULL, 1, '2020-09-17 05:40:15', '2020-09-17 05:40:15'),
(202, '9b48a7e778097f97b69d31706271c416', 'PHANG SHI NI', 'shini.phang@wellpoint.com.my', '8ba201d54d96eb2d6762e03c84926b23add3c40c0d2ff761e8c91fdaa49b7b24', 'bc6724bde1e60c5d1f8f6d49d8db28d6d2746abb', '0164289282', NULL, 1, '2020-09-17 05:44:02', '2020-09-17 05:44:02'),
(203, 'd01ef46ceb190d8ddf53c47cb44aef8a', 'SEE HUA', 'seehua.wellpoint@gmail.com', '0a3af9150d1f4d40504e73bdfac0609cfd55be03b44a406b419f20c33fb557c4', '2a6033533f9d663075b6da3a41fd7eb08f8febfa', '0134888683', NULL, 1, '2020-09-17 05:45:01', '2020-09-17 05:45:01'),
(204, '06218559015d027bf126dd22bbc2b7a0', 'OH SU HUI', 'suhui.oh@wellpoint.com.my', 'cf1e433fa010ea99f83d64a05d52d55315a9ca163a2d6d45188c87d0d065f6d5', 'b8567c29f682ff54a20f4548a81fc85e023237c7', '0124989021', NULL, 1, '2020-09-17 05:45:45', '2020-09-17 05:45:45'),
(205, '86e2b293b3efc80fb81b3aa16226a800', 'TAN HONG PING', 'hongping.tan@wellpoint.com.my', 'fb5f5d4d02773611748554caf361356098f9c711cf66a7a6b1b94b4ca4109069', '7f406bb95c994c1215b254410c0543fb60e78290', '0184034796', NULL, 1, '2020-09-17 05:46:40', '2020-09-17 05:46:40'),
(206, '228b85e3036e99a5ab61a1a0f6ea352e', 'LIM CHEN YING', 'admin@wellpoint.com.my', '3caa3f8bf3023498cbf28bdc8b932dc5667151c6e7eb4f85dee874e5d72b4206', '6b2b223d494cf652a1101b198fe0d9807fe6fea6', '0165215302', NULL, 1, '2020-09-17 05:47:30', '2020-09-17 05:47:30'),
(207, '62847140ff70adff2d05130269148bad', 'LIM HOOI LENG', 'gst@wellpoint.com.my', '1bd8905f82d21177941a74e0fd31d6141743c5a2aacd552feab41e563a3e35e9', '3f86e2a4a58cc2069c64738f3272b50a89cb56d6', '0164140937', NULL, 1, '2020-09-17 05:48:16', '2020-09-17 05:48:16'),
(208, '2b1a4bcc91d70742b404e5a692dd9857', 'QUAH SWEE IM', 'ocean.quah@wpco.com.my', '602f111e5a4115a02f9d15dd046908378a4cd3952c330e095476a3162c96d278', '921e823e6ef09f4c1a5db3c8b5c59f13d01f1734', '0125564103', NULL, 1, '2020-09-17 05:49:39', '2020-09-17 05:49:39'),
(209, '17aab7c0d901a00a688a2de711b4a822', 'YEAP PEI LUAN', 'peiluan.yep@wpco.com.my', 'd49bbc914517c8ff5a6abb6b1745c067f41308f4ae7d186518ef36f3d3e70db3', 'b4ccd605893107af6bc05b280996332209f9d6cf', '0125754778', NULL, 1, '2020-09-17 05:50:16', '2020-09-17 05:50:16'),
(210, 'e03ffc696e570034351af33779902bd6', 'QI HUA', 'qihua.wellpoint@gmail.com', 'b00fa64325bc15f881dc70d1629552d00661040c4770839ebeac487362988de0', '2124327d52c78df16a8e801c97105f8ae13793a9', '01110634679', NULL, 1, '2020-09-17 05:51:02', '2020-09-17 05:51:02'),
(211, 'a9476841680f549944c7607d63bc9265', 'TIU CHIEN CHIEN', 'chienchien.tiu@wellpoint.com.my', '22ba7a77fbb9fd6039a3ecd4e8513f25932b4721d945d9f04d414c449c5dd649', '5cee74240cdae3ba697e346d37a41e4f6ba30e82', '0174150168', NULL, 1, '2020-09-17 05:59:01', '2020-09-17 05:59:01'),
(212, 'd259bde35b8d1f235c0cf3618fcf1e64', 'LIM LIANG CHENG', 'liangcheng.lim@wellpoint.com.my', 'ca927d4def51aa919c9a02d3120a9c2317540a32f2055c51dcfca7f84e14c71f', 'd7f630765403229b4e13c016cc5bfafe677757a6', '0164538010', NULL, 1, '2020-09-17 05:59:52', '2020-09-17 05:59:52'),
(213, '05c53b596a0a7eb6d09534899412cf06', 'CHONG PEI WAH', 'peiwah.chong@wellpoint.com.my', '489915015458568badb2ab9a10b944f8004bd1383ea4c298f836d0889a0b3bda', '8d0437647b8a10869a49e5cf88b2801723dbc83d', '0124308428', NULL, 1, '2020-09-17 06:00:43', '2020-09-17 06:00:43'),
(214, 'c59840c654edef366cc35224c5d54092', 'LOH KHAI SHIN', 'khaishin.loh@wellpoint.com.my', '0437d02b63796ed687d8424cb6ae723df7b699a9ec06f32e81f3d045d1df43f7', '4032973dfb88539562fb68000195189d562052fc', '0164374992', NULL, 1, '2020-09-17 06:01:36', '2020-09-17 06:01:36'),
(215, 'aff9b462eba5a519ae0a58c26f1d2d1e', 'CHEW WEN XUN', 'wenxun.chew@wellpoint.com.my', '831f26166e62fbfa37c7dafa41692b38b67735af31a3dec00debae085d1e0d46', '3374df22e5fa4bfc43dbfc140481865c75e77e2c', '0175109546', NULL, 1, '2020-09-17 06:02:46', '2020-09-17 06:02:46'),
(216, '40b5363bdd7a2cf43a39ee8c3809b345', 'KENNY', 'hoesoon.song@wpco.com.my', '7304e27d12e8ca9fd49af059b271770192f039c42c554df48162b442c3b36893', 'dc0f4374478f0efaa9a4e0eb29de03ac4b3c641a', '0164353979', NULL, 1, '2020-09-17 06:04:51', '2020-09-17 06:04:51'),
(217, 'aeff3e03c4ab808041455284a553b6a3', 'LYE SWEE THENG', 'sweetheng.lye@wpco.com.my', '7d6e2f4e749c47bebf5f7bf1432c862dd40f2057657bedeb2679c490eebf7571', '2ca6432c72eeb084a6618dfbdcea178c90825c2f', '0124260087', NULL, 1, '2020-09-17 06:05:53', '2020-09-17 06:05:53'),
(218, '88e157d808d60e6a609d7617ae83aa36', 'CHONG KAI LEE', 'bmsp@wellpoint.com.my', '1544c1bb0ae82271c98b901e6844e01974b4ebfaa958de758d188593329d82b2', '200e03fe5acd608198baf46628670363196a482b', '0125508408', NULL, 1, '2020-09-17 06:06:35', '2020-09-17 06:06:35'),
(219, '592a79eb00e2c44e27de35236209f1d8', 'SIM YEAN NING', 'yeaning.sim@wellpoint.com.my', '5bd058d7b25766cdc6da54f850120e398938f6575e1a861a6b7a95669eb1733a', 'd6a8cb9428c6f8e9feceadde631faffd6c7cd729', '0164513553', NULL, 1, '2020-09-17 06:08:55', '2020-09-17 06:08:55'),
(220, '0ccca67dfc6aa98bf4a7d4783557d666', 'TAN HUI LIN', 'huilin.tan@wellpoint.com.my', 'cd6368f33d6657ad76710d1e82629f24d0c6561ee23c16484af22b319cb799cc', '90a9ddaf849676dc83dfa095e0350f0d317bc07f', '0174649343', NULL, 1, '2020-09-17 06:09:47', '2020-09-17 06:09:47'),
(221, '17b72a5c60f165b44e5cfeb3a71fa25a', 'WOON SHU WEN', 'shuwen.woon@wpco.com.my', 'de1f0d2a64f350c17af87f78625f06b16dc25c578f65b8fc54e3567502dd1984', '7876a7a1593c014b60b46dd9aeb701a4d8b0d831', '0164743286', NULL, 1, '2020-09-17 06:10:35', '2020-09-17 06:10:35'),
(222, '65ea8116c196e487d8912cee80cf105c', 'QUAH BEE CHUEN', 'beechuen.quah@wellpoint.com.my', '6dc7be20d69cfada9a203ca1251bca83e36992e788c69407b667f3d60ac237d3', '69861ab4cf7995c2b1c6a5c768a4cd82f7601c13', '0125596950', NULL, 1, '2020-09-17 06:16:01', '2020-09-17 06:16:01'),
(223, 'd3d3970e9695696dc6d28c61693a9ad4', 'Indiran A/L Balakrsnan', '', 'b26e754ccdaf0c7ab9e42edb4ab9b7abadc7ae94b96760a45c05982654647b0f', '9612e9958369f0dd83068c7af97957ad2bd3e79d', '0125225428', NULL, 1, '2020-09-17 06:23:37', '2020-09-17 06:23:37'),
(224, '2b157c0411dc0c18cd7215a3a71f49ca', 'MAS HAFIZA BT RAZALAN', 'mashafiza.razalan@wellpoint.com.my', '02869bcfc1ac8d5c461a141a0bcbf5063fa1785d2db2e01d10e1434e1790a88f', '0d6c5f23135722e8a93eea658a594de7273a9a06', '0125528197', NULL, 1, '2020-09-17 06:24:25', '2020-09-17 06:24:25'),
(225, 'b81950e6841e85d94ae2ceeb9ac989e7', 'TAN CHEE FONG', 'patricia.tcf10@gmail.com', '7003e7f1269e260b04ad97181250be52317fcaf2fd6bda505bbe1b6e9d5d9e39', '9adaf6844eed92dfa2fbb4adefb8cb6222fc8fb2', '0124529857', NULL, 1, '2020-09-17 06:24:34', '2020-09-17 06:24:34'),
(226, 'c2934b112767efcb59f652abbd5f513d', 'NORAIN BINTI ABU HASAN', 'norain@wpco.com.my', '8d848ab53a5426d3e7be081a49652ace790893bfbc3ec47594ae16574dc1148c', '13812bf722cfffcedd9724f778a3ed8c201462a6', '0194616641', NULL, 1, '2020-09-17 06:25:30', '2020-09-17 06:25:30'),
(227, 'd0e5f1d8c0d7fae6c22b71c243c2dd9b', 'NORFAHANA BINTI YUSOP', 'norfahana@wpco.com.my', '2ab1674926a74b3401914dc86e0aaa5f4111ac59953c39433ab415fd85c07a6e', 'd2a95054c9cf29eb58bf64783a054f0cb6855ec4', '0194083558', NULL, 1, '2020-09-17 06:29:56', '2020-09-17 06:29:56'),
(228, '0e1a5886d1b99e5e4cefbc8e445b2cb9', 'GAYATHIRI A/P ELENGGOVEN', 'gayathiri@wellpoint.com.my', 'ac0317437a0247189d38e3a50b0e97439acec0e284bddb0902835eaa0813c864', '6078825cb9e3b6e6ac78ffd9d11e675774c2f1ca', '0143420762', NULL, 1, '2020-09-17 06:31:59', '2020-09-17 06:31:59'),
(229, '9e466a008db8cdd1b43a91c5a26066b8', 'ANUSHA A/P RAJARAM NAIDU', 'anusha.rajaramnaidu@wellpoint.com.my', '52650b50d56f69db87cc4449a3335862ed1b5b8064997d61ae789633f6dba25e', 'e05d5e2dd207d1c098996b9c4d9ff50795995ac8', '0169335894', NULL, 1, '2020-09-17 06:33:09', '2020-09-17 06:33:09'),
(230, '909fae0954b60bdab380c120e5f90940', 'Ooi weng khuang', 'owkhiang@gmail.com', 'a4ca624a58d946159c447c6c225919904617f8fd554ccfd4b94376b9da320551', '0dfad1b0ecd970d96efab4aa564e4e56dd54c960', '0124716168', NULL, 1, '2020-09-17 08:02:19', '2020-09-17 08:02:19'),
(231, '099c11149ef484e5debc871e6f3ae392', 'EUNICE KHOR', 'cns9912@gmail.com', '06cf485e9b89d4bbd36aabe0455250b1c4feab0b0d0abc1482b10ea62092e062', '90d363f53ff189e2f76b8534efb78b357cf5ab47', '0164561233', NULL, 1, '2020-09-17 08:22:11', '2020-09-17 08:22:11'),
(232, 'f685c498551341277c4bec86084527de', 'SL Lianh', 'sllianh598@gmail.com', '5db3c43746799bfe290402ea99aed85d602c9df7fad0a1c0d2dad0c2a2ead4c2', '9f29431ccf22a4044e2c58d1934103c9d2399c9c', '0174764243', NULL, 1, '2020-09-17 08:25:06', '2020-09-17 08:25:06'),
(233, '87a1d2fc176ecd4cea460c2b7c6c046e', 'ANG SAY TEIK', 'sayteikang@gmail.com', '67c41029c4dd1b4d1e5e942c5ede3f539e7f9ec8d2c8dfb203237b6c89dca67c', '9c7f180cc9fbfe96ec0f77e4d48aca430235a9af', '0124825009', NULL, 1, '2020-09-17 08:55:17', '2020-09-17 08:55:17'),
(234, '3c0e4a6f2f99f504c31a0685bc3f7d6d', 'Tiw chee keong', 'edmundtiw@yahoo.com', 'd05a4b343dcf4e5043cd16a40325257b7f34087c262597469a952d1b89890d73', 'b39ce5d045f0dc2933ee8480b7d358824497b32c', '0189051439', NULL, 1, '2020-09-17 09:01:31', '2020-09-17 09:01:31'),
(235, '4e8c1b9c393a290bffaf565f45364654', 'WENDY LIM', 'siewcheng82@yahoo.com', '9a3b0c1bed3145012a547965f40e7173b6a6dcdc612b160284e6f116e437b41f', '61c3f11023aa5bb79c62df71077f36b4f0ccdf31', '0105394349', NULL, 1, '2020-09-17 09:02:47', '2020-09-17 09:02:47'),
(236, '62b02a858d23b8912ba78f6ef5054580', 'Lim siew cheng', 'wendylims@gmail.com', '380a28fb2ce8878ae0992b6b0c4b841472902ad12916f4180bba6cc69e328a83', 'c3bee3342981ed4a17f6ec15749c29acb0397085', '0164313124', NULL, 1, '2020-09-17 09:05:25', '2020-09-17 09:05:25'),
(237, '223eb69e983050f567d1197341667402', 'Edmund tiw', 'edmundtiw@gmail.com', '505081dd2536cd8d3f2bb8ba3d591bb71398b5a9f35ea8cf4cc8ceddcfd0756d', 'ca967542fc0f4a80268bff48388250d1789421f8', '01123890114', NULL, 1, '2020-09-17 09:24:56', '2020-09-17 09:24:56'),
(238, '2423365a510afae88ad00ceb757e7632', 'LOH NAM SENG', 'lohnamseng@gmail.com', '8e7d8c3f11932ec9a9fab5721ae7212a0edd4a9ac290fc1b0a28aae09c41bd37', '20656729232839cc6fa8dad29af28339782b5dda', '0124879888', NULL, 1, '2020-09-17 11:41:47', '2020-09-17 11:41:47'),
(239, '3adae8cf94d7eb85ea8674836be2a579', 'Ang Tze Jing', '', '145ef1586195b8e176244588d063e90f60a5bb4f7f3d3494093498fc4f60547b', 'e61d91f1362bea491b90504986cd0b90e10b8829', '61', NULL, 1, '2020-09-17 11:43:34', '2020-09-17 11:43:34'),
(240, '0a0a2b5436bcf2faa4a073687ecc6f58', 'Ang Tze jing', 'Zing_ang@yahoo.com', 'd5c34da16a5ea7a531bc3a0e6f014b9f17191b3c8be425dfbf5cc2b04ead8473', 'd13c27192bb6cb295a30870db60893c3d590c0bd', '0125589899', NULL, 1, '2020-09-17 11:44:15', '2020-09-17 11:44:15'),
(241, '0b5eb1dc3484cb1b3b4db5fddc91a8a1', 'vincent ooi', '1163719183@qq.com', '25e2717abed70ef6e6f9a215a8a898b11db26b6a6852ea38c1e6967d0244a824', '1ac32512426ace43845a9c647b385ea4ebf509f8', '01112859552', NULL, 1, '2020-09-17 12:16:49', '2020-09-17 12:16:49'),
(242, '1be18dba8f6ce3c7370e8999d6412719', 'CHIA KAH LENG', 'ckleng3873@gmail.com', '986f8238a8e6c9fb81fe4a49ee434348a8188cb968ddfa210346c5b9502fc3a6', '59ed55f710993c4b7a63b8bfb7e908ffebf6597b', '0174818133', NULL, 1, '2020-09-17 13:41:32', '2020-09-17 13:41:32'),
(243, 'dad157d2ec9ae2fb1d484c58792b69d6', 'LIM FONG CHOON', 'limfc2004@yahoo.com', '3128b61d65377781ea25051fe3e48b7a6a47a9fe10ca3a9f878379a3324f6f65', '15f583db1611d826dadb169cac8c2bb01cd4edae', '0124848907', NULL, 1, '2020-09-17 14:13:48', '2020-09-17 14:13:48'),
(244, 'f7fc95149fc7f8e8cf490ab2e3b3505d', 'GOH TUAN TEE', 'gohtuantee@gmail.com', 'a561e18cb3d8fb7fd889e99228d9cab718b6364a5ce9c4310dc290091d4e34a1', '517a0f20522179841b30422e69cdc23e4785f8a4', '0194791503', NULL, 1, '2020-09-17 14:37:28', '2020-09-17 14:37:28'),
(245, 'ee2939384ced33a994d967e3e1001885', 'Choo Yen Mei', 'ymchoo91@gmail.com', 'b914d273e2719f2d66e7b0fc7de3636c1844f3363a7924be5006f817f6ff1c2d', 'bae7e649f83ae0b4734975e91c586282e0a23598', '0124577741', NULL, 1, '2020-09-17 14:57:15', '2020-09-17 14:57:15'),
(246, 'b073d68ac613eaed22c3784c92ef19bb', 'Teh Yong Hong', 'tehsyonghong@hotmail.com', '40b26a3d5057c40d5f20b2b7995eb707dd75032559694c4ec906f6ccd9709a9b', '160492ce70802622392fdbac31f253a7eb347178', '0174170418', NULL, 1, '2020-09-17 16:47:37', '2020-09-17 16:47:37'),
(247, 'f4c4bd9283f20b78de0680309ade7a5a', 'Tan Gaik Hoon', 'tpc63@hotmail.com', 'c6083b278a9eaa66cbd0a6339eae8765640daeb7425583f714b9d83066d55846', '8e678f0c6de47e183715c8273fb04afdbd0900ba', '0124659315', NULL, 1, '2020-09-17 16:48:59', '2020-09-17 16:48:59'),
(248, '2de8bdac88233e5f2c6215f101ddbe3f', 'Chooi', 'Cheo6855@gmail.com', '128437389baefa6b3d1bb8cb590ca7641f0c7eada1bd3d6005a9132dff2d92e2', '7d1a44c882d9f5a9855454927d2816738ba4fd56', '0124088863', NULL, 1, '2020-09-17 21:57:38', '2020-09-17 21:57:38'),
(249, '6b299ea1b056d9754fbb14d3cd2d9611', 'Jackyteo', 'Jackyteo66@gmail.com', '74a625541e83cdffbb8d7a56247a79832f7c23635650ec69883424b76a42eb5c', '2b965160c2655acc94fb11a77753b40abe04f7ca', '60168281313', NULL, 1, '2020-09-17 22:10:29', '2020-09-17 22:10:29'),
(250, 'a1b78cc30d7e50b282d2087831f5b728', 'Alicia Toe', 'aliciatoe@gmail.com', 'c20cffa5ec20c6d53179f6f86f366189c67b47efcd38e8e4ca5f2b1fb6731f19', 'f874992aeb05f69c601a9cd9033057264308b10b', '0165558565', NULL, 1, '2020-09-17 23:52:17', '2020-09-17 23:52:17'),
(251, '0a5cccffd5a114a2a4cdac0e77ed6c65', 'Melinda', 'hooikeng.c@gmail.com', 'fb2fde4a5b07192cec29f525dccc37b959a4aeaca5a6c56e89f3504c397c0613', 'd7cb9ff626bd5ab2f4a3ddf0dca2b096ee0d50d5', '0124188013', NULL, 1, '2020-09-17 23:53:11', '2020-09-17 23:53:11'),
(252, 'b756809c0cb2e08beae1acadbf7805e7', 'Vanessa Ch&#039;ng', 'vanessacck@yahoo.com', '5475b953baa325c2531b26f18f6b050b271d531b3f28c77d5f8d15d9dcb29588', '38d4459432de189e84352c67a1c58f7ca5021cf8', '0195506699', NULL, 1, '2020-09-18 00:40:23', '2020-09-18 00:40:23'),
(253, '30b429b3047eff571aece1993e68d65b', 'Kenneth lim', 'Kenneth1037@gmail.com', '3460f45f38f6021fe8ee39bcba9451e4f87950a3790ab8c5f3d0b2e7a7f7726b', '80c42d02dde03d0b1c496d52ebabe211d03ab9b3', '0167110766', NULL, 1, '2020-09-18 01:06:46', '2020-09-18 01:06:46'),
(254, '94fa594aa21207fc7c7bbaff8bb6204f', 'Teoh Chiew Hai', 'chiewhai@gmail.com', '012613783d098ed41b8ba930444bd39f057dc61dae3d5d454048448908dd6ebb', 'daf0ae0d3f25502151ef3d04fc697ced73be1d54', '0194267502', NULL, 1, '2020-09-18 01:08:41', '2020-09-18 01:08:41'),
(255, 'b1aad25c8c79c2608b5b8a1e72c4eeb1', 'Chan Poh Geok', 'pgchan724@gmail.com', 'c5c7f995a03cbcbefc1463b77730449ffc5c9f360d4906b7a755042fa34d31c9', '44aca1adfd3c28bd3f90926caa3b1589973c5f08', '0125699868', NULL, 1, '2020-09-18 01:10:47', '2020-09-18 01:10:47'),
(256, '9a9bfb0b440a348c79763319c61fbdc6', 'WONG SHIAU FEN', 'vivianvarro@gmail.com', '546b95e1cd2820b76bd0d9006a22aa616cf127886c422b950d0bf4af0a7fbf49', 'c7fa3b4c83252471c550f35decedda0219e20191', '0124298526', NULL, 1, '2020-09-18 01:52:06', '2020-09-18 01:52:06'),
(257, '06ac953c92ec13756fe57ce669729b89', 'Wendy soon', 'chanjh9402@gmail.com', '5e05f1d9d5062ee91d3fe61c11f10258269af2e7db4f9dff684bb435dd0e22e9', 'ac2ecc2ad68aede3ba7c78ed5af56aab0e423655', '0194705593', NULL, 1, '2020-09-18 02:43:47', '2020-09-18 02:43:47'),
(258, 'e9a7af2d43a56803494b372e147b0e93', '6', '', '498092e13fb297c8cdbf55254b36bef5462cdcb544c4abd66bf68f197fee14c3', 'dc4620ad58b3686e645a65644b6ef09dd9e9f82d', '73473', NULL, 1, '2020-09-18 03:06:42', '2020-09-18 03:06:42'),
(259, 'c33e51dd9a1ab3355c9432e5b9191dd3', 'LEOW JOO HAI', 'jhleow@yahoo.com', '37cf61cfc718a65e066bd88789c50675ff7a055fb9340f0d4dba5777842e63b1', 'f8030d5907890ab41bebd0eeb0ccc47960aec62b', '0124882620', NULL, 1, '2020-09-18 03:21:53', '2020-09-18 03:21:53'),
(260, 'cfef41d53f41d4ba5f7680f420d2cfde', 'Michael', 'likkit1990@gmail.com', 'ea74396fa0d25d40a0f4bdeff02ec118974c605d069861ebf781086e972dae99', '6299296d609ff57bcb76c6179e065df7b1d65249', '174776832', NULL, 1, '2020-09-18 03:25:44', '2020-09-18 03:25:44'),
(261, '0aaf49f9752215d01eb175a7f8b96974', 'Natalie Tan', 'Natalietaneeyee@gmail.com', '4d19105a091d6aa71d163e3390f4a94b45dc8122a4c213137e684c0e9479c75a', 'fe50e51419011e337e6c08491826e99edaccc001', '01110770678', NULL, 1, '2020-09-18 03:31:47', '2020-09-18 03:31:47'),
(262, 'e78244a44d69b85b4477b1d135346e1a', 'Valerie Tan', 'Valeriethy@gmail.com', 'a450a74bcd62c1d6acbbc0747d6add777796cea73c8708a7d91b360c2db9ea4a', '8a4bd42be796e21c85a606d0e80de6c3e6b7c89d', '0164821822', NULL, 1, '2020-09-18 03:32:43', '2020-09-18 03:32:43'),
(263, 'fea470bcb272642b30132ae2dfc76736', 'Vanessa Tan', 'Vanessathf927@gmail.com', 'd81af98c95b8b0f38552561b3f476ef552cad8dcc11ae527f249bb02792c4317', '0ccf718c3759ad336fb17c28ea0eb13e3903b7c4', '0199880927', NULL, 1, '2020-09-18 03:33:54', '2020-09-18 03:33:54'),
(264, '5e1c552c9848b84adf9ae64529bbd1f2', 'Felicia Tan', 'Felicia_tanhz@hotmail.com', '24a954d431c28091f9b188c152dad59d3912b325b2c556196050ae12bf19cdd0', 'b0cd08a7026c33b45944367a66510fc540c7552c', '0124069914', NULL, 1, '2020-09-18 03:34:38', '2020-09-18 03:34:38'),
(265, '4f02b6464e0ac4db3a2f2ab5e9ab5959', 'Tan', 'Tankokkuang1962@gmail.com', '619566e1dc83cf0f77d229f1f02d352df34365bcb3a146c81a291e45760bb4bb', 'f3f6af961da4629fd86dee8ee3257f80d99177d6', '0194131010', NULL, 1, '2020-09-18 03:35:28', '2020-09-18 03:35:28'),
(266, '9257a61676c6c436810a5848f1b84446', 'BLay Teh', 'Tehbl2266@gmail.com', '7b1b6088287949f92843617ff6dea33e110d20434bfd0269d50c5e20b7204b57', '3935c6bb69430a16346bdea684e81491f7cc80d1', '0164851006', NULL, 1, '2020-09-18 03:36:14', '2020-09-18 03:36:14'),
(267, '65d14cd30f9958ae6d9fe5721c41a444', 'Ho', 'bhho88@yahoo.com', '03da2fd9d4b2d55c9a10c0f25e31da4abfbe24172cd5e7e65eac7fb53dca7cea', '83735193c58faf2c264be50f09a00b3fb410485a', '0125551335', NULL, 1, '2020-09-18 03:45:01', '2020-09-18 03:45:01'),
(268, 'd24e72b552e4f72118b696256f047dd6', 'YEN', 'yentyt_1002@hotmail.com', '18d5a8b4e73fca328991324b5265293d5dd92488e5a4116e0fac9e28be831298', 'b429323e965cdacb992943f38e4c54dce79bbfb1', '0198148687', NULL, 1, '2020-09-18 03:45:35', '2020-09-18 03:45:35'),
(269, '1faf7839f84707c329ac8975d98fa8ec', 'Yeoh Kok Beng', 'wilsonsonic@yahoo.com', '4ceff2f49883de53e996c994344a7f28f4f2cbc20c10329af74d16b82b73f700', '70730fb429b501be47627b9ba9083e7e852c4ff7', '0164581981', NULL, 1, '2020-09-18 03:50:51', '2020-09-18 03:50:51'),
(270, 'f1ecba8193da96ccaf84a53b60f0f0b9', 'Huong', 'huongmeeling86@hotmail.com', 'acf08be70e495bb78479d4706e4bf78c83b11d007825071bd646aee195777d5a', 'c10be465a10e97262d76a3beaedf1e46c7c9790a', '01113146513', NULL, 1, '2020-09-18 04:13:37', '2020-09-18 04:13:37'),
(271, '3a25dc14b74a51aa081002047a379b88', 'ginnyliew', 'ginnyliew000@hotmail.com', '5af2177593b84874b13920d07ce92cc9315b1f5c6923065b2414ade144526c31', 'ff613d2c2e8e02f245b4f811b6adb1814ed8c233', '0124825433', NULL, 1, '2020-09-18 05:25:48', '2020-09-18 05:25:48'),
(272, '4e9a19727ebef06dbbbf27732d8776be', 'Ann', 'fiera25@email.com', '7cc1689730440fab189755ec589179117ee7f59cc301364707838d9784dcfb59', 'bfabef3225f21a0e961e14f8c6d398cb9029f9e3', '0102100675', NULL, 1, '2020-09-18 05:39:46', '2020-09-18 05:39:46'),
(273, '148c6d46a9ad2e4666769eb36d2e203d', 'OOI SIEW LAY', 'slooi2903@gmail.com', '2d2e38a44cfc6b1970aa60aa2bca521dbc462db293f95183519db29eb7a2ffba', '46f6b489bc95061eb9824327beb5d8f89b985dba', '0164740669', NULL, 1, '2020-09-18 05:42:02', '2020-09-18 05:42:02'),
(274, '80e66f0d9e9330b26355acb2441d3107', 'HSLok', 'eileenlokhs@gmail.com', '177bfd1f544de6e15f5fe61564ec1b1404f721e04c007c634d4bdb8ede3a9609', 'e77a233d8fe093f88f323d9bf33ec0ce157217a0', '0105630190', NULL, 1, '2020-09-18 06:54:11', '2020-09-18 06:54:11'),
(275, 'aed7e2dcb740d61aa9b7afe0076171fc', 'LIM SIEW KEE', 'siewkee168@gmail.com', 'ec1db2ec77b6d87da0d33d2b68cdb2c4963de6f37e06846b1a4fb63326cfa0c7', '6a5da906929c1f95ead1f6745b31f84072d5cb35', '0125888281', NULL, 1, '2020-09-18 07:32:02', '2020-09-18 07:32:02'),
(276, '090b825eaa4b352bb44a4c49d05b1406', 'YEOH SHIN HUI', 'shinhuiyeoh@hotmail.co.uk', '4afee751537bc99e1d171fb30832fbc4aa2776395025ad55396c13b3e607489a', 'e9b04590cfe1176c75edcbc2b7c3930b4823bf54', '0175984328', NULL, 1, '2020-09-18 07:55:52', '2020-09-18 07:55:52'),
(277, '89912fac064bcaf90da09d568260d73d', 'Siah Tick Tee', 'Siahticktee@gmail.com', 'f45b89fcbb22905c73f13a368ce0030d471c35b63a8ecf082faf90c65f2e46b3', '65b055339267a171a406123a6677b2c0cbbb591c', '0189824241', NULL, 1, '2020-09-18 08:14:09', '2020-09-18 08:14:09'),
(278, '0cfb722f54737c501f07f72f524f8ea7', 'Tracy Ang', 'tracy8921@gmail.com', 'b3433ea597067253329495ea62675e077a9b3f6bc9b5abe80c772b0b71035469', '61563fde35b198e7530891647bc49a95ed0de2d6', '0104648921', NULL, 1, '2020-09-18 08:32:50', '2020-09-18 08:32:50'),
(279, 'ce5520e4def1c606cbce20795bf28fc9', 'Irene', 'cheesl2002@gmail.com', 'a3a384df4860bd90da4f63c74d2701744d074b6d885de083e164d2c05ed785b2', 'c35d9ad7b1d33d4ba21b1ec1a207cf53120cabad', '0125379180', NULL, 1, '2020-09-18 08:36:18', '2020-09-18 08:36:18'),
(280, '83cd825d983551db7083e7dcc0ba22fe', 'Kuang pit chye', 'pckuang1980@gmail.com', '6d61eca1548eaed13077f6179c781c5d0d08218225350ad0923920ad83f997c6', '4577c2fef2fdc082a0677d2656a412bf95d27fb5', '0164401893', NULL, 1, '2020-09-18 08:51:26', '2020-09-18 08:51:26'),
(281, 'ee78edf8cbb8ca2e070400312b676e82', 'ONG CHIN SOON', '88soon88@gmail.com', '82de992d803882e75f21ec958ddc8d54de53b617ccd77c648bf980609cdadf89', '9cb1d119e82efc9ba1f75af2863e52c4c6f63e71', '0164161062', NULL, 1, '2020-09-18 09:19:55', '2020-09-18 09:19:55'),
(282, 'e60ba8c0e7e9cbb52282a51889c6f2a7', '陈冠羽', 'thbectwjwrgynat@gmail.com', '58fdbdf10757041e53d60a1c72882c28b9f2c94c77e33cfcb92cfa007d5653f4', 'd3e4bf3d291911ce3cee40eee0ff828648d0b12e', '0126439470', NULL, 1, '2020-09-18 09:52:16', '2020-09-18 09:52:16'),
(283, '7edfe54d76e35f8d43f173de0e02c299', 'Teng Kim Lai', 'tengkimlai@gmail.com', '22ff8da250188c188277b26779909384a072f7718db883e9367d35f5e7e3fb49', '42b725cd9318cbff568c2bdf7a32c5549e54b9e1', '0124021226', NULL, 1, '2020-09-18 11:31:12', '2020-09-18 11:31:12'),
(284, '319407b3b48fe85923aab807825c1ee7', 'Loh', 'kgloh_23@yahoo.co.uk', '5934a64ab09fa9a2b38b7554e19d6d38fe3a06494e3726ead7fcc993bb2c8819', '4a1f5fcdf52ede666d155cdcf9010b9a71dd0188', '0139108132', NULL, 1, '2020-09-18 12:12:33', '2020-09-18 12:12:33'),
(285, 'ac2d2f2a5360a7060538997d5ed42bb3', 'P.H.Lim', 'shlimyan@yahoo.com', '5d77059365ca7a6f7fc1e9c9f92d12eadb8e3bb75014f7ce1288cde0d600c912', '36a3b9f59816ec180fd443ccb3f7c0d601e1d056', '60182436788', NULL, 1, '2020-09-18 12:54:38', '2020-09-18 12:54:38'),
(286, '9fadfffcc0632be050dfa90b240dfa9f', 'Khong', 'khongwy@yahoo.com', '52c8506d7dde6198cbd0947c313e4950eee028bc5431dbb6cc0bbfc1d31b8bae', 'e877640cb741b084839660998d14507422478211', '0122307577', NULL, 1, '2020-09-18 14:02:28', '2020-09-18 14:02:28'),
(287, '6fcd189ed9cfc5c0a52b64a67070abcf', 'Leonard', 'leonard_tan83@yahoo.com', '9d5b336c16d11fa46774f7aa800add954bcfb4d2c237839c6b8e119564478f78', '444b2eae10f9f899a9f112a0c5ab57198f9408f2', '0124918691', NULL, 1, '2020-09-18 14:43:58', '2020-09-18 14:43:58'),
(288, 'db291a13bbe2443d0d5b74a531876d73', 'Leow Cheah Wei', 'cheahwei.leow@gmail.com', '6eefa9148307f27265546522d8e8da20f79295ad54653446b2859df4f4f90b0a', '9aeba72c9be80abf6774f958fededfbf0abbd046', '0127239729', NULL, 1, '2020-09-18 15:00:17', '2020-09-18 15:00:17'),
(289, '7e2b7b069781f8fea896467db5da9c85', 'Chooi Wenc', 'chooiwen26@gmail.com', '54d8c97704af22bc952d10d761a08c4ab14387ad60df2275ad94408b081629cb', '7fe505087b758c2c52dd0cfc1ec48d3b0cb2765a', '0134314932', NULL, 1, '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(290, '479ee361b2fcb512c4a3ac87abb6963b', 'Tan Chee Seng', 'cstan87@hotmail.com', '039948717538659cd6039eaceb8a6cc49267f2f09cff0be8ac299f50b370e245', '334f2b9f3921de670416e11d05844df95bafd9a0', '0124521486', NULL, 1, '2020-09-18 15:11:30', '2020-09-18 15:11:30'),
(291, 'd32ce43edfc1fab2c833bdffefb1e5f9', 'Chew Chee Teong', 'chewct72@gmail.com', '700279b8c740050afe513f0db55766fc50989c494cdd4003c4874e686b74c8fe', '85e91d45aa0d3272c663421dea569b3f9bf9128b', '0125179293', NULL, 1, '2020-09-18 15:13:46', '2020-09-18 15:13:46'),
(292, 'f3d8c01ee5f186ac96dbbe517dd10d9c', 'Ng Tong Heak', 'ngtongheak@gmail.com', '3df5aba3ce98aa56aada1203029732cb8663a84eaa541f810017f2f5d541fc1c', '518669cc89a54c30acf164cfcba3ca2762b6f7d0', '0125883855', NULL, 1, '2020-09-18 15:49:20', '2020-09-18 15:49:20'),
(293, '7e7235ee639d4830d978977e1d0c8829', 'SHIRLEY LIM', 'stan1886@yahoo.com', '962b08dd8538db44c9a561637248cb178bcb1919bfe7996f604f3b05a46b753c', 'db68bef2be3e91e463b1454bf3be47f041b6c79c', '0124865535', NULL, 1, '2020-09-18 16:13:02', '2020-09-18 16:13:02'),
(294, 'b893a39e5aae0a0b5dfc37c1c0423700', 'cmahui', 'zhou6308@gmail.com', 'ae49d0e8e1de103dc53b599942d3fd9fa8ac9ceb02a665f63fa5d14a28762fc9', 'd05e9cf729707f85a381b47383734a53005bc7dc', '0195220220', NULL, 1, '2020-09-18 17:06:31', '2020-09-18 17:06:31'),
(295, 'd86b4b36a7e6b04a26efa3e3adf90d7b', 'SIEW CHIN FLY', 'flysiew118@gmail.com', 'de9eaf98e964b1942a4fe49bea12bb8c27aef62a54b386af4ce6661e6a93df4a', 'b1f092678366dca1a1392702336dda547666b050', '0124896571', NULL, 1, '2020-09-18 23:04:32', '2020-09-18 23:04:32'),
(296, '14fa6c4d0d1808fe8da314c69f11a147', 'ong pook hun', 'ongpookhun@yahoo.com', '4852bfa80b8e2784134f9ccf7cc9a7d663880e629b0529bad3bd530223824b6c', '9097d93081d591f140d00a8637e97845d9c863c7', '0124561022', NULL, 1, '2020-09-18 23:09:08', '2020-09-18 23:09:08'),
(297, 'f715d43e8f78654a99f628a051c42c46', 'Ms Ng', 'ncc6639@gmail.com', '52255ce5229b74969beeeeb57dc0fff2def692dc28a555be58ca2fcf843a0333', '4d3248f5ff41138b21ee22981115c7f3277e9d15', '0124181871', NULL, 1, '2020-09-18 23:54:43', '2020-09-18 23:54:43'),
(298, 'f7ec5a9e9490c7b2bf0e0500049b4334', 'Brianna Ooi', 'boonpei_ooi@hotmail.com', '2cf19b63823d0819e8e267e48250a99b759b24f32b97fb77a45f223f87411eef', '6f1af655168f5c5c7f3e58340973fc85a42e9e0f', '0164191774', NULL, 1, '2020-09-19 00:02:23', '2020-09-19 00:02:23'),
(299, '086bf9701d2c34fac5bdc6042f4ed3da', 'Leslim', 'leslimhg@gmail.com', '112e49c5fcb2e8e84f7270cb2196298558928c5f53adde3745ef0e2d591f433c', '1e1fbf78748707b1853654e34400481e9b97ce88', '0124855045', NULL, 1, '2020-09-19 00:19:08', '2020-09-19 00:19:08'),
(300, '01843a573f2c59662e1ef25c224a5308', 'Mooi Tuck Hoy', 'admin@mth.com.my', 'c0ae830ceb198743b8ddb5b80f02e8fc3f9a5cfa1fa6d5b2279cdf7173dc1209', 'cc954b44c41c68d9ebe14e61ac4ef501dc7ddab9', '0174628888', NULL, 1, '2020-09-19 01:06:13', '2020-09-19 01:06:13'),
(301, 'efffdf0e6b0dd71b41fd816bbb891e4f', 'ronny yong', 'yongronny@gmail.com', '699160258dcd99b2856e586bb033ca67c47664de367a86a775f06ae27d7dc5fa', '4f76eea7a1498dd272c60006c73cac1f784455e4', '0124726830', NULL, 1, '2020-09-19 02:30:52', '2020-09-19 02:30:52'),
(302, '846402448dd91035e3e56e03c77234ec', 'loh  Poee Hong', 'lohbh64@gmail.com', 'b9c17ce7d17fa620bee6243407f1f647b6a6f5046855fef4a3b0ef33a93a158b', 'b6186f5c16acc300825ad18b72ca2e53d890a71c', '0194736806', NULL, 1, '2020-09-19 03:06:04', '2020-09-19 03:06:04'),
(303, '52f7105b886629c4cecc53470d441465', 'Kenneth Low', 'kenneth_2001@hotmail.com', '6d6c70640733fcefa7601c72bc087365b1d19153891b3359de802ded27d4eb6b', '20d6dc71f5cbefbecb3655a5f8c4eb634e2eaf30', '01112371269', NULL, 1, '2020-09-19 03:09:45', '2020-09-19 03:09:45'),
(304, '6f2c85753d89cd6cbd9fb91ab81b7fd9', 'Queh Chin Teh', 'tehquehchin@gmail.com', '58b4c8927090ce80ccb3f11ea2707601b3d7840ee5a81073b8fa0765992934b1', 'ff7a337fc5478247378551a166048d883e6b869c', '0124865299', NULL, 1, '2020-09-19 03:46:15', '2020-09-19 03:46:15'),
(305, 'ca8225ab68b7b54ac14484ac1181962f', 'Sim Kong Beng', 'kbgsim@outlook.com', '9430e883780160d874d749c7fd7a297261e972e2351ae44087f622bc01fc9cda', '6119efa960758ec0b3e788050ea40fa8892b4f17', '0194107325', NULL, 1, '2020-09-19 04:39:25', '2020-09-19 04:39:25'),
(306, 'a22ddd0a2ef521656a24d372ef6e64e6', 'Hui Jin', 'annatang0210@gmail.com', '1e99fe8d865659f6fbe88b8c09ad7409ed7c7406038f1fd5a18c04e9695be974', '0663879e1555852ac825ca69132a55e1e019d0fa', '01113089567', NULL, 1, '2020-09-19 05:20:04', '2020-09-19 05:20:04'),
(307, '7d6f54fa1a2561a79a5bde63c7df7f81', 'Ooi Hock Kooi', 'ohk6599@gmail.com', 'e4796a6eaddd0cfa57925938ad86cee966d3901787214ef1b1ea6b3afc820187', '5fe9b7eb697be4ed008201106a264470e9292364', '0125782793', NULL, 1, '2020-09-19 06:24:19', '2020-09-19 06:24:19'),
(308, '76e9f3b2d39d043424bfcec758ed83c1', 'Tan Kee Hooi', 'keehooi@gmail.com', '131ad509464f67e491c65e0501e22dd0de339834bad9894706a5ad56f07b4fc1', 'a67f4e0b28554c37dcb794ddb8aa694c365f2a78', '0164883398', NULL, 1, '2020-09-19 06:38:09', '2020-09-19 06:38:09'),
(309, 'fc113fc4fc2b64fec7ff9d44395d0410', 'Shirley Ang', 'softwarestorage666@gmail.com', '7355ffa3089c8cb742b48cc3a19ce14c2b77b62c5684153213f8a90477e52d7e', '310c09d4002b5ff46d5a836718f55434866a860d', '0164287099', NULL, 1, '2020-09-19 07:25:22', '2020-09-19 07:25:22'),
(310, '3377f928d3addce9d8d80679d757a7fb', 'ong kwang yee', 'kyong13@hotmail.com', '66a294841fddff05248718017c1f2eb9f795fde5bc8aaf6257ad2c12c133a498', 'dda7d9dc30b7cf21df753d66b8f16989f9d034c5', '0164555846', NULL, 1, '2020-09-19 08:18:48', '2020-09-19 08:18:48'),
(311, 'ed802736e768513b04b9a82ae75cbf9e', 'Daniel Wong', 'dscwong76@hotmail.com', 'fba699b1194256f591528686702471c255b6eb06499620fb8696abf701b32da0', '5aa1413882b8bd0f5cc85c94dce5e30de346c4ca', '0125566025', NULL, 1, '2020-09-19 08:22:12', '2020-09-19 08:22:12'),
(312, 'd042106c09fa822a57b2c7bc72d4266f', 'Tham Chiew Suan', 'cstham25@gmail.com', '23d7ae2fa7ab1034e65deb3e53c5f7cf79616ffc86b1aefcba2767003a5fe60d', 'fa0cc08787b975632f2455c34ba7ad9935303671', '0164513373', NULL, 1, '2020-09-19 08:31:01', '2020-09-19 08:31:01'),
(313, 'c7ba8298f6e6d04929b8ef0d47924f11', 'Tan Chew Aun', 'chewaun_tan@yahoo.com', '737e0f0acadb86db9bea7da52bdf47658a0e4c1a8cbd5f5d31a6337ab32f2840', '4189a1aa8896b7e51ad2629eeb11e5059fd0fc5a', '0174139583', NULL, 1, '2020-09-19 08:39:06', '2020-09-19 08:39:06'),
(314, '3d5a516e697ba3e99d3a7cd1e7c72e89', 'Augustine tiong', 'augus2777@hotmail.com', '3fc7e5ef0f1cb37745f9fd505fbf5e44ca5b3c2ac05d26a14a389b40692e0519', 'c5c8f36ad46ceee8ca71d8c620e0b912b039abb4', '0167002500', NULL, 1, '2020-09-19 08:58:48', '2020-09-19 08:58:48'),
(315, '86cd051c4d62d7f4c4f8f9a8f5187aac', 'Seow Pheng Chew', 'smallapple1807@gmail.com', 'acbf818244c9f66663d127709a1ad86c79b2878a2c96a143dad8d651151c392b', 'e9cdd652f3459e8c3d83246ecf4623520af06e8c', '0124881807', NULL, 1, '2020-09-19 08:59:10', '2020-09-19 08:59:10'),
(316, '3e5e6161b8245f19490555ad017d1b38', 'Koay lay tin', 'wwkoay@hotmail.com', '8be87b6773396c38f06421830898a8e5d75f57a94007d889d5c47f906f932a2f', 'ec11c9c0b8b161dff809e1e2199f95b8b53df818', '0164810889', NULL, 1, '2020-09-19 09:18:42', '2020-09-19 09:18:42'),
(317, '52f2bb95deb7014754f1561c719e157f', 'Tan chin waye', 'kennytancw22@gmail.com', '74aea7deba3f36271ee21433f55ffeac38aec78880fd7723c818797fce1a0275', '9ee3eb7af7918913664bfdec31a9014cb93ab3ca', '0164111727', NULL, 1, '2020-09-19 09:30:49', '2020-09-19 09:30:49'),
(318, '5762b48be0c7679f69cde8ad072cf8a5', 'Tan Alice', 'virgo23_08@hotmail.com', 'cc565718434f1e744138ecb28dfda363fc7eb8c7bb4d5fa3121940604a33b635', '45f1e782e24b7daef19580662cba86f345c175f0', '0139020152', NULL, 1, '2020-09-19 09:48:41', '2020-09-19 09:48:41'),
(319, 'c183a32525717be4a7f3bad6064659d0', 'darius', 'darius.tkh@gmail.com', '063565cf7213f88c2a19dc7a5ee23ff956627bf9354ed5aa6f8840a203f0c635', 'f82cb419b68ed64e7c4a6e152f6d53bba517cc0c', '0167216603', NULL, 1, '2020-09-19 12:17:56', '2020-09-19 12:17:56'),
(320, 'a994d7e1236b73974bee9346f8399ddd', 'Hneah Chin Thong', 'cthneah@yahoo.com', 'a4874ef553006e0851dec8ff946e1fc1e2978672306491f226c72f2bfa19f2e1', '4cfd161887f9c7ea52cf04f965789fd16c0f076b', '0124213318', NULL, 1, '2020-09-19 12:32:48', '2020-09-19 12:32:48'),
(321, '7fd05229d64c20a1daf162ce3a560ad2', '王明发', 'bob0124600828@yahoo.com', 'f2ef5581ccdcda9f61b7430d7f0c2a242d5b3434429bbc1acad95e5f61c36bcf', '93a63aed87b741193494a7e3b16d72f4b4cad804', '0124600828', NULL, 1, '2020-09-19 12:54:25', '2020-09-19 12:54:25'),
(322, '439a812c3b9cdbfc7ef3f50bef963be1', 'KOAY BING SEN', 'Vincentkoay33@gmail.com', 'a14e72d1ee8a37114ab2903df86020431e4aace140edfc1b88ef786955aa9d30', '6896e58fcdf20c0b5184f87f9036ba79126fb04f', '0182727255', NULL, 1, '2020-09-19 12:56:55', '2020-09-19 12:56:55'),
(323, '04888dbbde05e8f5ee23634add983151', 'Chiew May', 'chiewmey@gmail.com', 'd876d5055077dcd88019416410b8ed8d6a57c9a8faa7ac48755bf191acbc90ad', '16c8f388b88005a0e7f7e8abf19e82b72206a21b', '0164263634', NULL, 1, '2020-09-19 13:51:12', '2020-09-19 13:51:12'),
(324, 'c69367efb79675ad91c1ba81aebcb976', 'Hong Jay', 'Jack03082001@gmail.com', '25ac4309c70c7d1f2b779e588314c2379c4976fffb4ceabd6142005ddf095a23', 'da45c93bf4ebed23503b65ca63234bb6f6700fa4', '01136893938', NULL, 1, '2020-09-19 14:30:33', '2020-09-19 14:30:33'),
(325, 'c59358d010ccd2208fcb84385ece1d87', 'Loh Hong Kit', '', '40d2fe9db359baaee1edbfe58c162653006e15a2b8db03c10035ac430b1ec7c1', '00aa33ae979ac8f91b7967edc72d73180d44fca2', '01115', NULL, 1, '2020-09-19 14:34:24', '2020-09-19 14:34:24'),
(326, '07c2a66daa3af02b5207b079781a0147', 'Loh Hong Kit', 'Raymond981117@gmail.com', '9e9ae5ae6a538c00179ee196aa7ff98fbff102d91eef27c387fc765262f32dc7', 'ab2abf0ae3afc40a855fdb66a5f9c57f1d4c5efc', '01115065129', NULL, 1, '2020-09-19 14:35:07', '2020-09-19 14:35:07'),
(327, 'd8382a773d613212536cc7319bc27c31', 'Jackson Khoo', 'kevkte@gmail.com', '299d89d6d32f43b2ad59b31cae251ef13680777ac840316953fd6fadd5bbff63', '7de35a4c1cde95bad9f77be8dfbabde87d7b4468', '0135818333', NULL, 1, '2020-09-19 14:44:09', '2020-09-19 14:44:09'),
(328, '58a06b94e595d1e2798aee73103bda32', 'Richard Chng', 'chngrichard00@gmail.com', '34ba9e118ca58d014a4217bf428f02b8a3334288511ad04639b6fa8e814a903c', 'd0cef24ecb2f9544648b97381ab1171545132969', '0125082358', NULL, 1, '2020-09-19 14:47:00', '2020-09-19 14:47:00'),
(329, '4af50b8820aa17ff3073b65b0d83c5db', 'Santiyaa kala', 'Kala2320.tk@gmail.com', '4ef8052876122fe8b53f600c518b560bed1c4e76f0364bf5618801cbcbf6263c', 'bcdecd46b02afe0010add7f816016cea80aa2248', '01136177170', NULL, 1, '2020-09-19 14:47:57', '2020-09-19 14:47:57'),
(330, 'a14cce51eb69eae2de8463d0e7343acb', 'Pierre chuah', 'Pierrechuah69@gmail.com', '482e4b4621cfcd15547411714fbe9cd98589db16c25eec70e80589ca589607da', 'b0c2258997af406f7fc11b8da205beb8af081f28', '0134888277', NULL, 1, '2020-09-19 14:50:21', '2020-09-19 14:50:21'),
(331, 'e45dd3cc882367e7447ec85576d2182f', 'Brandon Koh', 'brandonkhc@gmail.com', 'ee5abcbeba31e5d2786e1b78616e84c7780cfe72bab3e6ff7a20a3e3a8accc7e', 'b4acec0ec2df5511411e4b2c3d7d361d3f993b19', '0164801805', NULL, 1, '2020-09-19 15:04:05', '2020-09-19 15:04:05'),
(332, '98a4b79f6f1128a183b60e836688e64d', 'Loh Hong Weng', 'Michael-1897@hotmail.com', '38ea7adcacc7937e8d91f9da5988c8ffeec0bcbd4c250df509ceae81ce79bd5f', '13dbd99379c593bcb40b0eb56f88bf2948bfd81c', '01135171897', NULL, 1, '2020-09-19 15:05:31', '2020-09-19 15:05:31'),
(333, 'd1699fdbcc0ffe9d3b9cbe92081df384', 'Hibari Chin', 'Hibarichin47@gmail.com', '5064fb4c47d8f2785d8216198f8589219faf5be0200cc258c827381bc12cac00', '3c0ee6957120f740e8435a4a65b854b844e113d2', '01116159122', NULL, 1, '2020-09-19 15:05:55', '2020-09-19 15:05:55'),
(334, '64e0e4a28c3a63222a8f701bf40401ce', 'SOON BEE HONG', 'ds_soon@hitmail.com', '4fd48590a56da479644bf41b9742df69007fcb3096c36297c30477ef0c7e172e', '984dd615eebfea62cdbf4082632ec8b14bb281cd', '0164088887', NULL, 1, '2020-09-19 15:34:25', '2020-09-19 15:34:25'),
(335, '95c269d3255ca9cca4a2368773ffa0ad', 'KYOKO LOW', 'Kyokosanjo88@hotmail.com', '8c37bcb933e45aef33f9e9f04e5d9a65fd88d34ead81786859b2fa1c8fbcaea4', '40743cc92632bf764bb7b367d368cd47930af20d', '0165080918', NULL, 1, '2020-09-19 15:36:57', '2020-09-19 15:36:57'),
(336, '26b1286d2d1dc242c6c2efc8feee50a5', 'Ewe Kean Soon', 'dennisson2003@yahoo.com', 'ff7c41daaca674d66818485cfbe8ae879548956c110a72b2e66f07704a4ee4df', 'a4bd746822c05f751bcdc5a834e66ccf4a57ddc8', '0125305847', NULL, 1, '2020-09-19 15:39:08', '2020-09-19 15:39:08'),
(337, '1eb73bb9c179826b41d1046eb61dc1e5', 'mr ong', 'melvynocc@yahoo.com', 'be4142203c60db4eacc7fd5311765250e1e88b3cfed3cb42888b0338e47e2e2b', 'bf1bccdd21a1b45de39e0d7a514774b5c0debf5d', '0124939459', NULL, 1, '2020-09-19 15:43:21', '2020-09-19 15:43:21'),
(338, '002d12e514733982fa045218b962c829', 'Calmin Lim Khai Bin', 'calmin@live.com', '8ce287a892ae4effe241dfb8c777fcbe923a023631e50c2ba54235a29c989b68', 'e38883184a7ec7bf4a2cef8d82f2a079c3c0c467', '0124050558', NULL, 1, '2020-09-19 16:17:31', '2020-09-19 16:17:31'),
(339, 'a11707fb01654da7d0bb838ccbb29043', 'Iris Lim', 'ilzh@hotmail.com', '835dc55fdbc83c521d4c9d636a68fe215faa864e80931c5aa1a13df6fd393743', 'd818ffd45c4e2d73e446fedff91c7353f4496af2', '0124223747', NULL, 1, '2020-09-19 16:28:33', '2020-09-19 16:28:33'),
(340, '36b8ca62a0d801993ef4488d82dcbe31', 'Genevie teoh', 'teohhuijuan@hotmail.com', '282d9b44e80d4ca49ba592e798db4b4e7e2073e4c8f1291e1253b929ab2bf397', 'a0d3cd5632f01d21d85c3a9db35afb32c1b52378', '0135297728', NULL, 1, '2020-09-19 16:30:28', '2020-09-19 16:30:28'),
(341, '5cbbd0290bc7235ba08040598d8cfc9f', 'Weni Chua', 'wenicwn@hotmail.com', '79f2280a6dcc70fca5f08d2591f781ea9fa4694a593c6d32c4b9937eb0aaf07c', '2768b1d4814812a1871ff629fbf81306b49ada5f', '0164421775', NULL, 1, '2020-09-19 16:31:06', '2020-09-19 16:31:06'),
(342, 'fa615b397a834cb078a9d762f91a90f6', 'Sarah Tee', 'teeyinchuan@gmail.com', '6b7e88f414b859b39f7d4af1c8265c70f32294ed7972b03042c5bd1833b79193', '9b6d3450c1bee86c1a68ee77aa62f168941cd3d6', '0124882538', NULL, 1, '2020-09-19 18:08:59', '2020-09-19 18:08:59'),
(343, '23ad71849b6c75c8876735116df5b0e3', 'Chong Kwang Yew', 'c.kwangyew@gmail.com', 'ad4b2cbbf44805b8b530a09827e0a71dfc8b613e93f155c2f0cde03c39c886a1', '46db6dab539b76ef9f9b8d5e7f93c0467b30b3e4', '0164764801', NULL, 1, '2020-09-19 23:12:11', '2020-09-19 23:12:11'),
(344, '069b562acf4851bee52cc51aaffec9b3', 'Luah Ah Chang', 'luahac@gmail.com', '90285edac3565273cbde49a4e079e99db2e053437a4a458048b4e79297fa729b', '0e659770712e06fe105292733ea1cc57eeb7ce48', '01128226866', NULL, 1, '2020-09-20 01:36:21', '2020-09-20 01:36:21'),
(345, '985a71863d01a2a844df196aba7d6948', 'Stephy Tan', 'tanstephy_0730@hotmail.com', '48abf07497f61d93cadf83094c89f285c2abe85dd3eb48c538be7d8abaa05c44', '8d56e638fdbe3eefa5d548c9880327392cb7d306', '0164764599', NULL, 1, '2020-09-20 02:21:26', '2020-09-20 02:21:26'),
(346, '919c203aa55adec95c69aed4d4e0d40d', 'Alwin', 'alwinlim@zeon.com.my', 'e833dfb741af6d75e1214129a4af59a4de9a70cb33b5482da3f2fc5e93c73a50', 'f8af8f74ba2aeee2e5b2fb6f58c6d26de556bd23', '0134885988', NULL, 1, '2020-09-20 02:24:13', '2020-09-20 02:24:13'),
(347, 'c814540789ceb36af1a3163647fc6b4d', 'Dan', 'ols81@yahoo.com', 'f897c9d7591179052bc2db02662251dfe1601dca2ea2ff71ad340b27af373ae0', '4fc7686b398cf4288e80ee975f8a6b6f8df01a28', '0124300494', NULL, 1, '2020-09-20 02:44:47', '2020-09-20 02:44:47'),
(348, 'd00ccfc67f9a21729eae2e84651e44d7', 'Kathryn Lee', 'Kathrynlee@zeon.com.my', '646feb5b2b0453bed1693813c95d0e87edf65dc28367d1aa80a05855d6ee39ea', 'f6b4907acf16b78c2fc07f149935d633d1eeda1c', '0164231423', NULL, 1, '2020-09-20 02:47:36', '2020-09-20 02:47:36'),
(349, '5a801bbb08bbd1161d14329b30d85e5e', 'Chong Bee Khim', 'chongbk62@gmail.com', '65691978b7be762750aae3948a4282b395bc22fb97dfb72421b22016e5fd203c', '28b1e5399df840a19257970f3bd6c360c41fed01', '0164827339', NULL, 1, '2020-09-20 03:27:51', '2020-09-20 03:27:51'),
(350, '599f70da81427e0df1cc2170170831ae', 'Pedly khoo aik loon', 'Pedlyloon@gmail.com', '85a110a89e072c086aab57316450fceb5e65e4e2bb310d97ec25ecdaa0f8fffc', '4ce56acd195ef3baf6b56be0bee0aacc36526a71', '0164977966', NULL, 1, '2020-09-20 04:14:08', '2020-09-20 04:14:08'),
(351, '22a6d6b3c99d286f438ba1857a3e45d9', 'YEOH KEAN KEONG', 'yeohkk0620@gmail.com', '09549389887ceecc88c8ed96037324f7dce69e02b3434d79110e081105f9e436', '02986a82b90f1be181b971064a69c4e459406a3d', '0124217964', NULL, 1, '2020-09-20 04:57:31', '2020-09-20 04:57:31'),
(352, 'd5b1487b52befa420c213a9dc1007ab8', 'Ong Hock Aun', 'penang.oha@gmail.com', 'e9a724ab8be0d55cf05f9b6188d9f52f7b5790f27c2ce61cdce1a7bc47ae04dd', '95374e39c36a18843b4df883c49c6e038f17f151', '0164078393', NULL, 1, '2020-09-20 05:01:37', '2020-09-20 05:01:37'),
(353, '76f0b128535e9369610c107807e36af6', '陈文周', 'bctan8@gmail.com', '20d0e72c791d9e712857f86ea7eeee576b99400d6de3028e08467dc8e601924c', '16aced112d2b6455f5e96a00ce32c0cec62fa28a', '0125523630', NULL, 1, '2020-09-20 06:20:04', '2020-09-20 06:20:04'),
(354, 'a0e486607777119faa7875bea129c582', 'Ooi Chin Chin', '', '07283d3b1bd6c05724e49fd186d818863c154158d47197f5b1fd4b93978ed7d0', '5387240bbe3dd8bf4b1d16f6320543fefa8a3bef', '0164528093', NULL, 1, '2020-09-20 07:22:37', '2020-09-20 07:22:37'),
(355, '43f13422a78aabe87c46147c153165b2', 'Teoh Pek Em', 'rosie264@gmail.com', '9ae49f82edc55092f7e4c2e67a42ff0b526108fee6ebe6cea552f718ce715bcc', 'd0307a194ac4f69db1cd07c40ac89384c5ca558e', '0124565289', NULL, 1, '2020-09-20 08:06:18', '2020-09-20 08:06:18'),
(356, '8a352b7a357b5fed1e86fb3376ab9b5f', 'Teoh Shin Lynn', 'shinlynnteoh@gmail.com', 'c5454244ecdb30dc8f46221fd8176ff72b4f43835fc0e60c58b77ef7ad68eb96', '086a95bb509b6d2203c350550d57ea91589c96dd', '0124989059', NULL, 1, '2020-09-20 08:19:24', '2020-09-20 08:19:24'),
(357, '7cadca336c1bdc1c74957d90c88b2da7', 'Ooi Lip Xun', 'ooilipxun0908@gmail.com', 'cb300c5d2c11951183ab7a5cf771b8d9ea096f4f6b4c2e569f40edc7bbe25d08', '93ccaeac396635947db8f3cb33b410d894e8e673', '0125680703', NULL, 1, '2020-09-20 08:57:25', '2020-09-20 08:57:25'),
(358, 'e450ca09112007f29080e569081810f8', 'Wong Wei Sheng', 'wongwei78996@gmail.com', '5c0ca768b5d740f0f344f9e4ada922354ccaa03eca16ccb7118bf695d9e3a589', '3bfd51654821c19ce84d19f1da2ca33468c86a98', '125802747', NULL, 1, '2020-09-20 09:39:36', '2020-09-20 09:39:36'),
(359, 'a2ebf16af4fcafdc09923b990c97fe8a', 'Sim Guek liang', '9_4_8jalan bagan jermal 10250 pg', 'c1ef247ec72e3bb8eb84bdf46dc1399d56e569132db8fbc7417a040194774642', '9cf803031d66a47e8185d21877ba0fc8965c73e4', '0164900018', NULL, 1, '2020-09-20 11:52:25', '2020-09-20 11:52:25'),
(360, '2b6616da04750159c4b1170bad60cdef', 'Kay Chan', 'kayyie@gmail.com', 'aa16bb13180b7478fa7f4af62bfb7cae87e3b4a71d503a5e6b8a732ce418ac50', 'e544272b22f72255c44bd1bc6121a33dc2c9d8b1', '01159453928', NULL, 1, '2020-09-20 12:05:09', '2020-09-20 12:05:09'),
(361, '366b136aa72ad0879ccb635a2e229aa3', 'Chuachookooi', 'Cckpcc118@yahoo.com', '3766406fb9164d230217f49b6dd8a79d39853b5e1b981532b7451f60286439b7', 'a10f38caa210b9598a751eece32fd48f03407815', '0124292121', NULL, 1, '2020-09-20 12:09:02', '2020-09-20 12:09:02'),
(362, 'a5faba1c8082afbd49f688ad70f2c173', 'Mr Chong', 'huthoo@yahoo.com', '70df71b510192d020db26d9c49776d082e5856aa43f3956ce16b8657aedddd27', '0a395e47200e87c2ce516ace2138275173af1b36', '0124811998', NULL, 1, '2020-09-20 12:38:15', '2020-09-20 12:38:15'),
(363, 'b3cf99492adc4ab7700d730750ce2d93', 'Elaine', 'elainechg@msn.com', '3ee721bd21637cf5433b5062ad906938597fb92e70e238caea6ce04bc0307e8a', '317eaea6d38e0763812760a328c2fb1670ec9478', '0164655753', NULL, 1, '2020-09-20 13:43:53', '2020-09-20 13:43:53'),
(364, '38fc7fc6bbc44fe3b523d0e6c989bd5f', 'Goh chin hack', 'bm-goh@hotmail.com', 'da47ce5803de489324ca7e9bec61a44663d9db01cde3a66ed4b661d058bde553', '54191b4177bbdf5472b14b39f46c67cde2dc4cf7', '0174222100', NULL, 1, '2020-09-20 13:46:40', '2020-09-20 13:46:40'),
(365, 'c36b111766f8d6a7bb9fe87a0b6a6ea3', 'Li cheng', 'Lichen_khaw@hotmail.com', '0680944bc6e23a2fe46544c38451ffc4bac41f1a9bee6647d02cdea7f98da496', 'c91d149de04a477f1885c3e296a4b479cb5f4bef', '0175088468', NULL, 1, '2020-09-20 14:22:40', '2020-09-20 14:22:40'),
(366, 'fa43be0cc471645ef8b2772425ef68ee', 'Loo cheng hong', 'annyloo_44@hotmail.com', '94e4e586e82586e832a6981b20354bc0c5950293234b71031e8c60ff951d9baf', '9b619113e17a8c62cd9aae6c14106809d04a7f37', '0164397296', NULL, 1, '2020-09-20 14:29:21', '2020-09-20 14:29:21'),
(367, '0c743251179c5fee6c946df304d31bf4', 'Low Li Yuet', 'Yuetlow@yahoo.com', 'a496d096eeb0905440f73c2cddf5c2b85ff65e7c4956910b980dc7dcdab0c7ab', 'c9097c1288d9cd547d29150b2f91ac9282534b4f', '0164212685', NULL, 1, '2020-09-20 14:40:49', '2020-09-20 14:40:49'),
(368, 'b1d0c3086565221af1d984fff0400ce8', 'Iven NG', 'iven.ng@klkoleo.com', 'e8c8f6257f86abc9d656b87f9fecc09b209f452bbccc5fff3fc3c0138523e962', 'bcb76df6d58241d0bc28f587a7c48e770dd1aed2', '0123045394', NULL, 1, '2020-09-20 15:40:36', '2020-09-20 15:40:36'),
(369, '8826a0c588324f0421956f9f6b7b7605', 'tanchienyang', 'jian_319@hotmail.com', 'a4ccad94ae0eafde63184d603a2e58234f44d78c66f8ddc5f681aecec4551454', '3db57a3ec2d9460fa3ee65368f36873638b07198', '01115448098', NULL, 1, '2020-09-20 16:35:13', '2020-09-20 16:35:13'),
(370, 'bf0c5778b1f2690f5aa38d295aa167c6', 'SIM CHING SERN', 'simphortay@gmail.com', '4791411e71ac5e25c982a1978f5616ea7dc44e28cd654d5e67ced491166bddae', 'fa19864cb4910a10b8243450da3ad461ded3425b', '0125046650', NULL, 1, '2020-09-21 01:18:13', '2020-09-21 01:18:13'),
(371, '03708ce055cecebbda87cd0581ebeaf2', 'Adam', 'jjtham_5459@yahoo.com', 'b064e041c5103db825910c249a74406394d3e721394f5126458dbaa8f88cab11', '3f7b1fb02b5c289a8323aa4d3498190492fb05af', '0126566309', NULL, 1, '2020-09-21 01:52:40', '2020-09-21 01:52:40'),
(372, '21f658b4e95b553e29c199b9672dabf3', 'Chiang', 'yf_chiang@hotmail.com', '5e98fb4fd004b8ada3c3f26d37d54725d71e9f4b5933ba40bb5b41cd1e30929d', '64b9ebb1772cfa9ff4414c4db5d646b9ec05ebc3', '0194708098', NULL, 1, '2020-09-21 02:10:54', '2020-09-21 02:10:54'),
(373, '5217ac6b65411097b822c6605be139a0', 'Steven Ong Seng Ann', 'stevenosa88@gmail.com', '5c8b91efbb8794f6fd293a13aa4912796d50fb37687a9a3244ee3976a7381f7f', 'ee1958e722d12e05e94907b1687e615a6c44f6f0', '0124068833', NULL, 1, '2020-09-21 02:52:22', '2020-09-21 02:52:22'),
(374, 'ccc3422d8e3aee5b13fe0e184a9c6171', 'Qian Ning', 'n1ng-@hotmail.com', '764a2e89c918898a14e41237ebe7790679961aed713c42d56e5bf240bf30f34e', '3dd44fe76268b95920f057fba4a3afd079bac453', '0175551637', NULL, 1, '2020-09-21 03:27:46', '2020-09-21 03:27:46'),
(375, 'b6ef7522bd58325e75a0b4ee923d32e1', 'Chong Xin Wei', 'x.w.chong@hotmail.my', 'b1a644cd37d3d7ee9671e48f818c2ea7e7b3a33fb79602023b85200eae48f33d', '36feffbd6f34660109bb588b1e38b22bb2b572fd', '0193312881', NULL, 1, '2020-09-21 03:35:40', '2020-09-21 03:35:40'),
(376, '123d2eba320894c6b9f4e510a8e9ac17', 'Eunice Tan', 'hltaneunice@gmail.com', '2c68c529ec104e01a4f01802256d4ea0e49b07707f1ac17ed12029d39bf92e9b', 'e9bc4a521b5e62331d0cbe72ab1758802958f3fd', '0124216504', NULL, 1, '2020-09-21 05:05:45', '2020-09-21 05:05:45'),
(377, 'f5d25fce40c10532dd307ba27220ae33', 'JEN', 'jenchoo.1221@gmail.com', '7daea7a8254e43ebd1ad02328285c18ab0cdea631dba1f82b823fd15392dfde5', '34545298806bf50631058714ca75d78afcfaef5d', '0164319426', NULL, 1, '2020-09-21 05:11:58', '2020-09-21 05:11:58'),
(378, 'e38db08f23a9c01058dcf9a381dedf5d', 'Tan Siew Leng', 'tan_siewleng@hotmail.com', 'f51e6a903bcf788bc851e968ece923363c084e4ce077c8d9f22de158e4078ac8', '7fb9871d7438d5ba1baaac600774bf6834b8115d', '0124725336', NULL, 1, '2020-09-21 05:41:04', '2020-09-21 05:41:04'),
(379, 'c66d76217cf4fba128e8c4147058c7ed', 'James Tan', 'Cy711james@gmail.com', 'abd5afaeed49bddbad7a3252526fe922c5e156f47b19b504df3c292c61fc523a', 'e1c12294939412b45ecd2ab7ad3989765881afe1', '0149045998', NULL, 1, '2020-09-21 06:48:44', '2020-09-21 06:48:44'),
(380, '1c2ed749f8b8c04844f9d3b7d2e4e0b3', 'Yap Thiam Hin', 'yth955@gmail.com', 'de2c2cb4877ce238a20b0b9231571e99fb84716b7f90df14e1305126388900a9', 'b11d9a2246585b097ce103052fc8d5b5e59eed11', '0192214443', NULL, 1, '2020-09-21 07:21:39', '2020-09-21 07:21:39'),
(381, '333d0a2b4de76456058c6b3a63b54a19', 'Kam Sing Ho', 'ksho@hotmail.com', '1a81e793494e65d7ee72c832f638c40ae7371a27b4d897295b5a0353b177077b', '604da1bc736baccdfb63748f1cc3afcf2d038c87', '01139787495', NULL, 1, '2020-09-21 07:51:35', '2020-09-21 07:51:35'),
(382, 'b133e70cfe70a642daa7f77641ceb289', 'LOOI CHING CHIEH', 'looiginger@yahoo.com', '7190aac48024b221754439c2ca1de89d311e0a520abc9dce30a0d214d4efb24f', '86f26575d63da993549023ff6ab9ed3aae913f9f', '0124368299', NULL, 1, '2020-09-21 09:01:24', '2020-09-21 09:01:24');
INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(383, 'c134c1549fe47829726897cad41b95a9', 'JG', 'Jaclyngohsl@gmail.com', '5d60bd9b29ddff5792a615424d241eb81e51d06838e4f56150a838a40f755b97', 'b4211fc1bef16806e3e7f89f6fbf230518c53d2b', '0125199970', NULL, 1, '2020-09-21 10:15:06', '2020-09-21 10:15:06'),
(384, '8e46ab691a193b582927d60787257cb3', 'Wong kar wai', '', 'ad032ae981ab16eec3e01fad108d4db277e8302edfb3c14251c5b58dd3b0bcf6', '532146e6ba729c638c48a81eef11b539d9cf6f4f', '0182754656', NULL, 1, '2020-09-21 10:26:09', '2020-09-21 10:26:09'),
(385, '982abe94b8debed0750f32c2f5c3d5e7', 'GARY THOR', 'garykhthor@gmail.com', '7516cc660fc6ce061b7fd402aea9a93feff2dbd89663b86aea2c95bceba12c3a', '2b0ee545407593b3b8f0bf8170a126ab7e940c8e', '0167777550', NULL, 1, '2020-09-21 10:54:23', '2020-09-21 10:54:23'),
(386, 'f5f882653085e9542b5cff4688571da4', 'Fook Tone Huat', 'fookth88@gmail.com', '7c9e0b68cc3a45bb86846568beba98a7b4209816e2abf9197e8b5f0ef82deab8', '76a47ddaa0ce32c8ee411548cda96efc66e4d7ae', '0174752022', NULL, 1, '2020-09-21 11:25:22', '2020-09-21 11:25:22'),
(387, '83767fc0e8470ee8b192e7a9a60b52bf', 'nelson lim', 'nelsoncherng@gmail.com', 'f288d368299c61c7c4ab5ae9dd75f574d497f3cec5f49c78d915bffd58228338', '4d74dc4ec13e3f4edfa16ab2baf56d1fa9adf252', '0128405820', NULL, 1, '2020-09-21 13:01:56', '2020-09-21 13:01:56'),
(388, 'c404142bd0c26fa06610b192d3c31194', 'Lim', 'p1gy@hotmail.com', '6d49963bf72bcc05ec103871827252f0821a02b67d85a633fefcbfb1fe4f51b4', '431b8dbb5e808e07f5ad5be9466fc7a7ffc10022', '0164926988', NULL, 1, '2020-09-21 13:22:58', '2020-09-21 13:22:58'),
(389, '2cb75973bd2a0832f4d0ca34283403fc', 'Tang siew liang', 'selin_e06tsl@yahoo.com', '2deede9bd925c702fd760ae31deea0fa19cbdd03969e5545534dc61c1e5db633', '56297e664665cd71f026179ef7bb6926a815de7b', '0125825118', NULL, 1, '2020-09-21 13:35:50', '2020-09-21 13:35:50'),
(390, 'fdadd273c5d18d7782a5b1491ffc18ee', 'Stephen Soon', 'stephensoon@mnp.com.my', 'dc1b31477ee2cd7fb735bb28dd4ed3f15747a6aa6f81dd2367f7d1b3a17296c2', '9ea12bffa4b365cf3a706eacd9d1ad426f0e99f3', '0167231440', NULL, 1, '2020-09-21 13:37:30', '2020-09-21 13:37:30'),
(391, '1b5b0421c666756dc580cf309825003a', 'Ong Ah Kwong', 'Akwongong@gmail.com', '9738fd99b3a46f592d522762c79f23f4c63184d33cf3607269f8ce02c68863c2', '7cb0497c9d5a20715b2a288b097d78efb4d733f4', '0164956671', NULL, 1, '2020-09-21 13:40:52', '2020-09-21 13:40:52'),
(392, '72b511ff03a47a742a003041f0d4c299', 'Chuah chooi booy', '', '8fc5350351e6126455d33df3ea2bc978a23401fdadcf35c89fb57d773f5b34f7', '1f3bc55484a021ad59395c2001c6940227a37504', '0164686861', NULL, 1, '2020-09-21 14:00:07', '2020-09-21 14:00:07'),
(393, '23f3c1c16448d14adae6d0e365f03790', 'Tony Ang', 'Munling@kalyx.com.my', '6f16d1e398e904fae6ba2bc7c6f22c8da31dbf26d7dc2bdee245e2bda68cbdf3', 'fd750401f6d9ac58aa28d8acce6849b5e5fcffa4', '0105195328', NULL, 1, '2020-09-21 14:08:00', '2020-09-21 14:08:00'),
(394, '6c68a6bff212447cac1283eb6b360fa8', 'ivan', 'raincococ@gmail.com', 'a40c856aa3e579be112ce018339f164c57b9b44246fe383619f15504d464d3a4', '257df96f4c7b1da49465b15abc15e7f0efb18ed8', '01126682568', NULL, 1, '2020-09-21 14:08:56', '2020-09-21 14:08:56'),
(395, 'c5df18a26082b7d39a64075c8739dc26', 'Ng Chor Wooi', 'ngcw9039@gmail.com', '837503b55cdc91fc118972e7448e77e300f1d1860244b8c5233f85ecdf5d8ecb', '1e95f605cd24eced2353313ca51d2138953d186e', '045824090', NULL, 1, '2020-09-21 14:31:12', '2020-09-21 14:31:12'),
(396, 'be8e5626a66bc2d3cbadc4456574ef3f', 'Hadden Lim', 'hadden.kalyx@gmail.com', '41ae1863e22c2cdae3ede08b93021ec8f7286e0a0cd8632fdd549b5861a64f73', '057f2a3753f11f10a9df24a06abb4ea53f888d1d', '0164190499', NULL, 1, '2020-09-21 15:00:40', '2020-09-21 15:00:40'),
(397, '975d2af20634919446204e29c72c3184', 'Suu Ann', 'sylaw2011@hotmail.com', '0424426642e0845af8f6b4530e6fc4b1af26b7b76c6116f90cc34453727044f7', 'b70009a2beec96f85160a544c794445beab46f57', '0192515418', NULL, 1, '2020-09-21 15:03:55', '2020-09-21 15:03:55'),
(398, '468fe0dadb46564d60dec8193fb2800c', 'Tai Lian Pong', 'tailp@ghtag.com', 'b32bb6c459395db0941d15d3ffcd29e950173440a2dd38850c45021fce3c41bf', '45b7ca8a3a3f7268d9e43227d681caac0a7e8bba', '0124927762', NULL, 1, '2020-09-21 15:06:57', '2020-09-21 15:06:57'),
(399, '47d63114c4cd44055ac761d76b12f73f', 'Teh Khian Beng', 'tehkb@ghatg.com', 'a797b56a2dfba2abd84e91e1223849b59a0b1a15be86a4d7c39705ca7509604c', '69307907709e03fcab169dfc122360e0c41f4959', '0124271614', NULL, 1, '2020-09-21 15:09:02', '2020-09-21 15:09:02'),
(400, 'fa8eb16a8eab0dd07671a7fc2f382be0', 'Steven Khor', 'stvnkhor@gmail.com', '6ed08ad6804a4280f4e1c99c76f1d6c6904476b0c08e467ac56e339ded6fa24e', 'c2c3bde46fbad1c5f1d3e5670dfe99013137f69d', '0124245725', NULL, 1, '2020-09-21 15:33:08', '2020-09-21 15:33:08'),
(401, '391d14ba1245537af69d2eff71278a83', 'Tan shin rou', 'roxy92.t@gmail.com', '3b2dc5dd5884f2696e12fe364c19f0af5236981dbc38a6f4cb0b8fc807041243', '5c30ef88a197d636237b6934fb61d4eb8940aada', '0125387282', NULL, 1, '2020-09-21 15:33:57', '2020-09-21 15:33:57'),
(402, '79128b6af7c44d4b14f910aa1886a636', 'LIM TZE LING', 'limtzeling@gmail.com', '2677820f7f6c197a82c7c8d77137d8d71d3acae09d665ff6082ba2b6ae7945e1', '68a14c37350bcffb6c063dc70de24c52ab2a2ea8', '0164766387', NULL, 1, '2020-09-21 23:52:11', '2020-09-21 23:52:11'),
(403, '034d8477dbc8be18118916fc92debf88', 'Ooi chai sin', 'tanmc_86@hotmail.com', '2d9fa03b5111a92948b34a9a3d985c95c27f5dd552ca35dc00428345c07b3abd', '1d637bcaf4f6f69d0d8de07e385174211b16d98e', '0194055555', NULL, 1, '2020-09-22 00:05:09', '2020-09-22 00:05:09'),
(404, '7911c13c558e587d217e05bb9607a0f4', 'Lim Chen Yen', 'yencpy@live.com', 'da32e3c6fb697b2da4792742825e51ad4a0cf110b8e5afbbf253bdf08e49353d', '1f7cdc3bf07a45ec7772b17b3d556eddbe778004', '0174456550', NULL, 1, '2020-09-22 00:31:50', '2020-09-22 00:31:50'),
(405, 'f84b6522700ccaa51890a793ecd0fd3e', 'Jimmy Ong', 'jimmy@nsr-rubber.com', 'c918a2759968f8bd29c285b3129fa66109e1859d67bb97fcb88b5da7481d361e', 'a2af0e923e1251a3a880d6e1fc8b7470e2da7a43', '0124386080', NULL, 1, '2020-09-22 00:49:20', '2020-09-22 00:49:20'),
(406, 'e2d29157d71ae00db46d8e2a83170f95', 'yeoh', 'frankyeohtl@gmail.com', '584e45b4444c9fbc6192490c888c3e6ad4be4fecb1808759531734b9d8d472dd', '3edfc4e3fb68b5422521ee750d14e4146ab6b25f', '0103824483', NULL, 1, '2020-09-22 01:35:32', '2020-09-22 01:35:32'),
(407, '2e26a0ff3e7698e693f61dbe8026cee9', 'Janet Chan', 'Janetcyh888@gmail.com', 'db7b5a226a4784b99f60efef7e7b02c3c41fcdc61fda6646407d4155f12434b9', 'f6f0a0bcc197a9ae5bcd7b05022152585399329d', '0126596025', NULL, 1, '2020-09-22 01:37:07', '2020-09-22 01:37:07'),
(408, '2d3449c5e83472ea9ff29fffbb745418', 'Heng YH', 'yakhoi-heng@hotmail.com', '46b9780f7a386a656d8a988e2c92cea3546a24f1bd361842a84200413c4d23eb', '4509e577d0df6ae58b3c26ef221b933d6cf6b738', '0164448678', NULL, 1, '2020-09-22 01:49:30', '2020-09-22 01:49:30'),
(409, '1993ac3085b54157b6b3c1768c100f65', 'CHNG KHYE SENG', 'angelsky828@gmail.com', 'ffaeb385b94937462adcde87929e604e65835f42b73d28fcb843194a81e26265', 'a5cc4f7ba64b1c0d72a8d27374868d44a0bb1655', '0164488661', NULL, 1, '2020-09-22 02:21:59', '2020-09-22 02:21:59'),
(410, 'd8032cef7a30271bc95bb22f3c6e1c97', 'NORMAN NG', 'clchngsky@gmail.com', 'c72eaa7eecf9156a4d7aeaf1655a8887bea470d745c287b85b16511a392d3299', '296064ab5a3d4d2d7db6b960cf7507b91581bb65', '0164444188', NULL, 1, '2020-09-22 02:24:53', '2020-09-22 02:24:53'),
(411, '25cd0fbf8578fef44b5db2b1f3f6f319', 'Goh ah kooi', 'akgoh8039@gmail.com', '561b35d3080b83bae6ff014bd9b143e32370b524871f1043fe91c521d5882847', '400732fe56995eaf623cdcfd28028916bfac8170', '0124138039', NULL, 1, '2020-09-22 02:25:00', '2020-09-22 02:25:00'),
(412, 'de60aee579841b6af63f6c10007480b4', 'SuiSui', 'ypkoay@yahoo.com', '5212d2a61a986658e969292a798430ce8724a3d5c59271f619c63a1e77d9d154', '1506871d9a66f28f40b5a33b71e4f286eb56a9a5', '0125114803', NULL, 1, '2020-09-22 02:36:00', '2020-09-22 02:36:00'),
(413, '5ac62dfb60c94f1e5cbcc02c1d13a9f9', 'LEE KENG ENG', 'richardlee73@Hotmail.com', '392619f64c017aa7617a60d0a72bf783e47dbd70c34f48502def32b44130a2eb', 'cb068da128f7c8d10ac758c5d594a3d952c429cf', '0194843008', NULL, 1, '2020-09-22 03:01:20', '2020-09-22 03:01:20'),
(414, '2cf5ab9c2b22e5cd1c38d175bc122208', 'Chee Lai Kwan', 'cheelaikwan65@gmail.com', 'dc874d81947be4e457bea0ca5a667b0833a053d7c32e42fd7ee4564ab288796b', 'be17c10eb80f9f6639e4c6bcf41c25e14779203f', '0197567828', NULL, 1, '2020-09-22 03:31:54', '2020-09-22 03:31:54'),
(415, '70525b9da206caaa4535d710f6fd38fd', 'Sim Jing Dong', 'jdsim111@gmail.com', 'ecdfebfff6c85bce85936a9b1d528b5be2540c928807b391e575bf441b2f55bb', '0d8a742fa5f3b5817e6df3cf455bdc5fbe692026', '0175660036', NULL, 1, '2020-09-22 03:39:21', '2020-09-22 03:39:21'),
(416, '95c738d33f3f252f3c97ba15f43f30a4', 'Liew Bee Lan', 'liew_beelan@hotmail', 'b8981ef5dfdf81c18343e240db3a437ac2cb3b5a98115cf1b4c31eb3c582f9f3', 'ad63a8797c43353e9d12a9ec03b793b7e9478fad', '012460868', NULL, 1, '2020-09-22 04:07:16', '2020-09-22 04:07:16'),
(417, '6831a659e9c083844f20bcdb0b2cc4ed', 'Liew Bee Lan', 'liew_beelan@hotmail.com', '2fd542d6471c675b3ecb3931e86b4d619837f7008fa5688cbe9b29b98210c7c6', '4167256b7ab665f96eca00b914b1ba14097ccc47', '0124608688', NULL, 1, '2020-09-22 04:07:56', '2020-09-22 04:07:56'),
(418, '0eb62e227f6f0346a15520147093f4c3', 'Chew Eing yit', 'ey_chew76@yahoo.com', '3d78a7e9d0f7444907642ce67d56e499d675f58b1ee707d7450f4cf77f2c9290', 'd2e4a8cf523eee387c7015fe958e7058fdd180cc', '0124276659', NULL, 1, '2020-09-22 04:13:16', '2020-09-22 04:13:16'),
(419, 'fb66c82e0515950bf2b00375f709c67c', 'LEE HONG CHUN', 'hongchun.87@gmail.com', '5ed3db6820d531901141bf235d94c71a45bf036958f29d1aab666a9919bb0b1f', '78ed2aad73eb4f67f2c5d68fea642d0928b538d7', '0164134567', NULL, 1, '2020-09-22 04:22:05', '2020-09-22 04:22:05'),
(420, '3a4c805282fb5fd8ff473c30b507fed4', 'Yong SS', 'ykyong31@gmail.com', '1449eb13921e4d007967da5b2fc6d3aa907e1a350526d0955c749179119f5822', '87dfe08193b66eb518c534fd200b873ace5472c6', '0194472943', NULL, 1, '2020-09-22 04:29:56', '2020-09-22 04:29:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_share`
--
ALTER TABLE `live_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `sharing`
--
ALTER TABLE `sharing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_share`
--
ALTER TABLE `sub_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `title`
--
ALTER TABLE `title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `userdata`
--
ALTER TABLE `userdata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `live_share`
--
ALTER TABLE `live_share`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `platform`
--
ALTER TABLE `platform`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sharing`
--
ALTER TABLE `sharing`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `sub_share`
--
ALTER TABLE `sub_share`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `title`
--
ALTER TABLE `title`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `userdata`
--
ALTER TABLE `userdata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=421;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
