<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Sharing.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $sharingId = rewrite($_POST["sharing_uid"]);
    $status = "Delete";
    $type = "3";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $sharingDetails = getSharing($conn," uid = ?   ",array("uid"),array($sharingId),"s");   

    if(!$sharingDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "i";
        }

        array_push($tableValue,$sharingId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"sharing"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // // echo "DELETED !!";
            // header('Location: ../adminDashboard.php');
            if(isset($_SESSION['url'])) 
            {
                $url = $_SESSION['url']; 
                header("location: $url");
            }
            else 
            {
                // header("location: $url");
                header('Location: ../adminDashboard.php');
            }
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
