<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $username = rewrite($_POST["update_username"]);
    $email = rewrite($_POST["update_email"]);
    $phone = rewrite($_POST["update_phone"]);
    $uid = rewrite($_POST["user_uid"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $userDetails = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");   

    if(!$userDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "UPDATED !!";
            header('Location: ../adminDashboard.php');
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }
}
else 
{
    header('Location: ../index.php');
}
?>
