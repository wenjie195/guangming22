<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminAddUserProjectLogo.php" />
<meta property="og:title" content="User Logo | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>User Logo | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminAddUserProjectLogo.php" />

<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="croppie.js"></script>
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<?php 
	$picUid =$_POST['user_uid'];
?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">
	<h2 class="h1-title">Add User Project Logo</h2>
	<div class="width100 overflow text-center">
		<input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
		<input type="hidden" name="picUid" id="picUid" value="<?php echo $picUid ?>">
		<div class="clear"></div>
		<div id="uploaded_image"></div>
	</div>
	<div class="clear"></div>
</div>
	
<div class="clear"></div>

<div class="footer-div width100 overflow text-center">
	<p class="footer-p">© 2020 Guang Ming Daily, All Rights Reserved.</p>	
</div>

<div id="uploadimageModal" class="modal update-profile-div" role="dialog">
	<div class="modal-dialog update-profile-second-div">
		<div class="modal-content update-profile-third-div">
			<div class="modal-header update-profile-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Crop Project Logo</h4>
			</div>
			<div class="modal-body update-profile-body">
				<div id="image_demo" class="profile-pic-crop"></div>
				<div class="clear"></div>
				<div class="width100 overflow text-center">
					<!-- <button class="green-button mid-btn-width clean btn btn-success crop_image">Crop Image</button> -->
					<button class="clean-button clean login-btn pink-button crop_image">Crop Image</button>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.panel-default>.panel-heading{
	text-align: center;
    font-weight: bold;
    font-size: 18px;	
}
.modal-dialog{
	height:350px;}
</style>
  
<script>
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
		enableExif: true,
		viewport:{
			width:188,
            height:138,
			
		},
		boundary:{
            width:300,
            height:300
		}
	});

	$('#image_one').on('change', function(){
		var reader = new FileReader();
		reader.onload = function (event){
			$image_crop.croppie('bind',{
				url: event.target.result
			}).then(function(){
			console.log('jQuery bind complete');
			});
		}
		reader.readAsDataURL(this.files[0]);
		$('#uploadimageModal').modal('show');
	});

	$('.crop_image').click(function(event){
        var picUid = $("#picUid").val();
		$image_crop.croppie('result',{
			type: 'canvas',
			size: 'original'
		}).then(function(response){
		$.ajax({
		// url:"upload.php",
		url:"adminAddUserProjectLogoFunction.php",
        type: "POST",
        data:{"image": response,picUid:picUid},
		success:function(data)
		{
			$('#uploadimageModal').modal('hide');
            $('#uploaded_image').html(data);
		}
		});
		})
	});
});
</script>

<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>

<script>
	$(function() {
		$( '#dl-menu' ).dlmenu({
			animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
		});
	});
</script>

</body>
</html>