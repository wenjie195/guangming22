<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allUser = getUser($conn," WHERE user_type = '1' ");
$platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminAddUserProjectSharing.php" />
<meta property="og:title" content="Add New Zoom | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Add New Zoom | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminAddUserProjectSharing.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    $username = $userDetails[0]->getUsername();
    ?>

        <form action="utilities/registerNewSharingFunction.php" method="POST" enctype="multipart/form-data"> 

            <h2 class="h1-title"><?php echo $username;?></h2> 

            <div class="clear"></div>

            <h2 class="h1-title">Add New Zoom (Webinar)</h2> 

            <div class="clear"></div>

            <input class="aidex-input clean" type="hidden" value="<?php echo $userDetails[0]->getUsername();?>" name="register_user" id="register_user" readonly> 

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text">Booth/Department Name</p>
                <input class="aidex-input clean" type="text" placeholder="Title" name="update_title" id="update_title" required>       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Booth/Staff Image (354px (w) X 216px (h))</p>
                <p><input id="file-upload" type="file" name="file_one" id="file_one" class="margin-bottom10 pointer" required/></p>
            </div>

            <div class="clear"></div>
<!--
            <div class="dual-input">
                <p class="input-top-text">Host 1</p>
                <input class="aidex-input clean" type="text" placeholder="Host Name" name="update_host" id="update_host" required>       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Platform</p>
                <input class="aidex-input clean" type="text" value="Zoom" name="register_platform" id="register_platform" readonly>     
            </div>

			<div class="clear"></div>
-->
			<div class="dual-input">
                <p class="input-top-text">Staff Name (Optional)</p>
                <input class="aidex-input clean" type="text" placeholder=Staff Name" name="register_remark" id="register_remark" >       
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-text">Zoom Link</p>
                <input class="aidex-input clean" type="text" placeholder="Zoom Link" name="register_link" id="register_link" required>       
            </div>
<!--
            <div class="dual-input second-dual-input">
                <p class="input-top-text">Remark</p>
                <input class="aidex-input clean" type="text" placeholder="Remark" name="register_remark" id="register_remark" required>       
            </div>
-->
            <div class="clear"></div>
           
            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
            </div>

        </form>
    <?php
    }
    ?>    
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>