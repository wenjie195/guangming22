<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/Sharing.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $liveDetails = getLiveShare($conn);
// $subDetails = getSubShare($conn);

// $liveDetails = getLiveShare($conn," WHERE status != 'Available' AND type = '1' ");
// $subDetails = getSubShare($conn," WHERE status != 'Available' AND type = '1' ");

// $liveDetails = getLiveShare($conn," WHERE status != 'Delete' ");
// $subDetails = getSubShare($conn," WHERE status != 'Delete' ");

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

// Program to display URL of current page.
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
$link = "https";
else
$link = "http";

// Here append the common URL characters.
$link .= "://";

// Append the host(domain name, ip) to the URL.
$link .= $_SERVER['HTTP_HOST'];

// Append the requested resource location to the URL
$link .= $_SERVER['REQUEST_URI'];

if(isset($_GET['id']))
{
    $currentLink = $_GET['id'];
}
else
{
    $currentLink = "";
}

$allUser = getUser($conn," WHERE user_type = '1' ");
$allLive = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminViewBroadcastDetails.php" />
<meta property="og:title" content="All Video | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>All Video | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminViewBroadcastDetails.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">
   
        <?php
            // if(isset($_POST['user_uid']))
            if(isset($currentLink))
            {
                $conn = connDB();
                // $liveDetails = getLiveShare($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['user_uid']),"s");
                // $liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status != 'Delete' ", array("user_uid") ,array($_POST['user_uid']),"s");
                $liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status != 'Delete' ", array("user_uid") ,array($currentLink),"s");
                // $username = $liveDetails[0]->getUsername();
            ?>
                <!-- <h2 class="h1-title"><?php //echo $username;?></h2>   -->
                <div class="clear"></div>
                <h2 class="h1-title">Main Video</h2>    

                <?php
                if($liveDetails)
                {   
                    $totalLiveVideo = count($liveDetails);
                }
                else
                {   $totalLiveVideo = 0;   }
                ?>

                <?php
                if($totalLiveVideo < 1)
                // if($totalLiveVideo < 3)
                {   
                ?>
                    <form method="POST" action="adminAddUserMainVideo.php" class="hover1">
                        <button class="clean action-button" type="submit" name="user_uid" value="<?php echo $currentLink;?>">
                            Add New Main Video
                        </button>
                    </form>
                <?php
                }
                else{}
                ?>

                <div class="clear"></div>

                <div class="scroll-div margin-top30">                    
                    <table class="table-css">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Title</th>
                                    <th>Added On</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Action</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if($liveDetails)
                                {
                                    for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
                                    {
                                    ?>    
                                        <tr>
                                            <td><?php echo ($cnt+1)?></td>

                                            <td><?php echo $liveDetails[$cnt]->getTitle();?></td>

                                            <td>
                                                <?php echo $date = date("d-m-Y",strtotime($liveDetails[$cnt]->getDateCreated()));?>
                                            </td>
                                            <td><?php echo $liveDetails[$cnt]->getStatus();?></td>

                                            <td>
                                                <!-- <form action="editLive.php" method="POST" class="hover1"> -->
                                                <form action="adminEditMainVideo.php" method="POST" class="hover1">
                                                    <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                        Edit
                                                    </button>
                                                </form> 
                                            </td>

                                            <?php
                                                $liveStatus = $liveDetails[$cnt]->getStatus();
                                                if($liveStatus == 'Available')
                                                {
                                                ?>
                                                    <td>
                                                    <form method="POST" action="utilities/stopLiveFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                            Hide
                                                        </button>
                                                    </form>
                                                </td>
                                                <?php
                                                }
                                                elseif($liveStatus == 'Stop')
                                                {
                                                ?>
                                                    <td>
                                                    <form method="POST" action="utilities/startLiveFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                            Show
                                                        </button>
                                                    </form>
                                                    </td>
                                                <?php
                                                }
                                                elseif($liveStatus == 'Pending')
                                                {
                                                ?>
                                                    <td>
                                                    <form method="POST" action="utilities/startLiveFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                            Show
                                                        </button>
                                                    </form>
                                                    </td>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                    <td>    </td>
                                                <?php
                                                }
                                            ?>

                                            <td>
                                                <form method="POST" action="utilities/deleteLiveFunction.php" class="hover1">
                                                    <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                        Delete
                                                    </button>
                                                </form>
											</td>                                            

                                        </tr>
                                    <?php
                                    }
                                }
                                ?>                                 
                            </tbody>
                    </table>
				</div>
            <?php
            }
        ?>

        <div class="clear"></div>
        <h2 class="h1-title"></h2>   
        <div class="clear"></div>

        <?php
            // if(isset($_POST['user_uid']))
            if(isset($currentLink))
            {
                $conn = connDB();
                // $subDetails = getSubShare($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['user_uid']),"s");
                // $subDetails = getSubShare($conn,"WHERE user_uid = ? AND status != 'Delete' ", array("user_uid") ,array($_POST['user_uid']),"s");
                $subDetails = getSubShare($conn,"WHERE user_uid = ? AND status != 'Delete' ", array("user_uid") ,array($currentLink),"s");
            ?>

                <?php
                if($subDetails)
                {   
                    $totalSubVideo = count($subDetails);
                }
                else
                {   $totalSubVideo = 0;   }
                ?>

                <?php
                // if($totalSubVideo < 2)
                if($totalSubVideo < 6)
                {   
                ?>
                    <form method="POST" action="adminAddUserProjectVideo.php" class="hover1">
                        <button class="clean action-button" type="submit" name="user_uid" value="<?php echo $currentLink;?>">
                            Add New Project Video
                        </button>
                    </form>
                <?php
                }
                else{}
                ?>
            
                <div class="clear"></div>
                <h2 class="h1-title">Project Video</h2>    
                <div class="clear"></div>

                <div class="scroll-div margin-top30">                    
                    <table class="table-css">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Title 1</th>
                                <th>Video 1</th>
                                <th>Status</th>
                                <th>Edit</th>
                                <th>Action</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if($subDetails)
                            {
                                for($cntAA = 0;$cntAA < count($subDetails) ;$cntAA++)
                                {
                                ?>    
                                    <tr>
                                        <td><?php echo ($cntAA+1)?></td>
                                        <td><?php echo $subDetails[$cntAA]->getTitle();?></td>
                                        <td><?php echo $subDetails[$cntAA]->getHost();?></td>
                                        <td><?php echo $subDetails[$cntAA]->getStatus();?></td>

                                        <td>
                                            <!-- <form action="editSub.php" method="POST" class="hover1"> -->
                                            <form action="adminEditProjectVideo.php" method="POST" class="hover1">
                                                <button class="clean action-button" type="submit" name="subdata_uid" value="<?php echo $subDetails[$cntAA]->getUid();?>">
                                                    Edit
                                                </button>
                                            </form> 
                                        </td>

                                        <?php
                                            $status = $subDetails[$cntAA]->getStatus();
                                            if($status == 'Available')
                                            {
                                            ?>
                                                <td>
                                                    <form method="POST" action="utilities/stopSubFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                            Stop
                                                        </button>
                                                    </form>
                                                </td>
                                            <?php
                                            }
                                            elseif($status == 'Stop')
                                            {
                                            ?>
                                                <td>
                                                    <form method="POST" action="utilities/startSubFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                            Start
                                                        </button>
                                                    </form>
                                                </td>
                                            <?php
                                            }
                                            elseif($status == 'Pending')
                                            {
                                            ?>
                                                <td>
                                                    <form method="POST" action="utilities/startSubFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                            Start
                                                        </button>
                                                    </form>
                                                </td>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <td>    </td>
                                            <?php
                                            }
                                        ?>
                                    
                                        <td>
                                            <form method="POST" action="utilities/deleteSubFunction.php" class="hover1">
                                                <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                        
                                    </tr>
                                <?php
                                }
                            }
                            ?>                                 
                        </tbody>
                    </table>
                </div>
            <?php
            }
        ?>

        <?php
            // if(isset($_POST['user_uid']))
            if(isset($currentLink))
            {
                $conn = connDB();
                // $shareDetails = getSharing($conn,"WHERE user_uid = ? AND status != 'Delete' ", array("user_uid") ,array($_POST['user_uid']),"s");
                $shareDetails = getSharing($conn,"WHERE user_uid = ? AND status != 'Delete' ", array("user_uid") ,array($currentLink),"s");
            ?>

                <div class="clear"></div>
                <h2 class="h1-title">Zoom (Webinar)</h2>    

                <?php
                if($shareDetails)
                {   
                    $totalSharing = count($shareDetails);
                }
                else
                {   $totalSharing = 0;   }
                ?>

                <?php
                // if($totalSharing < 5)
                // if($totalSharing < 6)
                if($totalSharing < 13)
                {   
                ?>
                    <form method="POST" action="adminAddUserProjectSharing.php" class="hover1">
                        <button class="clean action-button" type="submit" name="user_uid" value="<?php echo $currentLink;?>">
                            Add New Zoom (Webinar)
                        </button>
                    </form>
                <?php
                }
                else{}
                ?>

                <div class="clear"></div>

                <div class="scroll-div margin-top30">                    
                    <table class="table-css">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Title</th>
                                    <!-- <th>Host</th> -->
                                    <th>Remark</th>
                                    <th>Link</th>
                                    <th>Edit</th>
                                    <th>Action</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($shareDetails)
                            {
                                for($cntBB = 0;$cntBB < count($shareDetails) ;$cntBB++)
                                {
                                ?>    
                                    <tr>
                                        <td><?php echo ($cntBB+1)?></td>
                                        <td><?php echo $shareDetails[$cntBB]->getTitle();?></td>
                                        <!-- <td><?php //echo $shareDetails[$cntBB]->getHost();?></td> -->
                                        <td><?php echo $shareDetails[$cntBB]->getRemark();?></td>
                                        <td><?php echo $shareDetails[$cntBB]->getLink();?></td>
                                        <!-- <td>Edit</td> -->

                                        <td>
                                            <form action="adminEditProjectSharing.php" method="POST" class="hover1">
                                                <button class="clean action-button" type="submit" name="sharing_uid" value="<?php echo $shareDetails[$cntBB]->getUid();?>">
                                                    Edit
                                                </button>
                                            </form> 
                                        </td>

                                        <!-- <td>Action</td>       -->

                                        <?php
                                            $status = $shareDetails[$cntBB]->getStatus();
                                            if($status == 'Available')
                                            {
                                            ?>
                                                <td>
                                                    <form method="POST" action="utilities/stopSharingFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="subdata_uid" value="<?php echo $shareDetails[$cntBB]->getUid();?>">
                                                            Stop
                                                        </button>
                                                    </form>
                                                </td>
                                            <?php
                                            }
                                            elseif($status == 'Stop' || $status == 'Pending')
                                            {
                                            ?>
                                                <td>
                                                    <form method="POST" action="utilities/startSharingFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="subdata_uid" value="<?php echo $shareDetails[$cntBB]->getUid();?>">
                                                            Start
                                                        </button>
                                                    </form>
                                                </td>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <td>    </td>
                                            <?php
                                            }
                                        ?>

                                        <!-- <td>Delete</td>         -->
                                        
                                        <td>
                                            <form method="POST" action="utilities/deleteSharingFunction.php" class="hover1">
                                                <button class="clean action-button" type="submit" name="sharing_uid" value="<?php echo $shareDetails[$cntBB]->getUid();?>">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                        
                                    </tr>
                                <?php
                                }
                            }
                            ?>  
                            </tbody>
                    </table>
                </div>

                <div class="clear"></div>

            <?php
            }
        ?>

        <div class="clear"></div>
        <h2 class="h1-title"></h2>    

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>