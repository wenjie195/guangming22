<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$mainAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '2' ");

$allAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminViewAnnouncement.php" />
<meta property="og:title" content="Admin Announcement | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Admin Announcement | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminViewAnnouncement.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <?php
    if($mainAnnoucement)
    {   
    $totalMainAnnoucement = count($mainAnnoucement);
    }
    else
    {   $totalMainAnnoucement = 0;   }
    ?>

    <?php
    if($totalMainAnnoucement < 1)
    {   
    ?>
        <!-- <h2 class="h1-title">Moving Announcement</h2> -->
        <h2 class="h1-title margin-top50 left-h1-title">Moving Announcement</h2>
        <a href="adminAddAnnouncement.php"><div class="clean action-button right-button2">Add Announcement</div></a>
    <?php
    }
    else
    {
    ?>
        <h2 class="h1-title">Moving Announcement</h2>
    <?php
    }
    ?>

    <!-- <h2 class="h1-title">Moving Announcement</h2> -->
   
    <div class="clear"></div>
    
    <div class="scroll-div margin-top30">
     
        <table class="table-css">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Content</th>
                        <th>Date Created</th>
                        <th>Edit</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($mainAnnoucement)
                    {
                        for($cnt = 0;$cnt < count($mainAnnoucement) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $mainAnnoucement[$cnt]->getContent();?></td>
                                <td><?php echo $mainAnnoucement[$cnt]->getDateCreated();?></td>
                                <td>
                                    <form action="adminEditAnnouncement.php" method="POST" class="hover1">
                                        <button class="clean action-button" type="submit" name="announcement_uid" value="<?php echo $mainAnnoucement[$cnt]->getUid();?>">
                                            Edit
                                        </button>
                                    </form> 
                                </td>
                                <td>
                                    <form method="POST" action="utilities/deleteAnnouncementFunction.php" class="hover1">
                                        <button class="clean action-button" type="submit" name="announcement_uid" value="<?php echo $mainAnnoucement[$cnt]->getUid();?>">
                                            Delete
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>  

    <div class="clear"></div>

    <h2 class="h1-title margin-top50 left-h1-title">Side Announcement</h2><a href="adminAddAnnouncement.php"><div class="clean action-button right-button2">Add Announcement</div></a>
    
    <div class="clear"></div>
    
    <div class="scroll-div margin-top30">
     
        <table class="table-css">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Content</th>
                        <th>Date Created</th>
                        <th>Edit</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($allAnnoucement)
                    {
                        for($cnt = 0;$cnt < count($allAnnoucement) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allAnnoucement[$cnt]->getContent();?></td>
                                <td><?php echo $allAnnoucement[$cnt]->getDateCreated();?></td>
                                <td>
                                    <form action="adminEditAnnouncement.php" method="POST" class="hover1">
                                        <button class="clean action-button" type="submit" name="announcement_uid" value="<?php echo $allAnnoucement[$cnt]->getUid();?>">
                                            Edit
                                        </button>
                                    </form> 
                                </td>
                                <td>
                                    <form method="POST" action="utilities/deleteAnnouncementFunction.php" class="hover1">
                                        <button class="clean action-button" type="submit" name="announcement_uid" value="<?php echo $allAnnoucement[$cnt]->getUid();?>">
                                            Delete
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>    

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>