<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Liveshare.php';
// require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Userdata.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$registrationDetails = getUserdata($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminViewRegistraion.php" />
<meta property="og:title" content="Admin View Registration | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Admin View Registration  | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<link rel="canonical" href="https://gmvec.com/adminViewRegistraion.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Registraton Details</h2>
    

	<div class="clear"></div>
    <div class="scroll-div margin-top30">
  
        <table class="table-css">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Register Time</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($registrationDetails)
                    {
                        for($cnt = 0;$cnt < count($registrationDetails) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $registrationDetails[$cnt]->getUsername();?></td>
                                <td><?php echo $registrationDetails[$cnt]->getEmail();?></td>
                                <td><?php echo $registrationDetails[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $registrationDetails[$cnt]->getDateCreated();?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>    

</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>