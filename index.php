<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $countryList = getCountries($conn);

// $total_visitors=mysqli_num_rows($result);
// if($total_visitors < 1)
// // if($total_visitors != "")
// // if($total_visitors)
// {
//     $query=" INSERT INTO pageview(userip) VALUES ('$visitor_ip') ";
//     $result = mysqli_query($conn,$query);
// }

// $query="SELECT * FROM pageview";
// $result = mysqli_query($conn,$query);

// // if(!$result)
// // {
// //     die("error".$query);
// // }

// $total_visitors=mysqli_num_rows($result);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/index.php" />
<link rel="canonical" href="https://gmvec.com/index.php" />
<meta property="og:title" content="Register | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Register | 光明線上產業展 Guang Ming Virtual Property Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="width100 gold-line"></div>
<div class="width100 same-padding overflow top-bg">
	<div class="width100 text-center">
		<img src="img/gmvec-logo.jpg" class="gmvec-logo" alt="GMVEC" title="GMVEC">
	</div>
	<h1 class="title-h1 text-center landing-title-h1 black-text">光明線上產業展<br>Guang Ming Properties E-Fair</h1>	
	<p class="text-center index-small-date-p"><img src="img/date.png" class="small-icon">25/9/2020 - 27/9/2020</p>
	<p class="text-center index-small-date-p"><img src="img/time.png" class="small-icon">10 am - 8 pm</p>
        <form action="utilities/userdataRegisterFunction.php" method="POST" class="index-form">
           
                <input class="three-div-width1 index-input clean" type="text" placeholder="Name 名字" id="register_name" name="register_name" required>
           

          
                <input class="three-div-width2 index-input clean" type="number" placeholder="Contact 联络号码" id="register_phone" name="register_phone" required>
          
                <input class="three-div-width3 index-input clean" type="Email 电邮" placeholder="Type Your Email" id="register_email" name="register_email">
          

            <div class="clear"></div>
			<div class="width100 text-center">
				<button class="clean-button clean register-button pink-button" name="register">Pre-register 预注册</button>
				<p class="index-small-details">Value rewards and rebates are waiting for you<br>超值獎勵与回扣等您领取</p>
            </div>
        </form>
	
</div>
<div class="clear"></div>
<div class="width100 overflow same-padding text-center">
	<img src="img/buildings.png" class="building-img">	
</div>
<div class="clear"></div>
<div class="width100 overflow same-padding program-spacing new-added-div">
	<div class="left-text-div same-text-div">

		<p class="index-dual-title"><b>光明線上產業展</b></p>
		<p class="left-text-div-p left-p1">
			線上看房，這里最旺!
			搶先登記，佔盡先機!</p>
		<p class="left-text-div-p left-p2">
			我國7 大房屋發展商聯合榮譽參展，
			提供全系列房屋款型任您彈指之間挑選，
			豪華公寓、房屋、別墅，
			享盡HOC擁屋計劃下的印花稅免付良機，
			數不盡的優惠配套，
			再加:</p>
			<table class="icon-table">
				<tr><td><img src="img/car.png" class="ul-icon"></td><td>買公寓~<b>送汽車!</b></td></tr>
				<tr><td><img src="img/management-fee.png" class="ul-icon"></td><td><b>免付</b>兩年管理費!</td></tr>
				<tr><td><img src="img/interior-design.png" class="ul-icon"></td><td>免費提供<b>室內设計!</b></td></tr>
				<tr><td><img src="img/lawyer-fee.png" class="ul-icon"></td><td><b>免付</b>買賣合約律師費!</td></tr>
				<tr><td><img src="img/cash-rebate.png" class="ul-icon"></td><td>還有花花綠綠的<b>現金回扣!</b></td></tr>
			</table>
		<p class="left-text-div-p left-p4">
			這是最壞的疫情時刻，
			卻<b>最好購買超值房屋</b>產業的良機。
		</p>
	</div>	
	<div class="right-text-div same-text-div">

		<p class="index-dual-title"><b>Guang Ming Properties E-Fair is here!</b></p>
		<p class="left-text-div-p left-p1">
			Be the first to register and grab the opportunity now!</p>
		<p class="left-text-div-p left-p2a">
			Total participants of 7 major housing developers in this online fairs.<br>
			Provide a full range of housing projects at your fingertips.<br>
			With luxury apartments, houses, villas, condos and industrial units available for selection.</p>
		<p class="left-text-div-p left-p3">
			Take advantage of the stamp duty exemption under the HOC Ownership Scheme,<br> 
			Together with other special reward packages:</p>
			<table class="icon-table">
				<tr><td><img src="img/car.png" class="ul-icon"></td><td>Buy an apartment ~ get a <b>car</b> for <b>free!</b></td></tr>
				<tr><td><img src="img/management-fee.png" class="ul-icon"></td><td><b>Free</b> two-year management fee!</td></tr>
				<tr><td><img src="img/interior-design.png" class="ul-icon"></td><td><b>Free interior design!</b></td></tr>
				<tr><td><img src="img/lawyer-fee.png" class="ul-icon"></td><td><b>Free legal fees</b> for sale and purchase contracts</td></tr>
				<tr><td><img src="img/cash-rebate.png" class="ul-icon"></td><td>Special <b>cash rebates!</b></td></tr>
			</table>
		<p class="left-text-div-p left-p4">
			This is the worst moment of the epidemic, but the best time to own a house.</p>
		</p>	
	</div>	
</div>	
<div class="width100 overflow same-padding program-spacing">
	<p class="index-dual-title"><b>Looking Forward 值得期待</b></p>
	<div class="index-four-div">
		<img src="img/look1.png" class="three-div-img">
		<p class="three-div-p">Comprehensive and safe
		<br>最全面、最安全線上看房</p>	
	</div>		
	<div class="index-four-div index-second-four-div">
		<img src="img/look2.png" class="three-div-img">
		<p class="three-div-p">Professional sales and consulting services
		<br>專業銷售和諮詢服務</p>	
	</div>			
	<div class="index-four-div index-third-four-div">
		<img src="img/look3.png" class="three-div-img">
		<p class="three-div-p">Lucky draw
		<br>幸運抽獎</p>	
	</div>
	<div class="index-four-div">
		<img src="img/look4.png" class="three-div-img">
		<p class="three-div-p">Solved the inconvenience of parking, queuing and crowding
		<br>免除泊車、排隊和擁擠的不便</p>	
	</div>		
</div>
<div class="clear"></div>
<div class="width100 overflow same-padding program-spacing">
	<p class="index-dual-title"><b>Our Program 我们的节目</b></p>
	<div class="index-three-div">
		<img src="img/program1.png" class="three-div-img">
		<!-- <p class="three-div-p">Online speech by Penang Chief Minister, YB Chow Kon Yeow
		<br>檳城首席部長曹觀友在線致辭</p>	 -->
		<p class="three-div-p">Online speech by Penang Chief Minister</p>	
		<p class="three-div-p">YAB Chow Kon Yeow</p>
		<p class="three-div-p">檳城首席部長曹觀友在線致辭</p>
	</div>	
	<div class="index-three-div index-mid-three-div">
		<img src="img/program2.png" class="three-div-img">
		<p class="three-div-p">Online talk show on property, feng shui, mortgage, valuations, auctions, tax...
		<br>名家线上主講產業投資講座、風水講座、融資和拍賣講座等</p>	
	</div>		
	<div class="index-three-div">
		<img src="img/program3.png" class="three-div-img">
		<p class="three-div-p">Online webinar
		<br>在線網絡研討會</p>	
	</div>		
</div>	
	
<div class="clear"></div>

<div class="width100 overflow same-padding program-spacing">
	<p class="index-dual-title"><b>Our Speakers 我们的主讲嘉宾</b></p>
	<div class="index-three-div">
		<img src="img/leon-lee.jpg" class="width100">
		<p class="three-div-p speaker-p">Mr Leon Lee 李烔良</p>
		<p class="three-div-p speech-p">First time homebuyers’ guide</p>
	</div>	
	<div class="index-three-div index-mid-three-div">
		<img src="img/dato-toh.jpg" class="width100">
		<p class="three-div-p speaker-p">Dato Toh 拿督杜进良</p>
		<p class="three-div-p speech-p">“HOC and other incentives; the time is NOW!”</p>
	</div>		
	<div class="index-three-div">
		<img src="img/ong-yu-shin.jpg" class="width100">
		<p class="three-div-p speaker-p">Mr Ong Yu Shin 王优幸</p>
		<p class="three-div-p speech-p">Basics of Residential, Service Suits & Commercial Tenancies</p>
	</div>
	<div class="clear"></div>
	<div class="index-three-div">
		<img src="img/adelyn.jpg" class="width100">
		<p class="three-div-p speaker-p">Miss Adelyn Low 刘家妤</p>
		<p class="three-div-p speech-p">Matters in relation to Sale and Purchase Agreement, a brief guide</p>
	</div>	
	<div class="index-three-div index-mid-three-div">
		<img src="img/stephen-soon.jpg" class="width100">
		<p class="three-div-p speaker-p">Mr Stephen Soon 孙子巄</p>
		<p class="three-div-p speech-p">Absolutely Successful Property Auction By Owner (Please take note, NOT bank auction). Can it really Work?<br>
绝对成功的业主所委任产业拍卖（请注意，非银行拍卖），真的行得通吗？
</p>
	</div>		
	<div class="index-three-div">
		<img src="img/stephen-kam3.JPG" class="width100">
		<p class="three-div-p speaker-p">Mr Stephen Kam 甘伟志</p>	
		<p class="three-div-p speech-p">Public Auction</p>
	</div>
	<div class="clear"></div>
	<div class="index-three-div">
		<img src="img/tan-chean-hwa.jpg" class="width100">
		<p class="three-div-p speaker-p">Sr Tan Chean Hwa 陈健桦</p>
		<p class="three-div-p speech-p">新常态下的房地产业</p>
	</div>	
	<div class="index-three-div index-mid-three-div">
		<img src="img/michael-geh.jpg" class="width100">
		<p class="three-div-p speaker-p">Sr Micheal Geh 倪川鹏</p>
		<p class="three-div-p speech-p">“The State of the Property Market"<br> "目前的房地产市场趋势"
</p>
	</div>		
	<div class="index-three-div">
		<img src="img/chuah-say-win.jpg" class="width100">
		<p class="three-div-p speaker-p">Chuah Say Win 蔡世赢</p>	
		<p class="three-div-p speech-p">Home Renovation Do’s and Dont’s</p>
	</div>	
	
	
	
</div>	

<div class="clear"></div>
<div class="width100 overflow same-padding co-spacing ow-co-spacing">
	<div class="index-left-or">
		<p class="index-title-size"><b>Co-organizers</b></p>
		<a href="https://guangming.com.my/" target="_blank"><img src="img/guangmingdaily2.png" class="index-logo-height index-guang opacity-hover" alt="Guang Ming Daily 光明日报" title="Guang Ming Daily 光明日报"></a>
		<a href="https://www.zeon.com.my/" target="_blank"><img src="img/zeon-properties2.png" class="index-logo-height index-guang opacity-hover" alt="Zeon Properties 益安房地产集团" title="Zeon Properties 益安房地产集团"></a>
	</div>	
	<div class="index-right-or">
		<p class="index-title-size"><b>Participants</b></p>
		<a href="https://aspen.com.my/" target="_blank">
			<img src="img/1aspen.png" class="index-logo-height2 index-mah opacity-hover" alt="Aspen Group" title="Aspen Group">	
		</a>
		<a href="https://jesseltonvillas.com/" target="_blank">
			<img src="img/2berjaya.png" class="index-logo-height2 index-cod opacity-hover" alt="Berjaya Land" title="Berjaya Land">
		</a>		
		<a href="./ewein-city-of-dreams.php" target="_blank"><img src="img/3cod.png" class="index-logo-height2 index-tah opacity-hover" alt="City of Dreams 梦想之城" title="City of Dreams 梦想之城"></a>
		<a href="http://www.hunzagroup.com/" target="_blank">
			<img src="img/4hunza.png" class="index-logo-height2 index-berjaya opacity-hover" alt="Hunza Properties 汇华产业集团" title="Hunza Properties 汇华产业集团">
		</a>	
		<a href="./mahsing.php" target="_blank"><img src="img/5mahsing.png" class="index-logo-height2 index-mah opacity-hover" alt="Mah Sing Group 馬星集團" title="Mah Sing Group 馬星集團"></a>		
		<a href="http://www.tahwah.com/" target="_blank">
		<img src="img/6tahwah.png" class="index-logo-height2 index-tah opacity-hover ow-bottom3-logo" alt="Tah Wah Group 大華集團" title="Tah Wah Group 大華集團"></a>	<a href="https://www.jadigroup.com/" target="_blank">
			<img src="img/7tamanjadi.png" class="index-logo-height2 index-tah opacity-hover" alt="Taman Jadi 嘉利发展有限公司" title="Taman Jadi 嘉利发展有限公司">
		</a>		
	</div>
</div>
<?php include 'js.php'; ?>
</body>
</html>