<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$liveDetails = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ORDER BY id ASC");

$adminShare = getUser($conn," WHERE user_type = '0' ");
$adminData = $adminShare[0];
$adminPlatform = $adminData->getPlatform();
$adminLink = $adminData->getLink();
$adminAutoplay = $adminData->getAutoplay();

$mainAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '2' ");
$allAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '1' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/index2.php" />
<link rel="canonical" href="https://gmvec.com/index2.php" />
<meta property="og:title" content="光明線上產業展 Guang Ming Properties E-Fair" />
<title>光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">

<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>
    <marquee class="announcement-marquee opacity-hover" behavior="scroll" direction="left">
        <a href="" target="_blank">
            <?php
            if($mainAnnoucement)
            {
                for($cntAA = 0;$cntAA < count($mainAnnoucement) ;$cntAA++)
                {
                ?>
                    <?php echo $mainAnnoucement[$cntAA]->getContent();?>
                <?php
                }
            }
            ?>
        </a>
    </marquee>
    <div class="width100 same-padding overflow gold-bg min-height marquee-distance">
    	<div class="width100 overflow text-center">
    		<img src="img/gmvec-logo.jpg" class="gmvec-logo" alt="光明線上產業展 Guang Ming Properties E-Fair" title="光明線上產業展 Guang Ming Properties E-Fair">
		</div>
        <h1 class="title-h1 text-center landing-title-h1 black-text">光明線上產業展<br>Guang Ming Properties E-Fair</h1>
        <!-- <h1 class="title-h1 text-center landing-title-h1 black-text"><?php //echo $mainTitle->getName();?></h1> -->
		<div class="left-video-only-div">

            <div class="width100 overflow">
                <iframe class="landing-top-iframe" src="https://www.youtube.com/embed/<?php echo $adminLink;?>?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

		</div>
   <div class="right-announcement-big-div">
         <!-- <h2 class="announcement-title">Announcement</h2> -->
         <h2 class="announcement-title">Schedule</h2>
         
   		<!-- Start repeat php code --->
        <?php
        if($allAnnoucement)
        {
            for($cnt = 0;$cnt < count($allAnnoucement) ;$cnt++)
            {
            ?>
                <a href="" target="_blank">
                    <div class="announcement-per-div opacity-hover">
                        <p class="small-date"><?php echo date("d-m-Y",strtotime($allAnnoucement[$cnt]->getDateCreated()));?></p>
                        <p class="announcement-content">
                            <?php echo $allAnnoucement[$cnt]->getContent();?>
                        </p>
                    
                    </div>
                </a>
            <?php
            }
        }
        ?>
   		<!-- End of php code -->

   </div> 
            
        
    <!-- <div class="width100 overflow same-padding program-spacing">
        <p class="index-dual-title"><b>Speakers Webinar</b></p>
        <div class="index-three-div">
            <img src="img/leon-lee.jpg" class="width100">
            <p class="three-div-p speaker-p">Mr Leon Lee 李烔良</p>
        </div>	
        <div class="index-three-div index-mid-three-div">
            <img src="img/dato-toh.jpg" class="width100">
            <p class="three-div-p speaker-p">Dato Toh 拿督杜进良</p>
        </div>		
        <div class="index-three-div">
            <img src="img/ong-yu-shin.jpg" class="width100">
            <p class="three-div-p speaker-p">Mr Ong Yu Shin 王优幸</p>
        </div>
        <div class="clear"></div>	
    </div>	 -->
        
        
        <div class="two-section-container overflow">
        <?php
        $conn = connDB();
        if($liveDetails)
        {
            for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
            {
            ?>

                <?php 
                    $platfrom =  $liveDetails[$cnt]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
                            <div class="two-section-div opacity-hover">
                                <p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                    <iframe class="two-section-iframe" src="https://www.youtube.com/embed/<?php echo $liveDetails[$cnt]->getLink();?>?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <div class="clear"></div>
                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>
                    <?php
                    }

                    elseif($platfrom == 'Zoom')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                <div class="two-section-iframe background-css" id="z<?php echo $liveDetails[$cnt]->getUid();?>" value="<?php echo $liveDetails[$cnt]->getUid();?>">
                            	</div>
                                <div class="clear"></div>
                                <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
							</div>
                        </a>
                        
						<style>
                        	#z<?php echo $liveDetails[$cnt]->getUid();?>{
								background-image:url(userProfilePic/<?php echo $liveDetails[$cnt]->getBroadcastShare();?>);}
                        </style>
                    <?php
                    }

                    elseif($platfrom == 'Facebook')
                    {
                    ?>

                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p  class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 

   
                                    <iframe  class="two-section-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $liveDetails[$cnt]->getLink();?>"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                           			<div class="clear"></div>
                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>

                    <?php
                    }

                    elseif($platfrom == 'Temporarily')
                    {
                    ?>

                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div">
                            	<p  class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 

   
                            <!-- <div class="staff-div-css overflow opacity-hover"> -->
                                    <div class="two-section-iframe" id="<?php echo "style".$liveDetails[$cnt]->getUid();?>"></div>

                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            <!-- </div>   -->
                        
                        <style>
                            /* .staff-1{ */
                            #<?php echo "style".$liveDetails[$cnt]->getUid();?>
                            {
                                background-image:url("uploads/<?php echo $liveDetails[$cnt]->getTitle();?>");
                                background-size:cover;
                                background-position:top;
                            }
                        </style>
                                    
                            </div>
                        </a>

                    <?php
                    }

                    else
                    {   }
                ?>

            <?php
            }
            ?>

        <?php
        }

        else
        {
        ?>
            NO BROADCASTING AT THE MOMENT, WE WILL RETURN SOON !!
        <?php
        }

        ?>
    </div>


    
    
    </div>

    <div class="clear"></div>
    
</div>

<?php include 'js.php'; ?>

</body>
</html>